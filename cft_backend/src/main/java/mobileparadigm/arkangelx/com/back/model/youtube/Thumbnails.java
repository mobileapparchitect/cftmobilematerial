
package mobileparadigm.arkangelx.com.back.model.youtube;


import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Thumbnails implements Parcelable {

    @JsonProperty("default")
    private Default _default;
    @JsonProperty("medium")
    private Medium medium;
    @JsonProperty("high")
    private High high;
    @JsonProperty("standard")
    private Standard standard;

    protected Thumbnails(Parcel in) {
        _default = in.readParcelable(Default.class.getClassLoader());
        medium = in.readParcelable(Medium.class.getClassLoader());
        high = in.readParcelable(High.class.getClassLoader());
        standard = in.readParcelable(Standard.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(_default, flags);
        dest.writeParcelable(medium, flags);
        dest.writeParcelable(high, flags);
        dest.writeParcelable(standard, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Thumbnails> CREATOR = new Creator<Thumbnails>() {
        @Override
        public Thumbnails createFromParcel(Parcel in) {
            return new Thumbnails(in);
        }

        @Override
        public Thumbnails[] newArray(int size) {
            return new Thumbnails[size];
        }
    };

    /**
     * 
     * @return
     *     The _default
     */
    @JsonProperty("default")
    public Default getDefault() {
        return _default;
    }

    /**
     * 
     * @param _default
     *     The default
     */
    @JsonProperty("default")
    public void setDefault(Default _default) {
        this._default = _default;
    }

    /**
     * 
     * @return
     *     The medium
     */
    @JsonProperty("medium")
    public Medium getMedium() {
        return medium;
    }

    /**
     * 
     * @param medium
     *     The medium
     */
    @JsonProperty("medium")
    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    /**
     * 
     * @return
     *     The high
     */
    @JsonProperty("high")
    public High getHigh() {
        return high;
    }

    /**
     * 
     * @param high
     *     The high
     */
    @JsonProperty("high")
    public void setHigh(High high) {
        this.high = high;
    }

    /**
     * 
     * @return
     *     The standard
     */
    @JsonProperty("standard")
    public Standard getStandard() {
        return standard;
    }

    /**
     * 
     * @param standard
     *     The standard
     */
    @JsonProperty("standard")
    public void setStandard(Standard standard) {
        this.standard = standard;
    }

}
