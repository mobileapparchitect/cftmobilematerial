/*
package mobileparadigm.arkangelx.com.back.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.view.View;



import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import mobileparadigm.arkangelx.com.back.CftMobileApplication;
import mobileparadigm.arkangelx.com.back.logs.Log;

public class ImageUtils {

    public static final int REQUEST_PICTURE = 100;
    public static final int PICTURE_CROPPED = 1011;
    public static final int REQUEST_IMAGE_CAPTURE = 102;
    public static final int MAX_AVATAR_SIZE = 250;
    public static final int MAX_COVER_SIZE = 600;
    public static final int MAX_POST_SIZE = 1000;
    private static final String LOG_TAG = "utils.ImageUtils";

    public static int calculateFutureImageHeight(int targetWidth, int width, int height) {
        double aspectRatio = (double) height / (double) width;
        int targetHeight = (int) (targetWidth * aspectRatio);
        return targetHeight;
    }

    public static byte[] getImageFileByteArray(String path, int bitmapCompression, int maxWidth, int maxHeight) {
        Bitmap bm;
        bm = path.startsWith("http") ? getImageFromUrl(path, maxWidth, maxHeight) : getImageFromDevice(path, maxWidth, maxHeight);
        if (bm != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, bitmapCompression, bos);
            byte[] data = bos.toByteArray();
            return data;
        } else {
            return null;
        }
    }

    public static Bitmap getImageFromUri(Uri uri, float targetHeight, float targetWidth) {
        InputStream in;
        Bitmap bmp = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            in = CftMobileApplication.getAppContext().getContentResolver().openInputStream(uri);
            bmp = BitmapFactory.decodeStream(in, null, options);
            in.close();
            options = getSampleSize(options, targetHeight, targetWidth);
            options.inJustDecodeBounds = false;
            in = CftMobileApplication.getAppContext().getContentResolver().openInputStream(uri);
            bmp = BitmapFactory.decodeStream(in, null, options);
            in.close();
        } catch (FileNotFoundException e) {
            Log.e(e);
        } catch (IOException e) {
            Log.e(e);
        }
        return bmp;
    }

    private static BitmapFactory.Options getSampleSize(BitmapFactory.Options options, float targetHeight, float targetWidth) {
        // Calculate in sample size//
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if ((targetHeight > 0 && height > targetHeight) || (targetWidth > 0 && width > targetWidth)) {
            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round(height / targetHeight);
            final int widthRatio = Math.round(width / targetWidth);
            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to
            // the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        options.inSampleSize = inSampleSize;
        return options;
    }

    public static Bitmap getImageFromDevice(String path, float targetHeight, float targetWidth) {
        if (path == null || targetHeight == -1 || targetWidth == -1) {
            Log.e(new Exception("getImageFromDevice error: path" +
                    path + " targetHeight: " + targetHeight + " targetWidth: " + targetWidth));
            return null;
        }
        BitmapFactory.Options options = decodeInBounds(path, targetHeight, targetWidth);
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static Bitmap getImageFromUrl(String urlString, float targetHeight, float targetWidth) {
        if (urlString == null || targetHeight == -1 || targetWidth == -1) {
            Log.e(new Exception("getImageFromUrl error: urlString" +
                    urlString + " targetHeight: " + targetHeight + " targetWidth: " + targetWidth));
            return null;
        }
        BitmapFactory.Options options = decodeInBounds(urlString, targetHeight, targetWidth);
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            conn.setInstanceFollowRedirects(false);
            InputStream is = new BufferedInputStream(conn.getInputStream());
            Bitmap bmp = BitmapFactory.decodeStream(is, null, options);
            is.close();
            return bmp;
        } catch (Exception e) {
            Log.e("Error while retrieving bitmap from " + urlString + " - " + e.toString());
            return null;
        }
    }

    public static String getImageURLWithDimensionsInPixels(String url, int width, int height) {
        if (url == null) {
            Log.e(new Exception("getImageURLWithDimensionsInPixels: URL is null"));
            return null;
        }
        final String ret = url.replace("{width}", Integer.toString(width)).replace("{height}", Integer.toString(height));
        return ret;
    }

    private static BitmapFactory.Options decodeInBounds(String path, float targetHeight, float targetWidth) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        if (path.startsWith("http")) {
            try {
                URL url = new URL(path);
                URLConnection conn = url.openConnection();
                InputStream is = new BufferedInputStream(conn.getInputStream());
                is.mark(is.available());
                BitmapFactory.decodeStream(is, null, options);
                is.close();
            } catch (MalformedURLException e) {
                Log.e(e);
            } catch (IOException e) {
                Log.e(e);
            }
        } else {
            try {
                BitmapFactory.decodeFile(path, options);
            } catch (Exception e) {
                Log.e(e);
            }
        }
        options = getSampleSize(options, targetHeight, targetWidth);
        options.inJustDecodeBounds = false;
        return options;
    }

    public static Uri captureImage(Activity context, String title) {
        Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "SportLobster " + title);
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        Uri selectedCroppedImageUri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedCroppedImageUri);
        context.startActivityForResult(imageCaptureIntent, REQUEST_IMAGE_CAPTURE);
        return selectedCroppedImageUri;
    }

    public static Uri captureImage(Fragment context, String title) {
        Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "SportLobster " + title);
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        Uri selectedCroppedImageUri = context.getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedCroppedImageUri);
        context.startActivityForResult(imageCaptureIntent, REQUEST_IMAGE_CAPTURE);
        return selectedCroppedImageUri;
    }

    public static void setBackground(View view, Drawable drawable) {
        if (view != null && drawable != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(drawable);
            } else {
                view.setBackground(drawable);
            }
        }
    }

    public static Drawable getAssetImage(Context context, String filename, float targetHeight, float targetWidth) {
        try {
            AssetManager assets = context.getResources().getAssets();
            String density = DeviceUtils.getDensity(context) + "/";
            InputStream buffer = new BufferedInputStream((assets.open("drawable/" + density + filename)));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeStream(buffer, null, options);
            buffer.close();
            if (bitmap != null) {
                final int height = options.outHeight;
                final int width = options.outWidth;
                int inSampleSize = 1;
                if (height > targetHeight || width > targetWidth) {
                    final int heightRatio = Math.round(height / targetHeight);
                    final int widthRatio = Math.round(width / targetWidth);
                    inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
                }
                options.inSampleSize = inSampleSize;
            }
            options.inJustDecodeBounds = false;
            buffer = new BufferedInputStream((assets.open("drawable/" + density + filename)));
            bitmap = BitmapFactory.decodeStream(buffer, null, options);
            return new BitmapDrawable(context.getResources(), bitmap);
        } catch (Exception e) {
            return null;
        }
    }

    public static void saveImage(Bitmap bmp, String path) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File file = new File(path);
        try {
            file.createNewFile();
            //write the bytes in file
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            // remember close de FileOutput
            fo.close();
        } catch (IOException e) {
            Log.e(e);
        }
    }

    public static boolean isImage(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            InputStream in = CftMobileApplication.getAppContext().getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(in, null, options);
            in.close();
            return options.outWidth != -1 && options.outHeight != -1;
        } catch (Exception e) {
            return false;
        }
    }
}
*/
