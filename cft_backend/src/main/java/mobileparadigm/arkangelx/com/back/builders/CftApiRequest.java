package mobileparadigm.arkangelx.com.back.builders;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.common.base.Charsets;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobileparadigm.arkangelx.com.back.CftMobileApplication;
import mobileparadigm.arkangelx.com.back.logs.Log;
import mobileparadigm.arkangelx.com.back.model.EntityBodyImageRequest;
import mobileparadigm.arkangelx.com.back.network.ErrorResponse;
import mobileparadigm.arkangelx.com.back.network.HttpsTrustManager;
import mobileparadigm.arkangelx.com.back.network.ServicesUtils;
import mobileparadigm.arkangelx.com.back.network.VolleySingleton;
import mobileparadigm.arkangelx.com.back.utils.DeviceUtils;


public class CftApiRequest<T> extends Request<T> {

    protected static final int SOCKET_TIMEOUT_MS = 15000;
    private final Class<T> responseType;
    private final Response.Listener<T> listener;
    private final Map<String, String> params;
    private final boolean allowFromCache;
    private final MultipartEntityBuilder builder=null;
    private HttpEntity entity;

    public CftApiRequest(String url, int method, Response.Listener listener, Class responseType,
                         Response.ErrorListener errorListener, HashMap<String, String> params, String tag, boolean allowFromCache) {
        super(method, url, new ErrorHandler(errorListener, url));
        super.setTag(tag);
        this.responseType = responseType;
        this.listener = listener;
        this.params = params;
        this.allowFromCache = allowFromCache;
        this.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        setShouldCache(false);
    }

    public CftApiRequest(String url, int method, Response.Listener listener, Class responseType,
                         Response.ErrorListener errorListener, HashMap<String, String> params,
                         String tag, boolean allowFromCache, List<EntityBodyImageRequest> imageBodies) {
        super(method, url, new ErrorHandler(errorListener, url));
        super.setTag(tag);
        this.responseType = responseType;
        this.listener = listener;
        this.params = params;
        this.allowFromCache = allowFromCache;
        this.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        setShouldCache(false);

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();
        String authToken = CftMobileApplication.getApiKey();
        String userpass = authToken + ":" + "X";
        //  String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
        String basicAuth = "Basic " + android.util.Base64.encodeToString(userpass.getBytes(), android.util.Base64.DEFAULT);
        if (authToken != null) {
            headers.put(HeaderFields.AUTHORIZATION, basicAuth);
        }

        return headers;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (getMethod() == Method.POST) {
            return params;
        } else {
            return new HashMap<>();
        }
    }

    @Override
    public String getBodyContentType() {
        return super.getBodyContentType();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return super.getBody();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String jsonString;
        if (response.headers.containsKey(HeaderFields.CONTENT_ENCODING) && response.headers.get(HeaderFields.CONTENT_ENCODING).equals(HeaderFields.ENCODING_GZIP)) {
            try {
                jsonString = ServicesUtils.getGzipString(response);
            } catch (IOException e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            }
        } else {
            try {
                jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            }
        }
        try {
            jsonString = ServicesUtils.getDataString(jsonString);
            T object = CftMobileApplication.getObjectMapperInstance().readValue(jsonString, responseType);
            Log.i("responseType:" + responseType + " object type:" + object.getClass().getSimpleName());
            return Response.success(object,
                    ServicesUtils.parseIgnoreCacheHeaders(response));
        } catch (Exception e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        try {
            String response = new String(volleyError.networkResponse.data,
                    HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
            return CftMobileApplication.getObjectMapperInstance().readValue(response, ErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.parseNetworkError(volleyError);
    }

    @Override
    protected void deliverResponse(T t) {
        listener.onResponse(t);
    }

    public void execute() {
        if (getUrl().startsWith("https")) {
            HttpsTrustManager.allowAllSSL();
        }

        if (allowFromCache) {
            Cache cache = VolleySingleton.getInstance().getRequestQueue().getCache();
            Cache.Entry entry = cache.get(getUrl());
            if (entry != null && !DeviceUtils.isConnected(CftMobileApplication.getAppContext())) {
                try {
                    String data = new String(entry.data, Charsets.UTF_8.name());
                    listener.onResponse(CftMobileApplication.getObjectMapperInstance().readValue(data, responseType));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        VolleySingleton.getInstance().getRequestQueue().cancelAll(getTag());
        VolleySingleton.getInstance().getRequestQueue().add(this);
    }

    private static class ErrorHandler implements Response.ErrorListener {

        private Response.ErrorListener errorListener;
        private String url;

        public ErrorHandler(Response.ErrorListener errorListener, String url) {
            this.errorListener = errorListener;
            this.url = url;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            //ErrorUtils.logCrashlyticVolleyException(error, url);
            if (errorListener != null)
                errorListener.onErrorResponse(error);
        }
    }
}
