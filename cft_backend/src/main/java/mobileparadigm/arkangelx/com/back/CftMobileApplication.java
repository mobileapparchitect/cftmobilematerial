package mobileparadigm.arkangelx.com.back;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.multidex.MultiDexApplication;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;

import mobileparadigm.arkangelx.com.back.logs.Log;
import mobileparadigm.arkangelx.com.back.network.VolleySingleton;

public class CftMobileApplication extends MultiDexApplication {

    protected static CftMobileApplication singleton;
    private static String userAgent;
    private static ObjectMapper objectMapper;
    private StringBuilder stringBuilder;

    public static CftMobileApplication getInstance() {
        return singleton;
    }

    public static ObjectMapper getObjectMapperInstance() {
        return objectMapper;
    }

    public static String getUserAgent() {
        if (userAgent != null) {
            return userAgent;
        }
        PackageInfo packageInfo;
        try {
            packageInfo = singleton.getPackageManager().getPackageInfo(singleton.getPackageName(), 0);
            userAgent = "Sportlobster/" + packageInfo.versionName + "/" + packageInfo.versionCode + " (android; " + Build.VERSION.RELEASE + ")";
        } catch (PackageManager.NameNotFoundException e) {
            //   Log.e("User Agent not found.");
            userAgent = "Sportlobster/unknown/unknown (android; " + Build.VERSION.RELEASE + ")";
        }
        return userAgent;
    }

    public static String getAccountType() {
        return singleton.getPackageName();
    }

    public static String getDeviceId() {
        return Settings.Secure.getString(singleton.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean getBooleanValueOfConfigProperty(String property) {
        Properties p = new Properties();
        try {
            p.load(singleton.getAssets().open("config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String value = p.getProperty(property);
        if (value != null) {
            return Boolean.valueOf(value);
        }
        return false;
    }

    public static String getApiKey() {
        return "jY0RGOZbm8mohPA1hK8MZZLRbjmY5DCOngdcUQDQ:X";
    }

    public static Context getAppContext() {
        return singleton.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        initLog();
        objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));


    }

    public void initLog() {
        mobileparadigm.arkangelx.com.back.logs.Log.init(this, new mobileparadigm.arkangelx.com.back.logs.LogConfig() {
            @Override
            public byte getLogLevel() {
                return Log.ALL;
            }

            @Override
            public HashSet<Class<?>> getClassesToMute() {
                return null;
            }

            @Override
            public HashSet<String> getPackagesToMute() {
                return null;
            }
        });
    }

    @Override
    public void onLowMemory() {
        Runtime.getRuntime().gc();
        mobileparadigm.arkangelx.com.back.logs.Log.e("onLowMemory()");
        VolleySingleton.getInstance().cancelAll();
        super.onLowMemory();
    }

    public boolean isDebug() {
        return (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }


    public static String getApiUrl() {
        return "https://bibles.org/v2";
    }

    public String getYoutbue() {
        return "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername=GoogleDevelopers&key=AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ";
    }

    public Locale getLocale() {
        return getResources().getConfiguration().locale;
    }

}