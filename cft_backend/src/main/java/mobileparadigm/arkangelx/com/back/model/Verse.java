package mobileparadigm.arkangelx.com.back.model;

import android.databinding.BaseObservable;
import android.os.Parcelable;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;


public class Verse extends BaseObservable implements Parcelable{

    private static final String FIELD_VERSE = "verse";
    private static final String FIELD_NEXT_OSIS_ID = "next_osis_id";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_REFERENCE = "reference";
    private static final String FIELD_LASTVERSE = "lastverse";
    private static final String FIELD_NEXT = "next";
    private static final String FIELD_OSIS_END = "osis_end";
    private static final String FIELD_LABEL = "label";
    private static final String FIELD_PARENT = "parent";
    private static final String FIELD_PREV_OSIS_ID = "prev_osis_id";
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_COPYRIGHT = "copyright";
    private static final String FIELD_PATH = "path";
    private static final String FIELD_PREVIOUS = "previous";
    private static final String FIELD_AUDITID = "auditid";

    @SerializedName(FIELD_VERSE)
    private int mVerse;

    @SerializedName(FIELD_NEXT_OSIS_ID)
    private String mNextOsisId;

    @SerializedName(FIELD_TEXT)
    private String mText;

    @SerializedName(FIELD_REFERENCE)
    private String mReference;

    @SerializedName(FIELD_LASTVERSE)
    private int mLastverse;

    @SerializedName(FIELD_NEXT)
    private Next mNext;

    @SerializedName(FIELD_OSIS_END)
    private String mOsisEnd;

    @SerializedName(FIELD_LABEL)
    private String mLabel;

    @SerializedName(FIELD_PARENT)
    private Parent mParent;

    @SerializedName(FIELD_PREV_OSIS_ID)
    private String mPrevOsisId;

    @SerializedName(FIELD_ID)
    private String mId;

    @SerializedName(FIELD_NAME)
    private String mName;

    @SerializedName(FIELD_COPYRIGHT)
    private String mCopyright;


    @SerializedName(FIELD_PATH)
    private String mPath;

    @SerializedName(FIELD_PREVIOUS)
    private Previous mPrevious;


    @SerializedName(FIELD_AUDITID)
    private int mAuditid;


    public Verse(){

    }

    public void setVerse(int verse) {
        mVerse = verse;
    }

    public int getVerse() {
        return mVerse;
    }

    public void setNextOsisId(String nextOsisId) {
        mNextOsisId = nextOsisId;
    }

    public String getNextOsisId() {
        return mNextOsisId;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getText() {
        return mText;
    }

    public void setReference(String reference) {
        mReference = reference;
    }

    public String getReference() {
        return mReference;
    }

    public void setLastverse(int lastverse) {
        mLastverse = lastverse;
    }

    public int getLastverse() {
        return mLastverse;
    }

    public void setNext(Next next) {
        mNext = next;
    }

    public Next getNext() {
        return mNext;
    }

    public void setOsisEnd(String osisEnd) {
        mOsisEnd = osisEnd;
    }

    public String getOsisEnd() {
        return mOsisEnd;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setParent(Parent parent) {
        mParent = parent;
    }

    public Parent getParent() {
        return mParent;
    }

    public void setPrevOsisId(String prevOsisId) {
        mPrevOsisId = prevOsisId;
    }

    public String getPrevOsisId() {
        return mPrevOsisId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setCopyright(String copyright) {
        mCopyright = copyright;
    }

    public String getCopyright() {
        return mCopyright;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getPath() {
        return mPath;
    }

    public void setPrevious(Previous previous) {
        mPrevious = previous;
    }

    public Previous getPrevious() {
        return mPrevious;
    }

    public void setAuditid(int auditid) {
        mAuditid = auditid;
    }

    public int getAuditid() {
        return mAuditid;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Verse){
            return ((Verse) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return mId.hashCode();
    }

    public Verse(Parcel in) {
        mVerse = in.readInt();
        mNextOsisId = in.readString();
        mText = in.readString();
        mReference = in.readString();
        mLastverse = in.readInt();
        mNext = in.readParcelable(Next.class.getClassLoader());
        mOsisEnd = in.readString();
        mLabel = in.readString();
        mParent = in.readParcelable(Parent.class.getClassLoader());
        mPrevOsisId = in.readString();
        mId = in.readString();
        mName = in.readString();
        mCopyright = in.readString();
        mPath = in.readString();
        mPrevious = in.readParcelable(Previous.class.getClassLoader());
        mAuditid = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Verse> CREATOR = new Creator<Verse>() {
        public Verse createFromParcel(Parcel in) {
            return new Verse(in);
        }

        public Verse[] newArray(int size) {
        return new Verse[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mVerse);
        dest.writeString(mNextOsisId);
        dest.writeString(mText);
        dest.writeString(mReference);
        dest.writeInt(mLastverse);
        dest.writeParcelable(mNext, flags);
        dest.writeString(mOsisEnd);
        dest.writeString(mLabel);
        dest.writeParcelable(mParent, flags);
        dest.writeString(mPrevOsisId);
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mCopyright);
        dest.writeString(mPath);
        dest.writeParcelable(mPrevious, flags);
        dest.writeInt(mAuditid);
    }


}