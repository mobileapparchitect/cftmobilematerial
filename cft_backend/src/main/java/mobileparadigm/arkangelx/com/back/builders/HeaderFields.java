package mobileparadigm.arkangelx.com.back.builders;

/**
 * Created by jakubszczygiel on 30/12/14.
 */
public interface HeaderFields {

    public static final String AUTHORIZATION = "Authorization";
    public static final String ENCODING_GZIP = "gzip";
    public static final String CONTENT_ENCODING = "Content-Encoding";

}
