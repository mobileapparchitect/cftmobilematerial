package mobileparadigm.arkangelx.com.back.model;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Response  extends BaseObservable implements Parcelable {

    private static final String FIELD_VERSES = "verses";

    @SerializedName(FIELD_VERSES)
    private List<Verse> mVerses;

    public Response() {
    }

    public void setVerses(List<Verse> verses) {
        mVerses = verses;
    }

    public List<Verse> getVerses() {
        return mVerses;
    }

    public Response(Parcel in) {
        mVerses = new ArrayList<Verse>();
        in.readTypedList(mVerses, Verse.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Response> CREATOR = new Creator<Response>() {
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
            return new Response[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mVerses);
    }
}