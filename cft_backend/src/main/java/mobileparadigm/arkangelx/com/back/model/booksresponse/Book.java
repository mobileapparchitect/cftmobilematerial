package mobileparadigm.arkangelx.com.back.model.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Book implements Parcelable{

    private static final String FIELD_ABBR = "abbr";
    private static final String FIELD_NEXT = "next";
    private static final String FIELD_OSIS_END = "osis_end";
    private static final String FIELD_PARENT = "parent";
    private static final String FIELD_TESTAMENT = "testament";
    private static final String FIELD_ID = "id";
    private static final String FIELD_VERSION_ID = "version_id";
    private static final String FIELD_ORD = "ord";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_COPYRIGHT = "copyright";
    private static final String FIELD_PATH = "path";
    private static final String FIELD_BOOK_GROUP_ID = "book_group_id";
    private static final String FIELD_PREVIOUS = "previous";


    @SerializedName(FIELD_ABBR)
    private String mAbbr;
    @SerializedName(FIELD_NEXT)
    private Next mNext;
    @SerializedName(FIELD_OSIS_END)
    private String mOsisEnd;
    @SerializedName(FIELD_PARENT)
    private Parent mParent;
    @SerializedName(FIELD_TESTAMENT)
    private String mTestament;
    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_VERSION_ID)
    private int mVersionId;
    @SerializedName(FIELD_ORD)
    private int mOrd;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_COPYRIGHT)
    private String mCopyright;
    @SerializedName(FIELD_PATH)
    private String mPath;
    @SerializedName(FIELD_BOOK_GROUP_ID)
    private int mBookGroupId;
    @SerializedName(FIELD_PREVIOUS)
    private Previous mPrevious;


    public Book(){

    }

    public void setAbbr(String abbr) {
        mAbbr = abbr;
    }

    public String getAbbr() {
        return mAbbr;
    }

    public void setNext(Next next) {
        mNext = next;
    }

    public Next getNext() {
        return mNext;
    }

    public void setOsisEnd(String osisEnd) {
        mOsisEnd = osisEnd;
    }

    public String getOsisEnd() {
        return mOsisEnd;
    }

    public void setParent(Parent parent) {
        mParent = parent;
    }

    public Parent getParent() {
        return mParent;
    }

    public void setTestament(String testament) {
        mTestament = testament;
    }

    public String getTestament() {
        return mTestament;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setVersionId(int versionId) {
        mVersionId = versionId;
    }

    public int getVersionId() {
        return mVersionId;
    }

    public void setOrd(int ord) {
        mOrd = ord;
    }

    public int getOrd() {
        return mOrd;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setCopyright(String copyright) {
        mCopyright = copyright;
    }

    public String getCopyright() {
        return mCopyright;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getPath() {
        return mPath;
    }

    public void setBookGroupId(int bookGroupId) {
        mBookGroupId = bookGroupId;
    }

    public int getBookGroupId() {
        return mBookGroupId;
    }

    public void setPrevious(Previous previous) {
        mPrevious = previous;
    }

    public Previous getPrevious() {
        return mPrevious;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Book){
            return ((Book) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return mId.hashCode();
    }

    public Book(Parcel in) {
        mAbbr = in.readString();
        mNext = in.readParcelable(Next.class.getClassLoader());
        mOsisEnd = in.readString();
        mParent = in.readParcelable(Parent.class.getClassLoader());
        mTestament = in.readString();
        mId = in.readString();
        mVersionId = in.readInt();
        mOrd = in.readInt();
        mName = in.readString();
        mCopyright = in.readString();
        mPath = in.readString();
        mBookGroupId = in.readInt();
        mPrevious = in.readParcelable(Previous.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        public Book[] newArray(int size) {
        return new Book[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAbbr);
        dest.writeParcelable(mNext, flags);
        dest.writeString(mOsisEnd);
        dest.writeParcelable(mParent, flags);
        dest.writeString(mTestament);
        dest.writeString(mId);
        dest.writeInt(mVersionId);
        dest.writeInt(mOrd);
        dest.writeString(mName);
        dest.writeString(mCopyright);
        dest.writeString(mPath);
        dest.writeInt(mBookGroupId);
        dest.writeParcelable(mPrevious, flags);
    }


}