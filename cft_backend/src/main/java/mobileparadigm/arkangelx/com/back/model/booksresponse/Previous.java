package mobileparadigm.arkangelx.com.back.model.booksresponse;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Previous  implements Parcelable{

    private static final String FIELD_BOOK = "book";


    @SerializedName(FIELD_BOOK)
    private Book mBook;


    public Previous(){

    }

    public void setBook(Book book) {
        mBook = book;
    }

    public Book getBook() {
        return mBook;
    }

    public Previous(Parcel in) {
        mBook = in.readParcelable(Book.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Previous> CREATOR = new Creator<Previous>() {
        public Previous createFromParcel(Parcel in) {
            return new Previous(in);
        }

        public Previous[] newArray(int size) {
        return new Previous[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mBook, flags);
    }


}