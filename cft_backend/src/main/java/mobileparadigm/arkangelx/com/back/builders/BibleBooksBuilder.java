package mobileparadigm.arkangelx.com.back.builders;

public class BibleBooksBuilder extends GetRequestBuilder {

    public BibleBooksBuilder() {

        super(false);
        url += RequestMethods.METHOD_BOOKS;
    }

    public static BibleBooksBuilder newBuilder() {
        return new BibleBooksBuilder();
    }


}
