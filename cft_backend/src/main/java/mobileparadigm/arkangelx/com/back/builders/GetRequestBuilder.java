package mobileparadigm.arkangelx.com.back.builders;

import com.android.volley.Request;

import mobileparadigm.arkangelx.com.back.network.ServicesUtils;

public  class GetRequestBuilder extends BaseRequestBuilder {
        public GetRequestBuilder(boolean flag) {
            super(flag);
            setMethod(Request.Method.GET);
        }

        @Override
        public CftApiRequest build() {
            encodeParams();
            checkPreconditions();
            return new CftApiRequest(url, method, listener, responseType, errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);
        }
    }