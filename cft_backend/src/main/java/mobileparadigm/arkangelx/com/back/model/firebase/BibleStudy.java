
package mobileparadigm.arkangelx.com.back.model.firebase;


import com.fasterxml.jackson.annotation.JsonProperty;

public class BibleStudy {

    @JsonProperty("book")
    private String book;
    @JsonProperty("chapter")
    private Integer chapter;
    @JsonProperty("date")
    private String date;
    @JsonProperty("eveningStudyChapter")
    private Integer eveningStudyChapter;
    @JsonProperty("eveningStudyVerse")
    private Integer eveningStudyVerse;
    @JsonProperty("eveningStudybook")
    private String eveningStudybook;
    @JsonProperty("verse")
    private Integer verse;

    /**
     * 
     * @return
     *     The book
     */
    @JsonProperty("book")
    public String getBook() {
        return book;
    }

    /**
     * 
     * @param book
     *     The book
     */
    @JsonProperty("book")
    public void setBook(String book) {
        this.book = book;
    }

    /**
     * 
     * @return
     *     The chapter
     */
    @JsonProperty("chapter")
    public Integer getChapter() {
        return chapter;
    }

    /**
     * 
     * @param chapter
     *     The chapter
     */
    @JsonProperty("chapter")
    public void setChapter(Integer chapter) {
        this.chapter = chapter;
    }

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The eveningStudyChapter
     */
    @JsonProperty("eveningStudyChapter")
    public Integer getEveningStudyChapter() {
        return eveningStudyChapter;
    }

    /**
     * 
     * @param eveningStudyChapter
     *     The eveningStudyChapter
     */
    @JsonProperty("eveningStudyChapter")
    public void setEveningStudyChapter(Integer eveningStudyChapter) {
        this.eveningStudyChapter = eveningStudyChapter;
    }

    /**
     * 
     * @return
     *     The eveningStudyVerse
     */
    @JsonProperty("eveningStudyVerse")
    public Integer getEveningStudyVerse() {
        return eveningStudyVerse;
    }

    /**
     * 
     * @param eveningStudyVerse
     *     The eveningStudyVerse
     */
    @JsonProperty("eveningStudyVerse")
    public void setEveningStudyVerse(Integer eveningStudyVerse) {
        this.eveningStudyVerse = eveningStudyVerse;
    }

    /**
     * 
     * @return
     *     The eveningStudybook
     */
    @JsonProperty("eveningStudybook")
    public String getEveningStudybook() {
        return eveningStudybook;
    }

    /**
     * 
     * @param eveningStudybook
     *     The eveningStudybook
     */
    @JsonProperty("eveningStudybook")
    public void setEveningStudybook(String eveningStudybook) {
        this.eveningStudybook = eveningStudybook;
    }

    /**
     * 
     * @return
     *     The verse
     */
    @JsonProperty("verse")
    public Integer getVerse() {
        return verse;
    }

    /**
     * 
     * @param verse
     *     The verse
     */
    @JsonProperty("verse")
    public void setVerse(Integer verse) {
        this.verse = verse;
    }

}
