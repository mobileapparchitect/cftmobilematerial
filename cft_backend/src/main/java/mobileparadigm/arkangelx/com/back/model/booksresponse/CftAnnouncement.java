package mobileparadigm.arkangelx.com.back.model.booksresponse;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by arkangel on 26/12/2015.
 */

@RealmClass
public class CftAnnouncement extends RealmObject {

    private String newsFlashTitle;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    private String parentId;

    public String getNewsFlashTitle() {
        return newsFlashTitle;
    }

    public void setNewsFlashTitle(String newsFlashTitle) {
        this.newsFlashTitle = newsFlashTitle;
    }

    public String getYearThemeTitle() {
        return yearThemeTitle;
    }

    public void setYearThemeTitle(String yearThemeTitle) {
        this.yearThemeTitle = yearThemeTitle;
    }

    public String getNewsFlashDetail() {
        return newsFlashDetail;
    }

    public void setNewsFlashDetail(String newsFlashDetail) {
        this.newsFlashDetail = newsFlashDetail;
    }

    public long getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(long announcementId) {
        this.announcementId = announcementId;
    }

    private String yearThemeTitle;
    private String newsFlashDetail;
    @PrimaryKey
    private long announcementId;
}
