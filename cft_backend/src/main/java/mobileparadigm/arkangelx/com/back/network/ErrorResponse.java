package mobileparadigm.arkangelx.com.back.network;

import com.android.volley.VolleyError;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jakubszczygiel on 30/12/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse extends VolleyError {
    public String error;
    @JsonProperty("error_code")
    public int errorCode;
    @JsonProperty("error_field")
    public String errorField;
    public String status;
    public String exception;
}
