package mobileparadigm.arkangelx.com.back.builders;

import android.text.TextUtils;

import org.apache.http.message.BasicNameValuePair;

import java.util.List;

import mobileparadigm.arkangelx.com.back.model.booksresponse.BookTable;

public class VersesGetRequestBuilder extends GetRequestBuilder {

    // chapters/eng-KJVA:1Cor.2/verses.js
    //start=5&end=6
    public VersesGetRequestBuilder(String version, String bookIndex, String book, String chapter, String start, String end) {
        // https://bibles.org/v2/chapters/?chapters%2F=eng-KJVA%3A&bookIndex=1&book=Cor.&chapter=2
        super(false);
        StringBuilder builder = new StringBuilder();
        builder.append(RequestMethods.METHOD_CHAPTERS);
        builder.append("/");
        builder.append(version);
        builder.append(":");
        builder.append(book);
        builder.append(".");
        builder.append(chapter);

        if (!TextUtils.isEmpty(start)){
            params.add(new BasicNameValuePair(RequestFields.FIELD_START, start));
        }else{
            start="0";
        }
        if(!TextUtils.isEmpty(end)){
            params.add(new BasicNameValuePair(RequestFields.FIELD_END, end));
        }

        else{
            params.add(new BasicNameValuePair(RequestFields.FIELD_END, String.valueOf(Integer.parseInt(start))));
        }
        url += builder.toString() + RequestMethods.METHOD_VERSES;

    }

    public static VersesGetRequestBuilder newBuilder(String version, String bookIndex, String book, String chapter, String start, String end) {
        return new VersesGetRequestBuilder(version, bookIndex, book, chapter, start, end);
    }



}
