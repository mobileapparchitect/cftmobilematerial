package mobileparadigm.arkangelx.com.back.builders;

public  interface RequestFields {

        public static final String FIELD_CHAPTERS = "chapters/";
        public static final String FIELD_BOOK_INDEX = "bookIndex";
        public static final String FIELD_BOOK = "book";
        public static final String FIELD_CHAPTER= "chapter";
        public static final String FIELD_VERSION= "version";
        public static final String FIELD_START="start";
        public static final String FIELD_END="end";

        public static final String VALUE_GRANT_TYPE_PASSWORD = "password";
        public static final String VALUE_XDEBUG_SESSION_START_NETBEANS_XDEBUG = "netbeans-xdebug";
        public static final String VALUE_PHOTO = "photo.png";
        public static final String VALUE_MINIFIED_TRUE = "true";
        public static final String VALUE_FACEBOOK_SHARE_MESSAGE_ONLY = "true";
        public static final String VALUE_ATTACH_IS_BADGE = "1";
        public static final String VALUE_MY_SPORTS = "1";
        public static final String VALUE_OPTIONS_TYPE_USER = "{\"type\":\"user\"}";


    }