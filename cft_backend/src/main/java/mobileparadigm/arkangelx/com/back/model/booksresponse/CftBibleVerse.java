package mobileparadigm.arkangelx.com.back.model.booksresponse;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CftBibleVerse extends RealmObject {

    private String verse_title;
    @PrimaryKey
    private long cftBibleVerseId;
    private String verse_detail;
    private String verse_detail_parent;

    public boolean isMorningOrEvening() {
        return morningOrEvening;
    }

    public void setMorningOrEvening(boolean morningOrEvening) {
        this.morningOrEvening = morningOrEvening;
    }

    private boolean morningOrEvening;

    public long getCftBibleVerseId() {
        return cftBibleVerseId;
    }

    public void setCftBibleVerseId(long cftBibleVerseId) {
        this.cftBibleVerseId = cftBibleVerseId;
    }




    public String getVerse_title() {
        return verse_title;
    }

    public void setVerse_title(String verse_title) {
        this.verse_title = verse_title;
    }

    public String getVerse_detail() {
        return verse_detail;
    }

    public void setVerse_detail(String verse_detail) {
        this.verse_detail = verse_detail;
    }

    public String getVerse_detail_parent() {
        return verse_detail_parent;
    }

    public void setVerse_detail_parent(String verse_detail_parent) {
        this.verse_detail_parent = verse_detail_parent;
    }
}