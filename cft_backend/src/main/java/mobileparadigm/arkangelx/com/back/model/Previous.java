package mobileparadigm.arkangelx.com.back.model;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Previous extends BaseObservable implements Parcelable{

    private static final String FIELD_VERSE = "verse";

    @SerializedName(FIELD_VERSE)
    private Verse mVerse;


    public Previous(){

    }

    public void setVerse(Verse verse) {
        mVerse = verse;
    }

    public Verse getVerse() {
        return mVerse;
    }

    public Previous(Parcel in) {
        mVerse = in.readParcelable(Verse.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Previous> CREATOR = new Creator<Previous>() {
        public Previous createFromParcel(Parcel in) {
            return new Previous(in);
        }

        public Previous[] newArray(int size) {
        return new Previous[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mVerse, flags);
    }


}