package mobileparadigm.arkangelx.com.back.model;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Next extends BaseObservable implements Parcelable{

    private static final String FIELD_VERSE = "verse";

    @SerializedName(FIELD_VERSE)
    private Verse mVerse;


    public Next(){

    }

    public void setVerse(Verse verse) {
        mVerse = verse;
    }

    public Verse getVerse() {
        return mVerse;
    }

    public Next(Parcel in) {
        mVerse = in.readParcelable(Verse.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Next> CREATOR = new Creator<Next>() {
        public Next createFromParcel(Parcel in) {
            return new Next(in);
        }

        public Next[] newArray(int size) {
        return new Next[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mVerse, flags);
    }


}