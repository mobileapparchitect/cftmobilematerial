package mobileparadigm.arkangelx.com.back.model;

/**
 * Created by jakubszczygiel on 06/01/15.
 */
public class EntityBodyImageRequest {
    public String path;
    public String bodyName;
    public String pictureName;
    public int maxWidth;
    public int maxHeight;

    public EntityBodyImageRequest(String _path, String _bodyName, String _pictureName, int maxWidth, int maxHeight) {
        path = _path;
        bodyName = _bodyName;
        pictureName = _pictureName;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }
}
