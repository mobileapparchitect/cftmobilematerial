package mobileparadigm.arkangelx.com.back.model.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Root  implements Parcelable{

    private static final String FIELD_RESPONSE = "response";

    @SerializedName("response")
    private Response mResponse;



    public void setResponse(Response response) {
        mResponse = response;
    }

    public Response getResponse() {
        return mResponse;
    }

    public Root(Parcel in) {
        mResponse = in.readParcelable(Response.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Root> CREATOR = new Creator<Root>() {
        public Root createFromParcel(Parcel in) {
            return new Root(in);
        }

        public Root[] newArray(int size) {
        return new Root[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mResponse, flags);
    }


}