package com.mobileparadigm.arkangel.cftmobile.utils;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verse;
import com.mobileparadigm.arkangel.cftmobile.data_models.firebase.Announcement;

import java.util.ArrayList;


/**
 * Created by arkangel on 08/12/2015.
 */
public class CftDataHolder {

    private static ArrayList<CalendarEvent> calendarEventArrayList;
    private static ArrayList<Verse> verseArrayList;
    private static CftDataHolder cftDataHolderInstance;
    private static ArrayList<GlideDrawable> listOfHomeViewPagerDrawables;

    public  Announcement getAnnouncement() {
        return announcement;
    }

    public  void setAnnouncement(Announcement announcement) {
        CftDataHolder.announcement = announcement;
    }

    public static Announcement announcement;

    public static CftDataHolder getInstance() {
        if (cftDataHolderInstance == null) {
            cftDataHolderInstance = new CftDataHolder();
            listOfHomeViewPagerDrawables = new ArrayList<>();
            calendarEventArrayList = new ArrayList<>();
            verseArrayList = new ArrayList<>();
        }
        return cftDataHolderInstance;
    }

    public ArrayList<GlideDrawable> getListOfHomeViewPagerDrawables() {
        return listOfHomeViewPagerDrawables;
    }

    public void setListOfHomeViewPagerDrawables(ArrayList<GlideDrawable> listOfHomeViewPagerDrawables) {
        CftDataHolder.listOfHomeViewPagerDrawables = listOfHomeViewPagerDrawables;
    }

    public ArrayList<CalendarEvent> getCalendarEventArrayList() {
        return calendarEventArrayList;
    }

    public void setCalendarEventArrayList(ArrayList<CalendarEvent> calendarEventArrayList) {
        CftDataHolder.calendarEventArrayList = calendarEventArrayList;
    }

    public ArrayList<Verse> getVerseArrayList() {
        return verseArrayList;
    }

    public void setVerseArrayList(ArrayList<Verse> verseArrayList) {
        CftDataHolder.verseArrayList = verseArrayList;
    }


}
