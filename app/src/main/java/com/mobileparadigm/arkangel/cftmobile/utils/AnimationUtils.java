package com.mobileparadigm.arkangel.cftmobile.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.mobileparadigm.arkangel.cftmobile.R;


/**
 * Created by arkangel on 17/04/15.
 */
public class AnimationUtils {

    public static Animation expand(final View v, final int suggestedHeight, long time, Animation.AnimationListener listener) {
        final int targetHeight;
        if (suggestedHeight == 0) {
            v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            targetHeight = v.getMeasuredHeight();
        } else {
            targetHeight = suggestedHeight;
        }
        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            public boolean willChangeBounds() {
                return true;
            }

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 && suggestedHeight == 0
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }
        };
        a.setAnimationListener(listener);
        a.setDuration(time);
        v.startAnimation(a);
        return a;
    }

    public static Animation expandDownwards(final View v, final int suggestedHeight, long time, Animation.AnimationListener listener) {
        final int targetHeight;
        if (suggestedHeight == 0) {
            v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            targetHeight = v.getMeasuredHeight();
        } else {
            targetHeight = suggestedHeight;
        }
        v.getLayoutParams().height = 0;
        //  v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            public boolean willChangeBounds() {
                return true;
            }

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 && suggestedHeight == 0
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }
        };
        a.setAnimationListener(listener);
        a.setDuration(time);
        v.startAnimation(a);
        return a;
    }

    public static Animation collapse(final View v, long time, Animation.AnimationListener listener) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        a.setAnimationListener(listener);
        a.setDuration(time);
        v.startAnimation(a);
        return a;
    }

    public static void alternateViewHolderBackground(final View view, long time, Animation.AnimationListener listener) {
        view.setAlpha(0.0f);
        final AlphaAnimation alpha = new AlphaAnimation(1.0F, 0.0F);
        alpha.setDuration(time); // Make animation instant
        // alpha.setFillAfter(true); // Tell it to persist after the animation ends
// And then on your layout
        alpha.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                alpha.setFillAfter(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(alpha);
    }

    public static void alternateFade(final View view, long time, final boolean alphaSet) {
        AlphaAnimation anim = null;
        if (alphaSet) {
            anim = new AlphaAnimation(1.0f, 0.0f);
        } else {
            anim = new AlphaAnimation(0.0f, 1.0f);
        }
        Animation.AnimationListener listener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (alphaSet) {
                    view.setVisibility(View.GONE);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        };
        anim.setDuration(time);
        anim.setAnimationListener(listener);
        //  anim.setInterpolator(new LinearInterpolator());
        view.startAnimation(anim);
    }

}
