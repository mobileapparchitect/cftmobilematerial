package com.mobileparadigm.arkangel.cftmobile.ridesharing.utils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Passenger object
 * Attributes:
 * 	String 		     name
 *  Point2D.Double   origin
 *  Point2D.Double   destination
 * @author Debosmit
 *
 */
public class Passenger {

	private String UID;
	private LatLng origin;
	private LatLng destination;
	
	public Passenger(String name) {
		this(name, (LatLng)null, (LatLng)null);
	}
	
	public Passenger(String name, LatLng origin, LatLng destination) {
		this.setUID(name);
		this.setOrigin(origin);
		this.setDestination(destination);
	}
	
	public LatLng getOrigin() {
		return origin;
	}

	public void setOrigin(LatLng origin) {
		this.origin = origin;
	}

	public LatLng getDestination() {
		return destination;
	}

	public void setDestination(LatLng destination) {
		this.destination = destination;
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}	
}
