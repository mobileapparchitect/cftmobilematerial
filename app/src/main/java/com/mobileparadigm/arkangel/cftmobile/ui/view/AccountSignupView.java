package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;
import com.mobileparadigm.arkangel.cftmobile.utils.DeviceUtils;


public class AccountSignupView extends RelativeLayout {
    private StartActivity.ShowNextEvent showNextEvent;
    private Activity parent;

    public AccountSignupView(Context context) {
        super(context);
    }

    public AccountSignupView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AccountSignupView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initView(Activity parent, int bgResourceId) {
        this.setLayoutParams(new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        this.parent = parent;
        int bgResourceId1 = bgResourceId;
        showNextEvent = new StartActivity.ShowNextEvent();
        Button emailButton = (Button) findViewById(R.id.signupChoiceEmailButton);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startLoginWithEmailActivity();
            }
        });
        Button facebookButton = (Button) findViewById(R.id.signupChoiceFacebookButton);
        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startLoginWithFacebook();
            }
        });
        Button twitterButton = (Button) findViewById(R.id.signupChoiceTwitterButton);
        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startLoginWithTwitter();
            }
        });
        Button googleButton = (Button) findViewById(R.id.signupChoiceGoogleButton);
        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoginWithGoogle();
            }
        });
    }

    private void startLoginWithGoogle() {
        if (DeviceUtils.isConnected(getContext())) {

        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }

    private void startLoginWithEmailActivity() {
        if (DeviceUtils.isConnected(getContext())) {
            showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_SHOW_EMAIL_SIGN_UP);
            //EventBusProvider.getInstance().post(showNextEvent);

        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }

    private void startLoginWithFacebook() {
        if (DeviceUtils.isConnected(getContext())) {
            if (DeviceUtils.isConnected(getContext())) {
                showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_LOGIN_WITH_FACEBOOK);
           //     EventBusProvider.getInstance().post(showNextEvent);

            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }

    private void startLoginWithTwitter() {
        if (DeviceUtils.isConnected(getContext())) {
            showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_LOGIN_WITH_TWITTER);
            //EventBusProvider.getInstance().post(showNextEvent);

        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }


}
