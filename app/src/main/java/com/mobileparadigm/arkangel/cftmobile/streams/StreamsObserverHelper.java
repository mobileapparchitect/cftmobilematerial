package com.mobileparadigm.arkangel.cftmobile.streams;

import android.text.TextUtils;

import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Book;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.BookTable;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Root;
import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.mobileparadigm.arkangel.cftmobile.utils.VerseProcessor;

import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import rx.Observable;
import rx.functions.Func1;
import timber.log.Timber;

/**
 * Created by arkangel on 30/07/16.
 */
public class StreamsObserverHelper {


    public static Func1<Root, Root> saveBibleData(Root root, Realm realm) {


        return new Func1<Root, Root>() {
            @Override
            public Root call(Root root) {
                List<Book> books = root.getResponse().getBooks();
                Realm jobRealm = realm;
                try {
                    //      jobRealm = Realm.getDefaultInstance();
                    RealmResults<BookTable> results = jobRealm.where(BookTable.class).findAll();
                    jobRealm.beginTransaction();
                    results.clear();
                    jobRealm.commitTransaction();
                    jobRealm.beginTransaction();
                    for (Book book : books) {
                        BookTable bookTable = new BookTable();
                        bookTable.setAbbreviation(book.getAbbr());
                        bookTable.setName(book.getName());
                        bookTable.setBooktableId(new Random().nextLong());
                        jobRealm.copyToRealmOrUpdate(bookTable);
                    }
                    jobRealm.commitTransaction();
                    jobRealm.close();
                    PreferencesHelper.setDatabaseCreated(true);
                } catch (RealmException realmException) {
                    jobRealm.cancelTransaction();
                    jobRealm.close();
                    realmException.printStackTrace();

                }
                return root;
            }
        };

    }

    public static Func1<Boolean, Boolean> savedAnnoucement() {
        return new Func1() {
            @Override
            public Object call(Object o) {
                return null;
            }
        };
    }





}

