package com.mobileparadigm.arkangel.cftmobile.model;

import java.util.ArrayList;
import android.os.Parcelable;
import java.util.List;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CalendarPage implements Parcelable{

    private static final String FIELD_PAGES = "pages";

    @JsonProperty(FIELD_PAGES)
    private List<Pagex> mPages;


    public CalendarPage(){

    }

    public void setPages(List<Pagex> pages) {
        mPages = pages;
    }

    public List<Pagex> getPages() {
        return mPages;
    }

    public CalendarPage(Parcel in) {
        mPages = new ArrayList<Pagex>();
        in.readTypedList(mPages, Pagex.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CalendarPage> CREATOR = new Creator<CalendarPage>() {
        public CalendarPage createFromParcel(Parcel in) {
            return new CalendarPage(in);
        }

        public CalendarPage[] newArray(int size) {
        return new CalendarPage[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mPages);
    }


}