package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.adapters.ExpandableListAdapter;
import com.mobileparadigm.arkangel.cftmobile.adapters.ExpandedMenuModel;
import com.mobileparadigm.arkangel.cftmobile.adapters.SubMenuModel;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.AcknowledgementFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.BibleStudyFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.CarouselFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.CftWebViewFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.HighLightsFragment;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainMenuActivity extends BaseActivity {

    private static final int ANIM_DURATION_TOOLBAR = 300;
    private static final int ANIM_DURATION_FAB = 400;
    private static AppCompatActivity context;
    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;
    ExpandableListAdapter mMenuAdapter;
    @BindView(R.id.navigationmenu)
    ExpandableListView expandableList;
    List<ExpandedMenuModel> listDataHeader;
    HashMap<ExpandedMenuModel, List<SubMenuModel>> listDataChild;
    @BindView(R.id.vNavigation)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @Inject
    Bus postFromAnyThreadBus;
    private android.support.v4.app.FragmentTransaction fragmentTransaction;
    private List listOfUrls;
    private boolean pendingIntroAnimation;


    @Inject
    SharedPreferences prefs;
    public static void startMainMenuActivity(AppCompatActivity callingActivity) {
        callingActivity.startActivity(new Intent(callingActivity, MainMenuActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);
        context = this;
        CFTMaterialApplication.getAppComponent().inject(this);
        postFromAnyThreadBus.register(this);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        listOfUrls = Arrays.asList(CFTMaterialApplication.getInstance().getResources().getStringArray(R.array.social_media_urls));
        setupDrawerContent(navigationView);
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild, expandableList);

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);
        setDrawer(drawerLayout);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                getSupportFragmentManager().beginTransaction().replace(R.id.flContentRoot, CarouselFragment.getInstance()).addToBackStack("Carousel").commit();
                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressBar.setVisibility(View.GONE);
            }
        }.execute();
       /* if (CFTMaterialApplication.myFirebaseRef.getAuth().getToken() == null) {
            Toast.makeText(MainMenuActivity.this, "Not Logged In", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainMenuActivity.this, "Logged In", Toast.LENGTH_LONG).show();
            AuthData auth = CFTMaterialApplication.myFirebaseRef.getAuth();
            CFTMaterialApplication.myFirebaseRef.child("Users").child(auth.getUid()).setValue(auth);
        }*/
    }

    public void setDrawer(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
        final ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //NAVIGATION
        final View.OnClickListener originalToolbarListener = actionBarDrawerToggle.getToolbarNavigationClickListener();

        getSupportFragmentManager().addOnBackStackChangedListener(new android.support.v4.app.FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportFragmentManager().popBackStack();
                        }
                    });
                } else {
                    actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                    actionBarDrawerToggle.setToolbarNavigationClickListener(originalToolbarListener);
                }
            }
        });
    }

    private void setupDrawerContent(NavigationView navigationView) {
        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                parent.setItemChecked(index, true);
                ExpandedMenuModel object = listDataHeader.get(groupPosition);
                SubMenuModel submenuObject = listDataChild.get(object).get(childPosition);
                switch (submenuObject.getMenu_id()) {
                    case APOSTLE_WEBSITE:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.flContentRoot, CftWebViewFragment.getInstance((String) listOfUrls.get(2)));
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                    case ASSISTANCE:
                        drawerLayout.closeDrawers();
                        return true;
                    case BIBLE_STUDY:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.flContentRoot, BibleStudyFragment.getInstance());
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                    case FACEBOOK:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.flContentRoot, CftWebViewFragment.getInstance((String) listOfUrls.get(0)));
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                    case TWITTER:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.flContentRoot, CftWebViewFragment.getInstance((String) listOfUrls.get(3)));
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                    case PRAYER_REQUESTS:
                        drawerLayout.closeDrawers();
                        return true;
                    case CREDITS:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.add(R.id.flContentRoot, AcknowledgementFragment.getInstance());
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                    case DISCIPLESHIP:
                        drawerLayout.closeDrawers();
                        return true;
                    case YOUTUBE:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.flContentRoot, CftWebViewFragment.getInstance((String) listOfUrls.get(4)));
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                    case SETTINGS:
                        drawerLayout.closeDrawers();
                        return true;
                    case CFT_WEBSITE:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.flContentRoot, CftWebViewFragment.getInstance((String) listOfUrls.get(1)));
                        fragmentTransaction.commit();
                        drawerLayout.closeDrawers();
                        return true;
                }


                drawerLayout.closeDrawers();
                return true;
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_inbox:
                        drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                             Fragment fragment= getSupportFragmentManager().findFragmentByTag("Carousel");
                                if(fragment==null){
                                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.flContentRoot, CarouselFragment.getInstance());
                                    fragmentTransaction.commit();
                                }else {
                                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.flContentRoot,fragment);
                                    fragmentTransaction.commit();
                                }

                            }
                        }, 100);

                        return true;
                }
                return false;
            }
        });
    }


    private void prepareListData() {
        listDataHeader = new ArrayList<ExpandedMenuModel>();
        listDataChild = new HashMap<ExpandedMenuModel, List<SubMenuModel>>();

        ExpandedMenuModel item1 = new ExpandedMenuModel();
        item1.setIconName("General");
        item1.setIconImg(R.drawable.ic_dashboard);
        // Adding data header
        // item1.setIconImg(0);
        listDataHeader.add(item1);

        ExpandedMenuModel item2 = new ExpandedMenuModel();
        item2.setIconName("Social Media");
        item2.setIconImg(R.drawable.ic_forum);
        //  item2.setIconImg(0);
        listDataHeader.add(item2);

        ExpandedMenuModel item3 = new ExpandedMenuModel();
        item3.setIconName("About App");
        item3.setIconImg(R.drawable.ic_headset);
        //    item3.setIconImg(0);
        listDataHeader.add(item3);

        // Adding child data
        List<SubMenuModel> heading1 = new ArrayList<SubMenuModel>();
        heading1.add(new SubMenuModel("Prayer Requests", SubMenuModel.MENU_ID.PRAYER_REQUESTS, R.drawable.ic_global_menu_feed));
        heading1.add(new SubMenuModel("Bible Study", SubMenuModel.MENU_ID.BIBLE_STUDY, R.drawable.ic_global_menu_feed));
        heading1.add(new SubMenuModel("Assistance", SubMenuModel.MENU_ID.ASSISTANCE, R.drawable.ic_global_menu_feed));
        heading1.add(new SubMenuModel("DiscipleShip Program", SubMenuModel.MENU_ID.DISCIPLESHIP, R.drawable.ic_global_menu_feed));


        List<SubMenuModel> heading2 = new ArrayList<SubMenuModel>();
        heading2.add(new SubMenuModel("Facebook", SubMenuModel.MENU_ID.FACEBOOK, R.drawable.ic_global_menu_feed));
        heading2.add(new SubMenuModel("Twitter", SubMenuModel.MENU_ID.TWITTER, R.drawable.ic_global_menu_feed));
        heading2.add(new SubMenuModel("CFT Website", SubMenuModel.MENU_ID.CFT_WEBSITE, R.drawable.ic_global_menu_feed));
        heading2.add(new SubMenuModel("YouTube", SubMenuModel.MENU_ID.YOUTUBE, R.drawable.ic_global_menu_feed));
        heading2.add(new SubMenuModel("Apostle's Website", SubMenuModel.MENU_ID.APOSTLE_WEBSITE, R.drawable.ic_global_menu_feed));


        List<SubMenuModel> heading3 = new ArrayList<SubMenuModel>();
        heading3.add(new SubMenuModel("Settings", SubMenuModel.MENU_ID.SETTINGS, R.drawable.ic_global_menu_feed));
        heading3.add(new SubMenuModel("Credits", SubMenuModel.MENU_ID.CREDITS, R.drawable.ic_global_menu_feed));


        listDataChild.put(listDataHeader.get(0), heading1);// Header, Child data
        listDataChild.put(listDataHeader.get(1), heading2);
        listDataChild.put(listDataHeader.get(2), heading3);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        postFromAnyThreadBus.unregister(this);


    }

    @Subscribe
    public void receiveEvent(HighLightsFragment.SendEvent sendEvent) {
        DetailActivity.startDetailsActivity(MainMenuActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (pendingIntroAnimation) {
            pendingIntroAnimation = false;
            startIntroAnimation();
        }
        return true;
    }

    private void startIntroAnimation() {

        int actionbarSize = Utils.dpToPx(56);
       // getToolbar().setTranslationY(-actionbarSize);
//        getIvLogo().setTranslationY(-actionbarSize);
        /*
        getInboxMenuItem().getActionView().setTranslationY(-actionbarSize);

        getToolbar().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(300);
        getIvLogo().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(400);
        getInboxMenuItem().getActionView().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        //    startContentAnimation();
                    }
                })
                .start();*/
    }
}
