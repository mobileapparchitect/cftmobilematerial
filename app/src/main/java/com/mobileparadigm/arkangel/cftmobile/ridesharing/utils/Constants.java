package com.mobileparadigm.arkangel.cftmobile.ridesharing.utils;

import java.io.File;
import java.util.Scanner;

public class Constants {

	// Seattle bounds
	// source: https://www.maptechnica.com/us-city-boundary-map/city/Seattle/state/WA/cityid/5363000
	private static final double[] LAT = {47.48172, 47.734145};
	private static final double[] LONG = {-122.151602, -122.419866};

	
	// for Google 
	private static final String key = "AIzaSyC1HjeUOJQ85jyY0BtitX-Sgv8Ao14JyAE";
	private static final String mode = "driving";
	
	// DEBUG mode
	private static final boolean DEBUG = true;
	
	public static double getMinimumLatitude() {
		return LAT[0];
	}
	
	public static double getMinimumLongitude() {
		return LONG[0];
	}
	
	public static double getMaximumLatitude() {
		return LAT[1];
	}
	
	public static double getMaximumLongitude() {
		return LONG[1];
	}
	
	public static String getMode() {
		return mode;
	}
	
	public static String getKey() {
		if(key.compareTo("INSERT YOUR GOOGLE DIRECTIONS API KEY") == 0) {
			String text = "";
			Scanner scanner = null;
			try {
				scanner = new Scanner( new File("GoogleAPIKey.txt"), "UTF-8" );
				text = scanner.useDelimiter("\\A").next();
				
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				scanner.close();
			}
			return text;
		}
		return key;
	}
	
	public static boolean isDebugMode() {
		return DEBUG;
	}
	
}
