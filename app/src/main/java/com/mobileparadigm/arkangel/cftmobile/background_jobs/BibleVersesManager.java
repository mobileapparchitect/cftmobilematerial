package com.mobileparadigm.arkangel.cftmobile.background_jobs;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;


/**
 * Created by arkangel on 28/12/2015.
 */
public class BibleVersesManager {

    public static BibleVersesManager bibleVersesManagerInstance;
    public static Stack<ArrayList<String>> listOfStackedVersed;

    public static Stack<ArrayList<String>> getListOfStackedVersed() {
        return listOfStackedVersed;
    }

    public static void setListOfStackedVersed(Stack<ArrayList<String>> listOfStackedVersed) {
        BibleVersesManager.listOfStackedVersed = listOfStackedVersed;
    }

    public static BibleVersesManager getInstance() {
        if (bibleVersesManagerInstance == null) {
            bibleVersesManagerInstance = new BibleVersesManager();
            listOfStackedVersed = new Stack<>();
            createBlocksOfVerses();
        }

        return bibleVersesManagerInstance;
    }

    private static void createBlocksOfVerses() {
        List<String> listOfVerses = Arrays.asList(CFTMaterialApplication.getInstance().getResources().getStringArray(R.array.cft_verses));
        int leftPointer = 0;
        for (int count = 0; count < listOfVerses.size(); count++) {
            if (count % 5 == 0 && count != leftPointer) {
                listOfStackedVersed.push(new ArrayList<String>(listOfVerses.subList(leftPointer, count)));
                leftPointer = count;
            } else {
                continue;
            }
        }

    }
}
