package com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_presenter;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_views.InitializationView;

public interface InitializationPresenter extends MvpPresenter<InitializationView> {
    void loadData();
}