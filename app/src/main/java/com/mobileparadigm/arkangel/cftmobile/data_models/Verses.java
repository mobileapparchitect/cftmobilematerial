package com.mobileparadigm.arkangel.cftmobile.data_models;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Verses extends BaseObservable implements Parcelable{

    private static final String FIELD_RESPONSE = "response";

    @SerializedName(FIELD_RESPONSE)
    private Response mResponse;


    public Verses(){

    }

    public void setResponse(Response response) {
        mResponse = response;
    }

    public Response getResponse() {
        return mResponse;
    }

    public Verses(Parcel in) {
        mResponse = in.readParcelable(Response.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Verses> CREATOR = new Creator<Verses>() {
        public Verses createFromParcel(Parcel in) {
            return new Verses(in);
        }

        public Verses[] newArray(int size) {
        return new Verses[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mResponse, flags);
    }


}