package com.mobileparadigm.arkangel.cftmobile.data_access_layer;

import android.support.annotation.Nullable;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verses;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Book;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.BookTable;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Root;
import com.mobileparadigm.arkangel.cftmobile.data_models.firebase.Announcement;
import com.mobileparadigm.arkangel.cftmobile.data_models.youtube.YoutubeVideosContent;
import com.mobileparadigm.arkangel.cftmobile.endpoints.BooksApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.VersesApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.YoutubeService;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.mobileparadigm.arkangel.cftmobile.utils.VerseProcessor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import io.socket.client.Socket;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

/**
 * Created by arkangel on 23/07/16.
 */
public class DataAccessManager {

    @Inject
    Socket mSocket;
    @Inject
    Gson gsonDeserializer;
    @Inject
    Realm realm;
    @Inject
    BooksApiService booksApiService;
    @Inject
    VerseProcessor verseProcessor;
    @Inject
    YoutubeService youtubeService;
    @Inject
    Firebase firebaseService;

    @Inject
    VersesApiService versesService;
    private String UID = "-KM5ixm5pKDA2mb5IJ0G";

    private CompositeSubscription compositeSubscription;

    //need a subject here
    PublishSubject publishSubject;

    public DataAccessManager() {
        CFTMaterialApplication.getAppComponent().inject(this);
        compositeSubscription = new CompositeSubscription();
        mSocket.on(Socket.EVENT_CONNECT, args3 -> {
            Timber.i("Websocket Connected");
        });
        mSocket.on(Socket.EVENT_DISCONNECT, args2 -> {
            Timber.i("Websocket DisConnected");
        });
        mSocket.on(Socket.EVENT_CONNECT_ERROR, args1 -> {
            Timber.i("Websocket Connected Error");
        });
        mSocket.on(Socket.EVENT_CONNECT_ERROR, args -> {
            Timber.i("Websocket Connected Error");
        });
  /*      mSocket.on("command_get_bibles", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                Timber.i(" bibles retrieve : " + data.toString());
                Root bibleRootObject = gsonDeserializer.fromJson(data.toString(), Root.class);
                Timber.i(" bibles retrieved data: " + bibleRootObject.toString());
                //  processBibleBooks(bibleRootObject); changed to observable
                getRootObservable(bibleRootObject).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Root>() {
                    @Override
                    public void onCompleted() {
                        Timber.i("Bibles processed on Observable");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Root root) {
                        Timber.i("Bibles processed on Observable");
                    }
                });

            }

        });*/
        mSocket.on("command_get_youtubeContent", args -> {
            JSONObject data = (JSONObject) args[0];
            Timber.i(" Got youtube from websocket  : " + data.toString());
            YoutubeVideosContent youtubeVideosContent = gsonDeserializer.fromJson(data.toString(), YoutubeVideosContent.class);
            Timber.i(" YoutubeContent retrieved data: " + youtubeVideosContent.toString());
            CFTMaterialApplication.youtubeVideosContent = youtubeVideosContent;
        });
        mSocket.connect();
        doAmazingThingObservable().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Subscriber<Root>() {
            @Override
            public void onCompleted() {
                Timber.i("Timer");
            }

            @Override
            public void onError(Throwable e) {
                Timber.i("Timer");
            }

            @Override
            public void onNext(Root root) {
                Timber.i("TimerRoot");
            }
        });
    }

    private Observable<Root> getRootObservable(final Root root) {
        return Observable.defer(() -> {
            return Observable.just(processBibleBooks(root));
        });

    }

    private Root processBibleBooks(Root root) {
        List<Book> books = root.getResponse().getBooks();
        try {
            RealmResults<BookTable> results = realm.where(BookTable.class).findAll();
            realm.beginTransaction();
            results.clear();
            realm.commitTransaction();
            realm.beginTransaction();
            for (Book book : books) {
                BookTable bookTable = new BookTable();
                bookTable.setAbbreviation(book.getAbbr());
                bookTable.setName(book.getName());
                bookTable.setBooktableId(new Random().nextLong());
                realm.copyToRealmOrUpdate(bookTable);
            }
            realm.commitTransaction();
            realm.close();
            PreferencesHelper.setDatabaseCreated(true);
        } catch (RealmException realmException) {
            realm.cancelTransaction();
            realm.close();
            realmException.printStackTrace();
        }
        return root;
    }


    public Observable<Boolean> getAnnoucementObservable() {

        return Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                return Observable.just(getAnnouncement());
            }
        });
    }


    public void processVerses() {
        versesService.getVerses(PreferencesHelper.getUserBibleVersion(), "1", "1")
                .doOnSubscribe(() -> {  /* starting request */
                    Timber.e("Starting");
                })
                .doOnCompleted(() -> { /* finished request */
                    Timber.e("Finish");
                })
                // request webservice for each dog id with all info
                .flatMap(dogId -> versesService.getVerses(PreferencesHelper.getUserBibleVersion(), "1", "1")
                        .doOnError(throwable -> {
                            Timber.e(throwable.getMessage());
                        }).subscribeOn(Schedulers.io()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Verses>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Verses verses) {

                    }
                });
    }

    @Nullable
    public Boolean getAnnouncement() {

        if (CftDataHolder.getInstance().getAnnouncement() != null) {
            return true;
        } else {
            firebaseService.child("announcement").orderByChild(UID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    Log.v("DownloadBulletin", snapshot.toString());
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        if (postSnapshot.getKey().equals(UID)) {
                            CftDataHolder.getInstance().setAnnouncement(postSnapshot.child("Announcement").getValue(Announcement.class));
                        }
                    }

                }

                @Override
                public void onCancelled(FirebaseError error) {
                    Log.v("DownloadBulletin", error.toString());
                }

            });

        }
        return true;
    }

    public Observable<Boolean> getYoutubeContentObservable() {
        return Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                return Observable.just(getYoutubeContent());
            }
        });
    }

    public Boolean getYoutubeContent() {

        if (mSocket.connected()) {
            try {
                JSONObject loginPayload = new JSONObject();
                loginPayload.put("URL", "  https://www.googleapis.com/youtube/v3/videos?chart=mostPopular&part=snippet&forUsername=christfaithtabernaclelondon&key=AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ");
                mSocket.emit("command_get_youtubeContent", loginPayload);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {

            youtubeService.getYoutube("mostPopular", "snippet", "christfaithtabernaclelondon", "AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ")
                    .subscribeOn(Schedulers.io())//do the work on this thread
                    .observeOn(AndroidSchedulers.mainThread())//return the result on
                    .subscribe(new Subscriber<YoutubeVideosContent>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(YoutubeVideosContent youtubeVideosContent) {
                            Timber.i("Got youtube: " + youtubeVideosContent.toString());
                            CFTMaterialApplication.youtubeVideosContent = youtubeVideosContent;
                        }
                    });
        }
        return true;
    }

    public Observable<Boolean> getBibleDataObservable() {

        return Observable.defer(() -> {
            return Observable.just(getBibleData());
        });

    }

    public Boolean getBibleData() {
        if (mSocket.connected()) {
            try {
                JSONObject loginPayload = new JSONObject();
                loginPayload.put("URL", "http://bibles.org/versions/eng-KJVA/books.js");
                mSocket.emit("command_get_bibles", loginPayload);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
          /*  loadBibleBooksSubscription =*/
            booksApiService.getBooks(PreferencesHelper.getUserBibleVersion())
                    .subscribeOn(Schedulers.io())//do the work on this thread
                    .observeOn(AndroidSchedulers.mainThread())//return the result on
                    .subscribe(new Subscriber<Root>() {
                        //oncomplete and onError mutex. only 1 gets called
                        @Override
                        public void onCompleted() {
                            Timber.i("complete");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(e, e.getMessage() + "completed with error");

                        }

                        @Override
                        public void onNext(Root root) {
                            Timber.i("Bible Retrieval completed");
                        }


                    });
        }
        return true;
    }


    public CompositeSubscription getCompositeSubscription() {
        return compositeSubscription;
    }

    public void loadData() {

        compositeSubscription.add(getBibleDataObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {
                Timber.i("Composite observable bible COMPLETES");
            }

            @Override
            public void onError(Throwable e) {
                Timber.i("Composite observable bible ERROR");
            }

            @Override
            public void onNext(Boolean aBoolean) {
                Timber.i("Composite observable bible FREE");
            }
        }));

        compositeSubscription.add(getYoutubeContentObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {
                Timber.i("Composite observable bible COMPLEATED");
            }

            @Override
            public void onError(Throwable e) {
                Timber.i("Composite observable bible ERROR");
            }

            @Override
            public void onNext(Boolean aBoolean) {
                Timber.i("Composite observable bible FREE");
            }
        }));

        compositeSubscription.add(getAnnoucementObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {
                Timber.i("Composite observable bible FREE");
            }

            @Override
            public void onError(Throwable e) {
                Timber.i("Composite observable bible FREE");
            }

            @Override
            public void onNext(Boolean aBoolean) {
                Timber.i("Composite observable bible FREE");
            }
        }));


     /*   compositeSubscription.add(youtubeService.getYoutube("mostPopular", "snippet", "christfaithtabernaclelondon", "AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ")
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .subscribe(new Subscriber<YoutubeVideosContent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(YoutubeVideosContent youtubeVideosContent) {
                        Timber.i("Got youtube: " + youtubeVideosContent.toString());
                        CFTMaterialApplication.youtubeVideosContent = youtubeVideosContent;
                    }
                }));*/

        ///compositeSubscription.add(getAnnoucementObservable());


    }

    public Observable<Root> doAmazingThingObservable() {


        return Observable.create(subscriber -> {

            if (mSocket.connected()) {
                try {
                    JSONObject loginPayload = new JSONObject();
                    loginPayload.put("URL", "http://bibles.org/versions/eng-KJVA/books.js");
                    mSocket.emit("command_get_bibles", loginPayload).on("command_get_bibles", args -> {
                        JSONObject data = (JSONObject) args[0];
                        Timber.i(" bibles retrieve : " + data.toString());
                        Root bibleRootObject = gsonDeserializer.fromJson(data.toString(), Root.class);
                        Timber.i(" bibles retrieved data: " + bibleRootObject.toString());
                        subscriber.onNext(bibleRootObject);
                        //  processBibleBooks(bibleRootObject); changed to observable
                        getRootObservable(bibleRootObject).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Root>() {
                            @Override
                            public void onCompleted() {
                                Timber.i("Bibles processed on Observable");
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Root root) {
                                Timber.i("Bibles processed on Observable");
                            }
                        });
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


        });
    }
}
