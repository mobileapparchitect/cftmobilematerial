
package com.mobileparadigm.arkangel.cftmobile.data_models.firebase;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AnnouncementDetails {

    @JsonProperty("date")
    private String date;
    @JsonProperty("detail")
    private String detail;
    @JsonProperty("title")
    private String title;
    @JsonProperty("yearTheme")
    private String yearTheme;

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The detail
     */
    @JsonProperty("detail")
    public String getDetail() {
        return detail;
    }

    /**
     * 
     * @param detail
     *     The detail
     */
    @JsonProperty("detail")
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The yearTheme
     */
    @JsonProperty("yearTheme")
    public String getYearTheme() {
        return yearTheme;
    }

    /**
     * 
     * @param yearTheme
     *     The yearTheme
     */
    @JsonProperty("yearTheme")
    public void setYearTheme(String yearTheme) {
        this.yearTheme = yearTheme;
    }

}
