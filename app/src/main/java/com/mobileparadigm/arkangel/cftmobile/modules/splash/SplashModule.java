package com.mobileparadigm.arkangel.cftmobile.modules.splash;

import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Named;

import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import rx.AsyncEmitter;
import rx.Observable;
import rx.Single;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

@Module
public class SplashModule {
  public static final String SPLASH_ACTIVITY = "SplashActivity";
  public static final String OBSERVABLE_SPLASH_LIBRARY = "observable_splash_library";
  public static final String OBSERVABLE_SPLASH_LIBRARY_ASYNC = "observable_splash_library_async";

  public SplashModule(){

  }
  @Provides
  @NonNull @SplashScope @Named(SPLASH_ACTIVITY)
  public AtomicBoolean initialized() {
    return new AtomicBoolean(false);
  }

  @Provides
  @NonNull @SplashScope
  public SplashLibrary splashLibrary() {
    return new SplashLibrary();
  }

  @Provides
  @NonNull @SplashScope @Named(OBSERVABLE_SPLASH_LIBRARY)
  public Observable<SplashLibrary> splashLibraryObservable(final Lazy<SplashLibrary> splashLazy) {
    return Observable.defer(() -> {return Observable.just(splashLazy.get());});
  }

  /** Other possible way to initialize {@link SplashLibrary}. */
  @Provides
  @NonNull @SplashScope @Named(OBSERVABLE_SPLASH_LIBRARY_ASYNC)
  public Observable<SplashLibrary> splashLibraryObservableAsync(final Lazy<SplashLibrary> splashLazy) {
    return Observable.fromAsync(new Action1<AsyncEmitter<SplashLibrary>>() {
      @Override public void call(AsyncEmitter<SplashLibrary> emitter) {
        emitter.onNext(splashLazy.get());
      }
    }, AsyncEmitter.BackpressureMode.NONE).subscribeOn(Schedulers.computation());
  }

  /** Other possible way to initialize {@link SplashLibrary}. */
  @Provides
  @NonNull @SplashScope
  public Single<SplashLibrary> splashLibrarySingle(final Lazy<SplashLibrary> splashLazy) {
    return Single.defer(() -> {  return Single.just(splashLazy.get());});
  }
}
