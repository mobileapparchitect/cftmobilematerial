package com.mobileparadigm.arkangel.cftmobile.eventbus;


import com.mobileparadigm.arkangel.cftmobile.receivers.NetworkConnectivityReceiver;

/**
 * Created by joseprodriguez on 09/02/16.
 */
public class BusManager {

    private static final BusManager sInstance = new BusManager();
    private final RxBus<NetworkConnectivityReceiver.NetworkStatus> mNetworkConnectivityChangeEventBus = new RxBus<>();

    public static BusManager getInstance() {
        return sInstance;
    }

    /**
     * NOTE: If the password is expired, the login event is still fired.
     *
     * @return the login event bus.
     */

    public RxBus<NetworkConnectivityReceiver.NetworkStatus> getNetworkConnectivityChangeEventBus() {
        return mNetworkConnectivityChangeEventBus;
    }

    // Setters only for testing purposes. Do NOT use them from the app itself.


}
