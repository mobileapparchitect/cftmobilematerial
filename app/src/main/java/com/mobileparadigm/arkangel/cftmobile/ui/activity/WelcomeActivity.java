package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.ScrollView;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.TosFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.VisionFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.WelcomeFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;


public class WelcomeActivity extends AppCompatActivity implements WelcomeFragment.WelcomeFragmentContainer {

    WelcomeActivityContent mContentFragment;
    private static FragmentManager fragmentManager;
    private static Stack welcomeFragmentStack;
    private static ScrollView scrollView;
    /**
     * Get the current fragment to display.
     * <p>
     * This is the first fragment in the list that WelcomeActivityContent.shouldDisplay().
     *
     * @param context the application context.
     * @return the WelcomeActivityContent to display or null if there's none.
     */
/*    private static WelcomeActivityContent getCurrentFragment(Context context) {
        List<WelcomeActivityContent> welcomeActivityContents = getWelcomeFragments();
        welcomeFragmentStack.addAll( welcomeActivityContents);
        *//*for (WelcomeActivityContent fragment : welcomeActivityContents) {
            if (!fragment.shouldDisplay(context)) {
                return fragment;
            }
        }
*//*
        return null;
    }*/

   /* private void setupAnimation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ImageView iv = (ImageView) findViewById(R.id.logo);
            AnimatedVectorDrawable logoAnim = (AnimatedVectorDrawable) getDrawable(R.drawable.io_logo_white_anim);
            iv.setImageDrawable(logoAnim);
            logoAnim.start();
        }§
    }*/

    /**
     * Whether to display the WelcomeActivity.
     * <p>
     * Decided whether any of the fragments need to be displayed.
     *
     * @param context the application context.
     * @return true if the activity should be displayed.
     */
    public static boolean shouldDisplay(Context context) {
       // WelcomeActivityContent fragment = getCurrentFragment(context);
        if (welcomeFragmentStack.isEmpty()) {
            return false;
        }
        return true;
    }

    public static void startWelcomeActivity(AppCompatActivity callingActivity) {
        callingActivity.startActivity(new Intent(callingActivity, WelcomeActivity.class));
    }
    private static List<WelcomeActivityContent> getWelcomeFragments() {
        return new ArrayList<WelcomeActivityContent>(Arrays.asList(
               new VisionFragment(), new TosFragment()
        ));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_layout);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        welcomeFragmentStack = new Stack<>();
      // getCurrentFragment(this);
        welcomeFragmentStack.addAll(getWelcomeFragments());
        mContentFragment = (WelcomeActivityContent) welcomeFragmentStack.pop();
        // If there's no fragment to use, we're done here.
        if (mContentFragment == null) {
            finish();
            return;
        }
        fragmentManager = getSupportFragmentManager();

        // Wire up the fragment
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.welcome_content, ( android.support.v4.app.Fragment) mContentFragment);
        fragmentTransaction.commit();
        setScrollViewToTop();

        //  setupAnimation();
    }

    public static void showNextFragment() {

        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.welcome_content, (android.support.v4.app.Fragment) welcomeFragmentStack.pop());
        fragmentTransaction.commit();
        setScrollViewToTop();
    }



    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    private static void setScrollViewToTop() {
        scrollView.post(new Runnable() {
            public void run() {
                scrollView.scrollTo(0,
                        scrollView.getMaxScrollAmount() - 2*CFTMaterialApplication.getInstance().getResources().getDimensionPixelSize(R.dimen.welcome_content_padding_top));
            }
        });
    }
    @Override
    public Button getPositiveButton() {
        return (Button) findViewById(R.id.button_accept);
    }

    @Override
    public void setPositiveButtonEnabled(Boolean enabled) {
        try {
            getPositiveButton().setEnabled(enabled);
        } catch (NullPointerException e) {
            //   LOGD(TAG, "Positive welcome button doesn't exist to set enabled.");
        }
    }

    @Override
    public Button getNegativeButton() {
        return (Button) findViewById(R.id.button_decline);
    }

    @Override
    public void setNegativeButtonEnabled(Boolean enabled) {
        try {
            getNegativeButton().setEnabled(enabled);
        } catch (NullPointerException e) {
            //   LOGD(TAG, "Negative welcome button doesn't exist to set enabled.");
        }
    }

    /**
     * The definition of a Fragment for a use in the WelcomeActivity.
     */
    public interface WelcomeActivityContent {
        /**
         * Whether the fragment should be displayed.
         *
         * @param context the application context.
         * @return true if the WelcomeActivityContent should be displayed.
         */
        public boolean shouldDisplay(Context context);
    }
}
