package com.mobileparadigm.arkangel.cftmobile.endpoints;

import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Root;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by arkangel on 11/07/16.
 */
public interface BooksApiService {

    // https://bibles.org/v2/versions/eng-GNTD/books.js
    @GET("/v2/versions/{versions}/books.js")
    Observable<Root> getBooks(@Path("versions") String versions);

}

