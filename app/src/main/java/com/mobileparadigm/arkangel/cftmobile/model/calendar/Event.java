
package com.mobileparadigm.arkangel.cftmobile.model.calendar;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Event {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("text_color")
    @Expose
    private String textColor;
    @SerializedName("event_title")
    @Expose
    private String eventTitle;
    @SerializedName("event_description")
    @Expose
    private String eventDescription;
    @SerializedName("event_location")
    @Expose
    private String eventLocation;

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The colorCode
     */
    public String getColorCode() {
        return colorCode;
    }

    /**
     * 
     * @param colorCode
     *     The color_code
     */
    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    /**
     * 
     * @return
     *     The textColor
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * 
     * @param textColor
     *     The text_color
     */
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    /**
     * 
     * @return
     *     The eventTitle
     */
    public String getEventTitle() {
        return eventTitle;
    }

    /**
     * 
     * @param eventTitle
     *     The event_title
     */
    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    /**
     * 
     * @return
     *     The eventDescription
     */
    public String getEventDescription() {
        return eventDescription;
    }

    /**
     * 
     * @param eventDescription
     *     The event_description
     */
    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    /**
     * 
     * @return
     *     The eventLocation
     */
    public String getEventLocation() {
        return eventLocation;
    }

    /**
     * 
     * @param eventLocation
     *     The event_location
     */
    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

}
