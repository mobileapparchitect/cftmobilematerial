package com.mobileparadigm.arkangel.cftmobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HighLightsRowItemAdapter extends RecyclerView.Adapter<HighLightsRowItemAdapter.ViewHolder> {

    List<String> titles;
    private Context mContext;

    public HighLightsRowItemAdapter( Context context, List<String> panelTitles) {
        mContext = context;
        titles = panelTitles;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        switch (position) {
            case 0:
                holder.mRecyclerViewRow.setLayoutManager(layoutManager);
                holder.mRecyclerViewRow.setHasFixedSize(false);
                ViewpagerRowAdapter viewpagerRowAdapter = new ViewpagerRowAdapter(mContext);
                holder.mRecyclerViewRow.setAdapter(viewpagerRowAdapter);
                RecyclerView finalRecyclerView = holder.mRecyclerViewRow;
                finalRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL_LIST));
                RelativeLayout.LayoutParams lp =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 800);
                holder.mRecyclerViewRow.setLayoutParams(lp);
                break;

            case 1:
                EventsRowRecyclerAdapter rowsRecyclerAdapter = new EventsRowRecyclerAdapter(mContext);
                holder.mRecyclerViewRow.setLayoutManager(layoutManager);
                holder.mRecyclerViewRow.setHasFixedSize(false);
                holder.mRecyclerViewRow.setAdapter(rowsRecyclerAdapter);
                finalRecyclerView = holder.mRecyclerViewRow;
                finalRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL_LIST));
                holder.panelTitleTextView.setText((CharSequence) titles.toArray()[--position]);
                rowsRecyclerAdapter.notifyDataSetChanged();
                break;

            case 2:
                PrayerRowsRecyclerAdapter prayerRowsRecyclerAdapter = new PrayerRowsRecyclerAdapter(mContext);
                holder.mRecyclerViewRow.setLayoutManager(layoutManager);
                holder.mRecyclerViewRow.setHasFixedSize(false);
                holder.mRecyclerViewRow.setAdapter(prayerRowsRecyclerAdapter);
                finalRecyclerView = holder.mRecyclerViewRow;
                finalRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL_LIST));
                holder.panelTitleTextView.setText((CharSequence) titles.toArray()[--position]);
                prayerRowsRecyclerAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.high_light_row, parent, false);
        return new ViewHolder(convertView);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recyclerView_row)
        RecyclerView mRecyclerViewRow;
        @BindView(R.id.panel_title)
        TextView panelTitleTextView;
        @BindView(R.id.container_title)
        RelativeLayout holdercontainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
