package com.mobileparadigm.arkangel.cftmobile.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.model.ItemObjects;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Miroslaw Stanek on 20.01.15.
 */
public class HomeScreenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int PHOTO_ANIMATION_DELAY = 600;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();
    private static final int TYPE_HEADER =0;
    private static final int TYPE_CARD =1;
    private static final int TYPE_ROW=2;

    private final Context context;
    private final int cellSize;

    private final List<String> photos;

    private boolean lockedAnimations = false;
    private int lastAnimatedItem = -1;
    private List<ItemObjects> itemObjectsList;

    public HomeScreenAdapter(Context context, List<ItemObjects> itemList) {
        this.context = context;
        itemObjectsList = itemList;
        this.cellSize = Utils.getScreenWidth(context) / 3;
        this.photos = Arrays.asList(context.getResources().getStringArray(R.array.user_photos));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (TYPE_HEADER == viewType) {
            final View view = LayoutInflater.from(context).inflate(R.layout.main_header, null);
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            layoutParams.setFullSpan(false);
            view.setLayoutParams(layoutParams);
            return new HeaderWidgetAdapter(view);
        }else if(viewType == TYPE_CARD){
            final View view = LayoutInflater.from(context).inflate(R.layout.home_card_item, null);
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            layoutParams.setFullSpan(false);
            view.setLayoutParams(layoutParams);
            return new HomeCardViewHolder(view);
        }

        return null;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (TYPE_HEADER == viewType) {
         //   bindProfileHeader((HeaderWidgetAdapter) holder);
        } else if (TYPE_CARD == viewType) {
       //     bindProfileOptions((ProfileOptionsViewHolder) holder);
        } else if (TYPE_ROW == viewType) {
       //     bindPhoto((PhotoViewHolder) holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == 1) {
            return TYPE_CARD;
        } else {
            return TYPE_ROW;
        }
    }

    private void bindProfileHeader(final PhotoViewHolder holder, int position) {
     //Glide stuff
        if (lastAnimatedItem < position) lastAnimatedItem = position;
    }

    private void bindProfileOptions(final PhotoViewHolder holder, int position) {
        //Glide stuff
        if (lastAnimatedItem < position) lastAnimatedItem = position;
    }

    private void bindPhoto(final PhotoViewHolder holder, int position) {

        // Glide.with(context).load(photos.get(position)).override(cellSize,cellSize).centerCrop().into(holder.ivPhoto);

        Glide.with(context).load(photos.get(position)).override(cellSize, cellSize).centerCrop().into(new GlideDrawableImageViewTarget(holder.ivPhoto) {


            @Override
            public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                animatePhoto(holder);
            }
        });

        if (lastAnimatedItem < position) lastAnimatedItem = position;
    }

    private void animatePhoto(PhotoViewHolder viewHolder) {
        if (!lockedAnimations) {
            if (lastAnimatedItem == viewHolder.getPosition()) {
                setLockedAnimations(true);
            }
            long animationDelay = PHOTO_ANIMATION_DELAY + viewHolder.getPosition() * 30;

            viewHolder.flRoot.setScaleY(0);
            viewHolder.flRoot.setScaleX(0);

            viewHolder.flRoot.animate()
                    .scaleY(1)
                    .scaleX(1)
                    .setDuration(200)
                    .setInterpolator(INTERPOLATOR)
                    .setStartDelay(animationDelay)
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        return itemObjectsList.size();
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.flRoot)
        FrameLayout flRoot;
        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;

        public PhotoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    static class HomeCardViewHolder extends RecyclerView.ViewHolder {
   /*     @InjectView(R.id.img_thumbnail)
        ImageView imageView;
        @InjectView(R.id.card_title)
        TextView name;*/

        public HomeCardViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class HeaderWidgetAdapter extends RecyclerView.ViewHolder {

        @BindView(R.id.header_image)
        ImageView ivPhoto;

        public HeaderWidgetAdapter(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setLockedAnimations(boolean lockedAnimations) {
        this.lockedAnimations = lockedAnimations;
    }
}
