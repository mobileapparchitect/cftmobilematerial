package com.mobileparadigm.arkangel.cftmobile.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Record implements Parcelable{

    private static final String FIELD_DAY = "day";
    private static final String FIELD_SPEAKER = "speaker";
    private static final String FIELD_COMMENTS = "comments";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_SERIAL = "serial";
    private static final String FIELD_RECORD = "Record";
    private static final String FIELD_DATE = "date";


    private String mDay;
    private String mSpeaker;
    private String mComment;
    private String mTitle;
    private String mSerial;
    private List<Record> mRecords;
    private String mDate;


    public Record(){

    }

    public void setDay(String day) {
        mDay = day;
    }

    public String getDay() {
        return mDay;
    }

    public void setSpeaker(String speaker) {
        mSpeaker = speaker;
    }

    public String getSpeaker() {
        return mSpeaker;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getComment() {
        return mComment;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setSerial(String serial) {
        mSerial = serial;
    }

    public String getSerial() {
        return mSerial;
    }

    public void setRecords(List<Record> records) {
        mRecords = records;
    }

    public List<Record> getRecords() {
        return mRecords;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDate() {
        return mDate;
    }

    public Record(Parcel in) {
        mDay = in.readString();
        mSpeaker = in.readString();
        mComment = in.readString();
        mTitle = in.readString();
        mSerial = in.readString();
        mRecords = new ArrayList<Record>();
        in.readTypedList(mRecords, Record.CREATOR);
        mDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Record> CREATOR = new Creator<Record>() {
        public Record createFromParcel(Parcel in) {
            return new Record(in);
        }

        public Record[] newArray(int size) {
        return new Record[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDay);
        dest.writeString(mSpeaker);
        dest.writeString(mComment);
        dest.writeString(mTitle);
        dest.writeString(mSerial);
        dest.writeTypedList(mRecords);
        dest.writeString(mDate);
    }


}