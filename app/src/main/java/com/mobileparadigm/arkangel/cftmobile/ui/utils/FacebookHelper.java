package com.mobileparadigm.arkangel.cftmobile.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.NativeProtocol;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.model.GraphUser;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;



public class FacebookHelper {
    private static final String MARKET_URL = "market://details?id=";
    public static final String FACEBOOK_MARKET_URL = MARKET_URL + NativeProtocol.FACEBOOK_KATANA_PKG;
    public static final String MESSENGER_MARKET_URL = MARKET_URL + NativeProtocol.FACEBOOK_MESSENGER_PKG;

    private static final String WEB_URL = "https://play.google.com/store/apps/details?id=";
    public static final String FACEBOOK_WEB_URL = WEB_URL + NativeProtocol.FACEBOOK_KATANA_PKG;
    public static final String MESSENGER_WEB_URL = WEB_URL + NativeProtocol.FACEBOOK_MESSENGER_PKG;
    private static List<String> READ_PERMISSION_LIST = Arrays.asList("email", "user_location", "user_birthday", "user_friends");
    private CallbackManager callbackManager;
    private boolean publishRequested;
    private ShareDialog shareDialog;
    private Activity context;
    private FacebookListener listener;
    @Inject
    Firebase firebaseService;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Bundle bundle = new Bundle();
            bundle.putString("fields", "id,name,gender,birthday,email,last_name,first_name,location");
            GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "me", new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse graphResponse) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    onFacebookAccessTokenChange(AccessToken.getCurrentAccessToken());
                    try {
                        GraphUser user = objectMapper.readValue(graphResponse.getRawResponse(), GraphUser.class);
                        String fbToken = graphResponse.getRequest().getParameters().getString("access_token");
                        //PreferencesHelper.setKeyFacebookToken(fbToken);
                        loginSuccess(fbToken);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("onLoginFailed: " , graphResponse.toString());
                        loginFailed();
                    }
                }
            });
            request.setParameters(bundle);
            request.executeAsync();
        }


        private void onFacebookAccessTokenChange(AccessToken token) {
            if (token != null) {
                firebaseService.authWithOAuthToken("facebook", token.getToken(), new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        Log.v("FacebookHelper: " , "The Facebook user is now authenticated with your Firebase app");
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        // there was an error
                        Log.e("FacebookHelper: " , firebaseError.getDetails());
                    }
                });
            } else {
        /* Logged out of Facebook so do a logout from the Firebase app */
              firebaseService.unauth();
            }
        }
        @Override
        public void onCancel() {
            loginFailed();
        }

        @Override
        public void onError(FacebookException exception) {
            if (!(exception != null && exception instanceof FacebookOperationCanceledException)) { //Don't show if the user cancelled operation

            }
            exception.printStackTrace();
            loginFailed();
        }
    };

    public void cancelPending() {
    }


    public void shareContent(String url, String title, String description, FacebookCallback<Sharer.Result> shareCallback) {
        if (shareDialog == null) {
            shareDialog = new ShareDialog(context);
            // this part is optional
            shareDialog.registerCallback(callbackManager, shareCallback);
        }
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent content = new ShareLinkContent.Builder().setContentTitle(title).setContentDescription(description)
                    .setContentUrl(Uri.parse(url))
                    .build();
            shareDialog.show(context, content);
        }
    }

    /*public static void inviteFriends(Activity context) {
        if (AppInviteDialog.canShow()) {
            String serverName = context.getString(R.string.google_play_link);
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(serverName)
                    .build();
            AppInviteDialog.show(context, content);
        }
    }*/

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public FacebookHelper(Activity context, FacebookListener listener) {
        this.context = context;
        this.listener = listener;
        init();
    }

    public FacebookHelper(Fragment fragment, FacebookListener listener) {
        this.context = fragment.getActivity();
        this.listener = listener;
        init();
    }


    public static void redirectToFbMessengerInstallPage(Activity activity) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MESSENGER_MARKET_URL)));
        } catch (android.content.ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MESSENGER_WEB_URL)));
        }
    }

    public static void redirectToFacebookInstallPage(Activity activity) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_MARKET_URL)));
        } catch (android.content.ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FACEBOOK_WEB_URL)));
        }
    }

    public static boolean checkIfFacebookInstalled(Context context) {
        try {
            ApplicationInfo info = context.getPackageManager().
                    getApplicationInfo(NativeProtocol.FACEBOOK_KATANA_PKG, 0);
            if (info != null) {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(info.packageName, 0);
//                String version = pInfo.versionName;
//                if (!TextUtils.isEmpty(version)) {
                // TODO: version name is "43.0.1.2", below code throws parse exception
//                    int code = Integer.parseInt(version);
//                }
                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    private void init() {
        FacebookSdk.setApplicationName(CFTMaterialApplication.getInstance().getString(R.string.facebook_app_name));
        FacebookSdk.setApplicationId(CFTMaterialApplication.getInstance().getString(R.string.facebook_app_id));
        FacebookSdk.sdkInitialize(CFTMaterialApplication.getInstance());

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, callback);
        LoginManager.getInstance().logOut();
    }

    public void login() {
        LoginManager.getInstance().logInWithReadPermissions(context, READ_PERMISSION_LIST);
    }

    public void checkPermissions() {
        if (AccessToken.getCurrentAccessToken() != null) {
            Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();
            if (!permissions.contains("publish_actions")) {
                if (!publishRequested) {
                    LoginManager.getInstance().logInWithPublishPermissions(
                            context,
                            Arrays.asList("publish_actions"));
                    publishRequested = true;
                    return;
                } else {
                    loginFailed();
                    publishRequested = false;
                }
            } else {
                verified();
                publishRequested = false;
            }
        }
    }

    private void loginFailed() {
        if (listener != null) {
            listener.loginFailed();
        }
    }

    private void verified() {
        if (listener != null) {
            listener.verified();
        }
    }

    private void loginSuccess(Object user) {
        if (listener != null) {
            listener.loginSuccess(user);
        }
    }

    public interface FacebookListener {

        void loginFailed();

        void inProgress();

        void loginSuccess(Object user);

        void verified();
    }
}
