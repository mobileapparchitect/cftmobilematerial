package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;


import org.w3c.dom.Text;
import org.xml.sax.XMLReader;


public class VerseTagHandler implements Html.TagHandler {

    private int startIndex = 0;

    private int stopIndex = 0;

    @Override
    public void handleTag(boolean opening, String tag, Editable output,
                          XMLReader xmlReader) {
        if (tag.toLowerCase().equals("verse")) {
            if (opening) {
                startGame(tag, output, xmlReader);
            } else {
                endGame(tag, output, xmlReader);
            }
        }

    }

    public void startGame(String tag, Editable output, XMLReader xmlReader) {
        startIndex = output.length();
        Log.i("Tag found:" ,tag);
    }

    public void endGame(String tag, Editable output, XMLReader xmlReader) {
        stopIndex = output.length();
        Log.i("Tag found:" ,tag);

        output.setSpan(new GameSpan(), startIndex, stopIndex,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }



}