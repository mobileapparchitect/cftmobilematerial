package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.view.CustomTextView;
import com.mobileparadigm.arkangel.cftmobile.ui.view.DepthPageTransformer;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.squareup.otto.Bus;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by arkangel on 25/05/15.
 */
public class UsageViewPagerFragment extends BaseFragment {

    @BindView(R.id.page_indicator)
    CirclePageIndicator pagerIndicator;

    @BindView(R.id.skip)
    CustomTextView skipTextView;


    @BindView(R.id.tutorial_view_pager)
    ViewPager
            splashViewPager;
    private TutorialAdapter tutorialAdapter;
    private View view;

    @Inject
    Bus postFromAnyThread;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.screen_tutorial_viewpager, container, false);
        ButterKnife.bind(this, view);
        skipTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        tutorialAdapter = new TutorialAdapter();
        tutorialAdapter.addView(inflater.inflate(R.layout.screen_usage_viewpager_step_one, null));
        tutorialAdapter.addView(inflater.inflate(R.layout.screen_usage_viewpager_step_two, null));
        tutorialAdapter.addView(inflater.inflate(R.layout.screen_usage_viewpager_step_three, null));
        tutorialAdapter.addView(inflater.inflate(R.layout.screen_usage_viewpager_step_four, null));
        View screenFive = inflater.inflate(R.layout.screen_usage_viewpager_step_five, null);
        tutorialAdapter.addView(screenFive);
        splashViewPager.setAdapter(tutorialAdapter);
        splashViewPager.setPageTransformer(true, new DepthPageTransformer());
        pagerIndicator.setViewPager(splashViewPager);
        pagerIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }


        });


        ((Button) screenFive.findViewById(R.id.go_to_main_screen_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesHelper.setSeenIntro(true);
                showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_PRE_SIGNUP);
                postFromAnyThread.post(showNextEvent);

            }
        });


        return view;
    }

    @OnClick(R.id.skip)
    public void loginScreen(){
        showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_SHOW_LOGIN);
        postFromAnyThread.post(showNextEvent);
    }



    private class TutorialAdapter extends PagerAdapter {

        private ArrayList<View> views = new ArrayList<View>();

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v = views.get(position);
            container.addView(v);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(views.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getItemPosition(Object object) {
            int index = views.indexOf(object);
            if (index == -1)
                return POSITION_NONE;
            else
                return index;
        }

        public int addView(View v) {
            return addView(v, views.size());
        }

        public int addView(View v, int position) {
            views.add(position, v);
            return position;
        }
    }
}
