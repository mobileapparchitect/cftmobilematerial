
package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mobileparadigm.arkangel.cftmobile.ui.activity.MainMenuActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.WelcomeActivity;


/**
 * A Fragment class for use with {@link WelcomeActivity} to embed content into the activity.
 *
 * Contains utitlies for attaching the fragment to the activity and updating UI elements.
 */
public abstract class WelcomeFragment extends android.support.v4.app.Fragment {


    protected Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
     //   LOGD(TAG, "Creating View");

        // If the acitivty the fragment has been attached to is a WelcomeFragmentContainer
        if (mActivity instanceof WelcomeFragmentContainer) {
            WelcomeFragmentContainer activity = (WelcomeFragmentContainer) mActivity;

            // Attach to the UI elements
            attachToPositiveButton(activity.getPositiveButton());
            attachToNegativeButton(activity.getNegativeButton());
        }
        return view;
    }

    /**
     * Attach to the positive action button of the WelcomeFragmentContainer.
     *
     * @param button the ui element to attach to.
     */
    protected void attachToPositiveButton(Button button) {
        // Set the button text
        button.setText(getPositiveText());

        // Set the click listener
        button.setOnClickListener(getPositiveListener());
    }

    /**
     * Attach to the negative action button of the WelcomeFragmentContainer.
     *
     * @param button the ui element to attach to.
     */
    protected void attachToNegativeButton(Button button) {
        // Set the button text
        button.setText(getNegativeText());

        // Set the click listener
        button.setOnClickListener(getNegativeListener());
    }


    /**
     * Get a resource string.
     *
     * @param id the id of the string resource.
     * @return the value of the resource or null.
     */
    protected String getResourceString(int id) {
        if(mActivity != null) {
            return mActivity.getResources().getString(id);
        }
        return null;
    }

    /**
     * Get the text for the positive action button.
     *
     * E.g. Accept
     *
     * @return the text for the button.
     */
    protected abstract String getPositiveText();

    /**
     * Get the text for the negative action button.
     *
     * E.g. Decline
     *
     * @return the text for the negative action button.
     */
    protected abstract String getNegativeText();

    /**
     * Get the {@link android.view.View.OnClickListener} for the positive action click event.
     *
     * @return the click listener.
     */
    protected abstract View.OnClickListener getPositiveListener();

    /**
     * Get the {@link android.view.View.OnClickListener} for the negative action click event.
     *
     * @return the click listener.
     */
    protected abstract View.OnClickListener getNegativeListener();

    /**
     * A convience {@link android.view.View.OnClickListener} for the common use case in the
     * WelcomeActivityContent.
     */
    protected abstract class WelcomeFragmentOnClickListener implements View.OnClickListener {
        /**
         * The action to perform on click, before proceeding to the next activity or exiting the
         * app.
         */
        Activity mActivity;

        /**
         * Construct a listener that will handle the transition to the next activity or exit after
         * completing.
         *
         * @param activity the Activity to interact with.
         */
        public WelcomeFragmentOnClickListener(Activity activity) {
            mActivity = activity;
        }

        /**
         * Proceed to the next activity.
         */
        void doNext() {
          //  LOGD(TAG, "Proceeding to next activity");
            if (WelcomeActivity.shouldDisplay(getActivity())) {
                WelcomeActivity.showNextFragment();
            } else {
                MainMenuActivity.startMainMenuActivity((AppCompatActivity) getActivity());
                //mActivity.startActivity(new Intent(mActivity, StartActivity.class));
                //StartActivity.loadStartingActivity((AppCompatActivity) mActivity);
                mActivity.finish();
            }
        }

        /**
         * Finish the activity.
         *
         * We're done here.
         */
        void doFinish() {
        //    LOGD(TAG, "Closing app");
            mActivity.finish();
        }
    }

    /**
     * The receiever for the action to be performed on a button click.
     */
    interface WelcomeFragmentClickAction {
        public void doAction(Context context);
    }

    /**
     * The Container for the WelcomeActivityContent.
     */
   public interface WelcomeFragmentContainer {

        /**
         * Retrieve a posistive action button from the container.
         *
         * @return the positive action button.
         */
        public Button getPositiveButton();

        /**
         * Enable the positive action button in the container.
         *
         * @param enabled true to enable it, false to disable it.
         */
        public void setPositiveButtonEnabled(Boolean enabled);

        /**
         * Retrieve a negative action button from the container.
         *
         * @return the negative action button.
         */
        public Button getNegativeButton();

        /**
         * Enable the negative action button in the container.
         *
         * @param enabled true to enable it, false to disable it.
         */
        public void setNegativeButtonEnabled(Boolean enabled);
    }
}
