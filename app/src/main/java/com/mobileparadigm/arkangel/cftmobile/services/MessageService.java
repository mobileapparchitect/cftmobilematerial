package com.mobileparadigm.arkangel.cftmobile.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.WritableMessage;


public class MessageService extends Service {

    private static final String APP_KEY = "c2b57729-be3f-4f8b-806f-ea87d31506c4";
    private static final String APP_SECRET = "JENCvhCHbEe4SsA1idxx7g==";
    private static final String ENVIRONMENT = "sandbox.sinch.com";
    private final MessageServiceInterface serviceInterface = new MessageServiceInterface();
    private SinchClient sinchClient = null;
    private MessageClient messageClient = null;
    private String currentUserId;
    private LocalBroadcastManager broadcaster;
    private Intent broadcastIntent = new Intent("com.mobileparadigm.arkangel.cftmobile.ui.activity.BaseActivity");

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!isSinchClientStarted()) {
            startSinchClient("android_god");
            broadcaster = LocalBroadcastManager.getInstance(this);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void startSinchClient(String username) {
        sinchClient = Sinch.getSinchClientBuilder().context(this).userId(username).applicationKey(APP_KEY)
                .applicationSecret(APP_SECRET).environmentHost(ENVIRONMENT).build();
        sinchClient.addSinchClientListener(new SinchClientListener() {
            @Override
            public void onClientStarted(SinchClient sinchClient) {
                Log.i(MessageService.class.getSimpleName(),"started");
                broadcastIntent.putExtra("success", true);
                broadcaster.sendBroadcast(broadcastIntent);
                sinchClient.startListeningOnActiveConnection();
                messageClient = sinchClient.getMessageClient();
            }

            @Override
            public void onClientStopped(SinchClient sinchClient) {
                Log.i(MessageService.class.getSimpleName(),"stopped");
                sinchClient = null;
            }

            @Override
            public void onClientFailed(SinchClient sinchClient, SinchError sinchError) {
                Log.i(MessageService.class.getSimpleName(),"failed");
                broadcastIntent.putExtra("success", false);
                broadcaster.sendBroadcast(broadcastIntent);
                sinchClient = null;
            }

            @Override
            public void onRegistrationCredentialsRequired(SinchClient sinchClient, ClientRegistration clientRegistration) {
                Log.i(MessageService.class.getSimpleName(),"reg required");
            }

            @Override
            public void onLogMessage(int i, String s, String s1) {
                Log.i(MessageService.class.getSimpleName(),s + "/" + s1);
            }
        });
        sinchClient.setSupportMessaging(true);
        sinchClient.setSupportActiveConnectionInBackground(true);
        sinchClient.checkManifest();
        sinchClient.start();
    }

    private boolean isSinchClientStarted() {
        return sinchClient != null && sinchClient.isStarted();
    }





    @Override
    public IBinder onBind(Intent intent) {
        return serviceInterface;
    }




    public void sendMessage(String recipientUserId, String textBody) {
        if (messageClient != null) {
            WritableMessage message = new WritableMessage(recipientUserId, textBody);
            messageClient.send(message);
        }
    }

    public void addMessageClientListener(MessageClientListener listener) {
        if (messageClient != null) {
            messageClient.addMessageClientListener(listener);
        }
    }

    public void removeMessageClientListener(MessageClientListener listener) {
        if (messageClient != null) {
            messageClient.removeMessageClientListener(listener);
        }
    }

    @Override
    public void onDestroy() {
        if(sinchClient !=null){
            sinchClient.stopListeningOnActiveConnection();
            sinchClient.terminate();
        }

    }

    public class MessageServiceInterface extends Binder {
        public void sendMessage(String recipientUserId, String textBody) {
            MessageService.this.sendMessage(recipientUserId, textBody);
        }

        public void addMessageClientListener(MessageClientListener listener) {
            MessageService.this.addMessageClientListener(listener);
        }

        public void removeMessageClientListener(MessageClientListener listener) {
            MessageService.this.removeMessageClientListener(listener);
        }

        public boolean isSinchClientStarted() {
            return MessageService.this.isSinchClientStarted();
        }
    }
}

