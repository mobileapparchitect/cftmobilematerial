package com.mobileparadigm.arkangel.cftmobile.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;


public class PreferencesHelper {

    private static final String DATA_DOWNLOADED="dataDownloaded";
    private static final String CURRENT_ANNOUNCEMENT= "current_announcement";
    private static final String CURRENT_USER_BIBLE_VERSION="user_version";
    private static final String KEY_FACEBOOK_TOKEN = "facebook_token";
    private static final String KEY_USER_LOGGED_IN="loggedIn";
    private static final String KEY_EMAIL="user_email";
    private static final String KEY_PASSWORD="user_password";
    private static final String KEY_INTRO_PAGER="introPager";
    private static final String KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String KEY_TOKEN="auth_twitter_token";
    private static final String KEY_SECRET="auth_twitter_secret";
    private static final String KEY_TWITTER_USER_NAME = "twitter_user_name";
    private static final String KEY_IS_BULLETIN_DOWNLOADED="bulletinDownloaded";
    private static final String PREF_TOS_ACCEPTED ="prefsTosAccepted";
    private static final String PREF_CONDUCT_ACCEPTED ="prefsConductAccepted";
    private static final String DATABASE_CREATED = "booksTable";


    public static String getCurrentAnnouncementId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
       return prefs.getString(CURRENT_ANNOUNCEMENT, null);
    }

    /*
    public static void setKeyFacebookToken(final String token) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(KEY_FACEBOOK_TOKEN, token).apply();
    }
    public static boolean isUserLoggedIn() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(KEY_USER_LOGGED_IN, false);
    }



    public static void setLoggedIn(boolean loggedIn) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(KEY_USER_LOGGED_IN, loggedIn).apply();
    }





    public static String getUserEmail() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getString(KEY_EMAIL, null);
    }

    public static void setUserEmail(final String email) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(KEY_EMAIL, email).apply();
    }
    public static void setPassword(final String email) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(KEY_PASSWORD, email).apply();
    }

    public static String getPassword() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getString(KEY_PASSWORD, null);
    }


        public static boolean getTwitterLoggedIn() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(KEY_TWITTER_LOGIN, false);
    }


    public static void setTwitterLoggedIn(boolean loggedIn) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(KEY_TWITTER_LOGIN, loggedIn).apply();
    }


    public static String getTwitterToken() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getString(KEY_TOKEN, null);
    }

    public static void setTwitterToken(final String token) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(KEY_TOKEN, token).apply();
    }

    public static String getTwitterUsername() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getString(KEY_TWITTER_USER_NAME, null);
    }

    public static void setTwitterUserName(final String twitterUserName) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(KEY_TWITTER_USER_NAME, twitterUserName).apply();
    }

    public static String getTwitterSecret() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getString(KEY_SECRET, null);
    }

    public static void setTwitterSecret(final String email) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(KEY_SECRET, email).apply();
    }



    */

    public static boolean isSeenIntro() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(KEY_INTRO_PAGER, false);
    }

    public static void setSeenIntro(boolean introSeen) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(KEY_INTRO_PAGER, introSeen).apply();
    }


    public static void setBulletinDownloaded(boolean bulletinDownloaded) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(KEY_IS_BULLETIN_DOWNLOADED, bulletinDownloaded).apply();
    }

    public static boolean isBulletinDownloaded() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(KEY_IS_BULLETIN_DOWNLOADED, false);
    }


    public static void setCurrentAnnouncmentId(String currentAnnouncement) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(CURRENT_ANNOUNCEMENT, currentAnnouncement).apply();
    }


    public static String getUserBibleVersion() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getString(CURRENT_USER_BIBLE_VERSION, null);
    }

    public static String getPredefindUserVersion(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(CURRENT_USER_BIBLE_VERSION, null);
    }

    public static void setUserBibleVersion(String version) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putString(CURRENT_USER_BIBLE_VERSION, version).apply();
    }


    public static boolean isConductAccepted() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(PREF_CONDUCT_ACCEPTED, false);
    }

    public static void setDataRetrieved(boolean dataRetrieved) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(DATA_DOWNLOADED, dataRetrieved).apply();
    }


    public static boolean isDataRetrieved() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(DATA_DOWNLOADED, false);
    }
    public static void markConductAccepted(boolean newValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(PREF_TOS_ACCEPTED, newValue).apply();
    }

    /**
     * Mark {@code newValue whether} the user has accepted the TOS so the app doesn't ask again.
     *
     *
     */
    public static void markTosAccepted( boolean newValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(PREF_TOS_ACCEPTED, newValue).apply();
    }

    public static boolean isTosAccepted() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(PREF_TOS_ACCEPTED, false);
    }


    public static void setDatabaseCreated(boolean loggedIn) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        prefs.edit().putBoolean(DATABASE_CREATED, loggedIn).apply();
    }


    public static boolean getDatabaseCreated() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CFTMaterialApplication.getInstance());
        return prefs.getBoolean(DATABASE_CREATED, false);
    }
}