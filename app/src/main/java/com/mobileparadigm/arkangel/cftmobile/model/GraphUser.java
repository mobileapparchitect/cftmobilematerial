package com.mobileparadigm.arkangel.cftmobile.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by SAmson akisanya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraphUser {

    private String id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String email;
    private String birthday;
    private String gender;
    private String name;
    private Location location;

    public Location getLocation() {
        return location;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Location {
        int id;
        String name;
    }
}
