package com.mobileparadigm.arkangel.cftmobile.endpoints;


import com.mobileparadigm.arkangel.cftmobile.data_models.Verse;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verses;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by arkangel on 11/07/16.
 */
public interface VersesApiService {

    //   https://bibles.org/v2/chapters/eng-KJVA:1Cor.2/verses.js?start=5&end=6
    @GET("/v2/chapters/{versions}/verses.js")
    Observable<Verses> getVerses(@Path("versions") String versions, @Query("start") String start, @Query("end") String end);

}
