package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Version   implements Parcelable{

    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_PATH = "path";

    @SerializedName(FIELD_ID)
    private String mId;
    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_PATH)
    private String mPath;


    public Version(){

    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getPath() {
        return mPath;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Version){
            return ((Version) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return mId.hashCode();
    }

    public Version(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mPath = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Version> CREATOR = new Creator<Version>() {
        public Version createFromParcel(Parcel in) {
            return new Version(in);
        }

        public Version[] newArray(int size) {
        return new Version[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mPath);
    }


}