package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.MainMenuActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.utils.CroutonUtils;
import com.mobileparadigm.arkangel.cftmobile.ui.view.CustomEditTextView;
import com.mobileparadigm.arkangel.cftmobile.utils.DeviceUtils;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.sinch.android.rtc.internal.gen.Build;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by arkangel on 25/05/15.
 */
public class SignupFragment extends BaseFragment implements Validator.ValidationListener {

    private static Activity activity;

    @NotEmpty
    @Email
    @BindView(R.id.edit_text_email_address)
    CustomEditTextView emailAddressEditBox;
    @NotEmpty
    @BindView(R.id.edit_text_password)
    CustomEditTextView userPassword;

    @NotEmpty
    @BindView(R.id.edit_text_username)
    CustomEditTextView username;
    private View view;
    private Validator validator;
    private ProgressBar progressBar;
    @Inject
    Firebase firebaseService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.screen_signup, container, false);
        validator = new Validator(this);
        activity = getActivity();

        validator.setValidationListener(this);
        ButterKnife.bind(this, view);
        if (Build.DEBUG) {
            emailAddressEditBox.setText(Utils.generateRandomeEmail());
        }
        return view;
    }

    @OnClick(R.id.button_launch_submit)
    public void signUp() {
        Toast.makeText(getActivity(), "SignupButton", Toast.LENGTH_SHORT).show();
        validator.validate();
    }

    @OnClick(R.id.button_facebookbutton)
    public void signUpFacebook() {
        Toast.makeText(getActivity(), "FacebookSignup", Toast.LENGTH_SHORT).show();
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        signUpWithParse(username.getText().toString(), emailAddressEditBox.getText().toString(), userPassword.getText().toString());
    }

    protected void signUpMsg(String msg) {
        CroutonUtils.error(getActivity(), msg);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                CroutonUtils.error(getActivity(), message);
            }
        }
    }

    private void signUpWithParse(final String mUsername, final String mEmail, final String mPassword) {
        if (DeviceUtils.isConnected(getActivity())) {
          firebaseService.authWithPassword(mUsername, mPassword, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    System.out.println("User ID: " + authData.getUid() + ", Provider: " + authData.getProvider());
                  //  PreferencesHelper.setUserEmail(mUsername);
                    //PreferencesHelper.setPassword(mPassword);
                    signUpMsg("Account Created Successfully");
                    getActivity().finish();
                    MainMenuActivity.startMainMenuActivity((AppCompatActivity) getActivity());
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    // there was an error
                    signUpMsg(firebaseError.getDetails());
                }
            });
        } else {
            CroutonUtils.error(getActivity(), getString(R.string.no_connectivity));
        }
    }
}