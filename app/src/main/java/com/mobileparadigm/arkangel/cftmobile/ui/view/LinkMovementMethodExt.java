package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.os.Message;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.ui.activity.DetailActivity;

public class LinkMovementMethodExt extends LinkMovementMethod {
	private static LinkMovementMethod sInstance;
	private DetailActivity activity = null;
	private Class spanClass = null;
	
	public static MovementMethod getInstance(DetailActivity _handler,Class _spanClass) {

		if (sInstance == null) {
			sInstance = new LinkMovementMethodExt();
			((LinkMovementMethodExt)sInstance).activity = _handler;
			((LinkMovementMethodExt)sInstance).spanClass = _spanClass;
		}
		Log.d("DEUBGING", " " + sInstance);
		return sInstance;
	}
	 @Override
	    public boolean onTouchEvent(TextView widget, Spannable buffer,
	                                MotionEvent event) {

		 	DetailActivity.handler.sendMessage(new Message());
	        int action = event.getAction();

	        if (action == MotionEvent.ACTION_UP ||
	            action == MotionEvent.ACTION_DOWN) {
	            int x = (int) event.getX();
	            int y = (int) event.getY();

	            x -= widget.getTotalPaddingLeft();
	            y -= widget.getTotalPaddingTop();

	            x += widget.getScrollX();
	            y += widget.getScrollY();

	            Layout layout = widget.getLayout();
	            int line = layout.getLineForVertical(y);
	            int off = layout.getOffsetForHorizontal(line, x);
	            /**
	             * get you interest span
	             */
	            Object[] spans = buffer.getSpans(off, off, spanClass);
	            if (spans.length != 0) {
	                if (action == MotionEvent.ACTION_DOWN) {
						Log.d("DEUBGING", " " + "onTouchEvent");
	                	Selection.setSelection(buffer,
								buffer.getSpanStart(spans[0]),
								buffer.getSpanEnd(spans[0]));
	                	MessageSpan obj = new MessageSpan();
	                				obj.setObj(spans);
	                				obj.setView(widget);
	                	Message message = new Message();
	                			message.obj = obj;
	                			message.what = 100;
						activity.putSpan(message);
	                	return true;
	                } else if (action == MotionEvent.ACTION_UP) {
	                	MessageSpan obj = new MessageSpan();
        				obj.setView(widget);
	                	Message message = new Message();
		            			message.obj = obj;
		            			message.what = 200;
						activity.unputSpat(message);
		                return true;		            			
	                }
	            } 
	        }

	        return super.onTouchEvent(widget, buffer, event);
	    }

	 public boolean canSelectArbitrarily() {
	        return true;
	    }
	 
	public boolean onKeyUp(TextView widget, Spannable buffer, int keyCode,
			KeyEvent event) {
		return false;
	}
}
