package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Dave on 09/11/2015.
 */
public class IntelligentTextView extends TextView {

    public IntelligentTextView(Context context) {
        super(context);
        setText(this.getText());
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf"));
    }

    public IntelligentTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IntelligentTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(String text) {
        if (TextUtils.isEmpty(text)) {
            setVisibility(GONE);
        } else {
            super.setText(text);
            setVisibility(VISIBLE);
        }
    }
}
