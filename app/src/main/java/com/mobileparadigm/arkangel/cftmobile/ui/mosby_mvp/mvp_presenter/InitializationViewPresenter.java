package com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_presenter;

import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_views.InitializationView;




public class InitializationViewPresenter extends MvpBasePresenter<InitializationView>
        implements InitializationPresenter {

    private static final String TAG = InitializationViewPresenter.class.getSimpleName();


    public InitializationViewPresenter() {Log.d(TAG , "constructor " + toString());
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);


    }

    @Override
    public void attachView(InitializationView view) {
        super.attachView(view);
        Log.d(TAG + "attach view " , view.toString());
    }

    @Override
    public void loadData() {
        //Dataprovider stuff
    }


}