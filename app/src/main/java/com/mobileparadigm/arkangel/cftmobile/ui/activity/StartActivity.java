package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.modules.PostFromAnyThreadBus;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.LoginWithEmailFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.PreSignupFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.SignupFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.UsageViewPagerFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.utils.CroutonUtils;
import com.mobileparadigm.arkangel.cftmobile.ui.utils.FacebookHelper;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;


import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;


/**
 * Created by arkangel on 25/05/15.
 */
public class StartActivity extends AppCompatActivity {
    public static final int NAV_CONSTANT_SHOW_VIEWPAGER = 30;
    public static final int NAV_CONSTANT_SHOW_LOGIN = 31;
    public static final int NAV_CONSTANT_SHOW_EMAIL_SIGN_UP = 33;
    public static final int NAV_CONSTANT_RESET_PASSWORD = 34;
    public static final int NAV_CONSTANT_RESET_EMAIL = 35;
    public static final int NAV_CONSTANT_LOGIN_WITH_FACEBOOK = 37;
    public static final int NAV_CONSTANT_LOGIN_WITH_TWITTER = 38;
    public static final int NAV_CONSTANT_PRE_SIGNUP = 39;
    public static final int NAV_CONSTANT_GO_MAIN = 40;
    public static final int NAV_CONSTANT_SHOW_EMAIL_LOGIN_FORM = 534;
    public static final int TWITTER_LOGIN = 100;
    public static final String MISC_NAV = "misc_nav";

    private android.support.v4.app.FragmentManager fragmentManager;
    private Bundle startIntent;
    private RequestToken requestToken;
    private Twitter twitter;
    private User user = null;
    private String verifier;
    private String username;
    private FacebookHelper facebookHelper;
    private static AppCompatActivity activity;
    @Inject
    Firebase firebaseService;
    @Inject
    Bus postFromAnyThreadBus;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_layout);
        activity = this;
        fragmentManager = getSupportFragmentManager();
        startIntent = getIntent().getExtras();
        postFromAnyThreadBus.register(this);
        if (startIntent == null) {
            if (!PreferencesHelper.isSeenIntro()) {
                fragmentManager.beginTransaction().add(R.id.fragment_container, new UsageViewPagerFragment()).commit();
            } else {
                fragmentManager.beginTransaction().add(R.id.fragment_container, new com.mobileparadigm.arkangel.cftmobile.ui.fragments.LoginFragment()).commit();
            }
            return;
        }
        int nav = startIntent.getInt(MISC_NAV);
        switch (nav) {

            case NAV_CONSTANT_SHOW_VIEWPAGER:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new UsageViewPagerFragment()).commit();
                break;


            case NAV_CONSTANT_SHOW_EMAIL_SIGN_UP:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new SignupFragment()).commit();
                break;


            case NAV_CONSTANT_RESET_EMAIL:
                break;
            case NAV_CONSTANT_GO_MAIN:
                finish();
                Intent in = new Intent(this, MainMenuActivity.class);
                startActivity(in);
                break;

            case NAV_CONSTANT_RESET_PASSWORD:
                break;

            case NAV_CONSTANT_SHOW_LOGIN:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new com.mobileparadigm.arkangel.cftmobile.ui.fragments.LoginFragment()).setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right).commit();
                break;
            default:
                if (!PreferencesHelper.isSeenIntro()) {
                    fragmentManager.beginTransaction().add(R.id.fragment_container, new UsageViewPagerFragment()).commit();
                } else {
                    Intent intent = new Intent(this, MainMenuActivity.class);
                    finish();
                    startActivity(intent);
                }
                break;
        }
    }


    public static void loadStartingActivity(AppCompatActivity callingActivity) {
        callingActivity.startActivity(new Intent(callingActivity, StartActivity.class));
    }

    private void loginToTwitter() {

     //   if (!PreferencesHelper.getTwitterLoggedIn()) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(getString(R.string.twitter_client_key));
            builder.setOAuthConsumerSecret(getString(R.string.twitter_client_secret));

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(getString(R.string.twitter_callback));
                final Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, TWITTER_LOGIN);

            } catch (TwitterException e) {
                e.printStackTrace();
            }
       // }
    }


    private void startFacebookSession() {
        facebookHelper = new FacebookHelper(this, facebookListener);
        facebookHelper.login();
    }

    private void saveTwitterInfo(AccessToken accessToken) {

        long userID = accessToken.getUserId();
        User user;
        try {
            user = twitter.showUser(userID);

            String username = user.getName();
            // PreferencesHelper.setTwitterToken(accessToken.getToken());
            //  PreferencesHelper.setTwitterSecret(accessToken.getTokenSecret());
            //  PreferencesHelper.setTwitterLoggedIn(true);
            //PreferencesHelper.setTwitterUserName(username);
            if (accessToken != null) {
                firebaseService.authWithOAuthToken("twitter", accessToken.getToken(), new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        Log.v("FacebookHelper: " , "The Twitter user is now authenticated with your Firebase app");
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        // there was an error
                        Log.e("FacebookHelper: " , firebaseError.getDetails());
                    }
                });
            } else {
        /* Logged out of Faceboo so do a logout from the Firebase app */
               firebaseService.unauth();
            }

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }


    protected void linkOAuthThenFinish(Intent intent) {
        setResult(RESULT_OK, intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode != TWITTER_LOGIN) {
            if (resultCode == RESULT_OK) {
                linkOAuthThenFinish(data);
                Intent intent = new Intent(this, MainMenuActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                facebookHelper.onActivityResult(requestCode, resultCode, data);
                //PreferencesHelper.setLoggedIn(true);
                return;
            }
        }

        if (resultCode == Activity.RESULT_OK) {
            verifier = data.getExtras().getString(getString(R.string.twitter_oauth_verifier));
            try {
                Boolean twitterLoginResult = new AsyncTask<Void, Void, Boolean>() {

                    @Override
                    protected Boolean doInBackground(Void... params) {
                        AccessToken accessToken = null;
                        try {
                            accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                            if (accessToken != null) {
                                long userID = accessToken.getUserId();
                                user = twitter.showUser(userID);
                                username = user.getName();
                                saveTwitterInfo(accessToken);
                                return true;
                            }


                        } catch (TwitterException e) {
                            e.printStackTrace();
                        }
                        return false;


                    }


                }.execute().get();

                if (twitterLoginResult) {
                    finish();
                  //  PreferencesHelper.setLoggedIn(twitterLoginResult);
                   // Intent in = new Intent(this, MainMenuActivity.class);
                    MainMenuActivity.startMainMenuActivity(this);
                    //startActivity(in);
                }


            } catch (Exception e) {
                android.util.Log.e("Twitter Login Failed", e.getMessage());
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe
    public void onPushEventFired(ShowNextEvent event) {
        switch (event.getScreenIndex()) {
            case NAV_CONSTANT_SHOW_VIEWPAGER:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new UsageViewPagerFragment()).addToBackStack(null).commit();
                break;
            case NAV_CONSTANT_LOGIN_WITH_TWITTER:
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        loginToTwitter();
                        return null;
                    }
                }.execute();

                break;
            case NAV_CONSTANT_SHOW_LOGIN:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new com.mobileparadigm.arkangel.cftmobile.ui.fragments.LoginFragment()).setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right).commit();
                break;

            case NAV_CONSTANT_SHOW_EMAIL_LOGIN_FORM:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new LoginWithEmailFragment()).setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right).commit();

                break;
            case NAV_CONSTANT_GO_MAIN:
                finish();
                Intent in = new Intent(this, MainMenuActivity.class);
                startActivity(in);
                break;

            case NAV_CONSTANT_SHOW_EMAIL_SIGN_UP:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new SignupFragment()).addToBackStack(null).commit();
                break;
            case NAV_CONSTANT_PRE_SIGNUP:
                fragmentManager.beginTransaction().add(R.id.fragment_container, new PreSignupFragment()).addToBackStack(null).commit();
                break;

            case NAV_CONSTANT_LOGIN_WITH_FACEBOOK:
                startFacebookSession();
                break;

            case NAV_CONSTANT_RESET_EMAIL:
                break;

            case NAV_CONSTANT_RESET_PASSWORD:
                break;

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        if (facebookHelper != null) {
            facebookHelper.cancelPending();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        postFromAnyThreadBus.unregister(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if (BaseFragment.misc_flag) {
            finish();
            BaseFragment.misc_flag = false;
            return;
        }*/
    }

    FacebookHelper.FacebookListener facebookListener = new FacebookHelper.FacebookListener() {
        @Override
        public void loginFailed() {
            CroutonUtils.error(activity, "login failed");

        }

        @Override
        public void inProgress() {
            CroutonUtils.info(activity, "login in progress");
        }

        @Override
        public void loginSuccess(Object user) {
            CroutonUtils.info(activity, "login in succeeded");
        }

        @Override
        public void verified() {
            CroutonUtils.info(activity, "login in verified");
        }
    };


    public static class ShowNextEvent {

        private int screenIndex;

        public int getScreenIndex() {
            return screenIndex;
        }

        public void setScreenIndex(int screenIndex) {
            this.screenIndex = screenIndex;
        }

    }

}