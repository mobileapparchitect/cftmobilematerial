package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CftEvent extends RealmObject {


    private String event_detail_parent;
    @PrimaryKey
    private long cftEventId;
    private String event_title;
    private String event_location;
    private String event_time;
    private String event_detail;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    private String startDate;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    private String endDate;

    public long getCftEventId() {
        return cftEventId;
    }

    public void setCftEventId(long cftEventId) {
        this.cftEventId = cftEventId;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_detail() {
        return event_detail;
    }

    public void setEvent_detail(String event_detail) {
        this.event_detail = event_detail;
    }

    public String getEvent_detail_parent() {
        return event_detail_parent;
    }

    public void setEvent_detail_parent(String event_detail_parent) {
        this.event_detail_parent = event_detail_parent;
    }

    public String getEvent_location() {
        return event_location;
    }

    public void setEvent_location(String event_location) {
        this.event_location = event_location;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }


}