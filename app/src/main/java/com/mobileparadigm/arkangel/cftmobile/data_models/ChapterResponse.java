
package com.mobileparadigm.arkangel.cftmobile.data_models;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ChapterResponse extends BaseObservable implements Parcelable {

    @SerializedName("chapters")
    @Expose
    private List<Chapter> chapters = new ArrayList<Chapter>();
    @SerializedName("meta")
    @Expose
    private Meta meta;

    protected ChapterResponse(Parcel in) {
        chapters = in.createTypedArrayList(Chapter.CREATOR);
    }

    public static final Creator<ChapterResponse> CREATOR = new Creator<ChapterResponse>() {
        @Override
        public ChapterResponse createFromParcel(Parcel in) {
            return new ChapterResponse(in);
        }

        @Override
        public ChapterResponse[] newArray(int size) {
            return new ChapterResponse[size];
        }
    };

    /**
     *
     * @return
     *     The chapters
     */
    public List<Chapter> getChapters() {
        return chapters;
    }

    /**
     *
     * @param chapters
     *     The chapters
     */
    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    /**
     *
     * @return
     *     The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * 
     * @param meta
     *     The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(chapters);
    }
}
