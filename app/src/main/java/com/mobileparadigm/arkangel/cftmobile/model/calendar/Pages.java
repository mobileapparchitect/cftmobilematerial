
package com.mobileparadigm.arkangel.cftmobile.model.calendar;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Pages {

    @SerializedName("page")
    @Expose
    private List<Page> page = new ArrayList<Page>();

    /**
     * 
     * @return
     *     The page
     */
    public List<Page> getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(List<Page> page) {
        this.page = page;
    }

}
