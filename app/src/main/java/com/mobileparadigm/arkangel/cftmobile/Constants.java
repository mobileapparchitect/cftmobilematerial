package com.mobileparadigm.arkangel.cftmobile;

/**
 * Created by arkangel on 02/05/15.
 */
public class Constants {

    public static String BIBLE_REGEX_TAG = "[(\\s+A-Za-z{2,6}\\s+\\d{1,3}:\\d{1,3}]\b";
    public static String BIBLE_REGEX_TAG_TWO = "(?:\\d\\s*)?[A-Z]?[a-z]+\\s*\\d+(?:[:-]\\d+)?(?:\\s*-\\s*\\d+)?(?::\\d+|(?:\\s*[A-Z]?[a-z]+\\s*\\d+:\\d+))?";
    public static final String COMMAND_GET_BIBLE_BOOKS="command_get_bibles";
    public static final String COMMAND_GET_YOUTUBE_CONTENT="command_get_youtubeContent";
    public static final String CHANNEL_CONGREGATION="CONGREGATION";
    public static final String CHANNEL_CHOIR = "CHOIR";
    public static final String CHANNEL_JTA = "JTA";
    public static final String CHANNEL_PROTOCOL = "PROTOCOL";
    public static final String CHANNEL_ANNOUNCEMENT = "Announcements";
    public static final String  BIBLE_VERSION_KJVA = "eng-KJVA";
    public static final String BIBLE_VERSION_AMPLIFIED="eng-AMP";
    public static final String BIBLE_VERSION_ENGLISH_STANDARD = "eng-ESV";
    public static final String BIBLE_VERSION_NEW_AMERICAN_STANDARD="eng-NASB";



    public static final String KEY_PRAYER_POINTS="prayerpoints";
    public static final String KEY_BIBLE_STUDIES="biblestudies";
    public static final String KEY_EVENTS="events";
    public static final String KEY_BULLETINS="bulletins";

    public static final String KEY_ANNOUNCEMENT_NEWS_FLASH_TITLE="news_flash_title";
    public static final String KEY_ANNOUNCEMENT_NEWS_FLASH_DETAIL="news_flash";
    public static final String KEY_ANNOUNCEMENT_NEWS_YEAR_THEME_TITLE="year_theme_title";
    public static  final String KEY_ANNOUNCEMENT_NEWS_DATE="date";

    public static final String KEY_BULLETIN_DETAIL="bulletin_detail";

    public static final String KEY_BULLETIN_TITLE="title";
    public static final String KEY_BULLETIN_THEME="bulletin_theme";
    public static final String KEY_PRAYER_DETAIL="detail";
    public static final String KEY_PRAYER_POINT="prayer_point";
    public static final String KEY_BIBLE_VERSE="verse";
    public static final String KEY_BIBLE_VERSE_DETAIL="verse_detail";
    public static final String KEY_BIBLE_STUDY_LABEL="study_label";

    public static final String KEY_EVENT_DETAIL="detail";
    public static final String KEY_EVENT_START_DATE="start_date";
    public static final String KEY_EVENT_END_DATE="end_date";
    public static final String KEY_EVENT_TIME="time";
    public static final String KEY_EVENT_LOCATION="location";




}
