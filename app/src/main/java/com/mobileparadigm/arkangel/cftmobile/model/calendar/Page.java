
package com.mobileparadigm.arkangel.cftmobile.model.calendar;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Page {

    @SerializedName("page_month")
    @Expose
    private String pageMonth;
    @SerializedName("year_theme")
    @Expose
    private String yearTheme;
    @SerializedName("month_prayer_point")
    @Expose
    private String monthPrayerPoint;
    @SerializedName("theme_of_the_month")
    @Expose
    private String themeOfTheMonth;
    @SerializedName("verses_for_the_month")
    @Expose
    private List<String> versesForTheMonth = new ArrayList<String>();
    @SerializedName("events")
    @Expose
    private List<Event> events = new ArrayList<Event>();

    /**
     * 
     * @return
     *     The pageMonth
     */
    public String getPageMonth() {
        return pageMonth;
    }

    /**
     * 
     * @param pageMonth
     *     The page_month
     */
    public void setPageMonth(String pageMonth) {
        this.pageMonth = pageMonth;
    }

    /**
     * 
     * @return
     *     The yearTheme
     */
    public String getYearTheme() {
        return yearTheme;
    }

    /**
     * 
     * @param yearTheme
     *     The year_theme
     */
    public void setYearTheme(String yearTheme) {
        this.yearTheme = yearTheme;
    }

    /**
     * 
     * @return
     *     The monthPrayerPoint
     */
    public String getMonthPrayerPoint() {
        return monthPrayerPoint;
    }

    /**
     * 
     * @param monthPrayerPoint
     *     The month_prayer_point
     */
    public void setMonthPrayerPoint(String monthPrayerPoint) {
        this.monthPrayerPoint = monthPrayerPoint;
    }

    /**
     * 
     * @return
     *     The themeOfTheMonth
     */
    public String getThemeOfTheMonth() {
        return themeOfTheMonth;
    }

    /**
     * 
     * @param themeOfTheMonth
     *     The theme_of_the_month
     */
    public void setThemeOfTheMonth(String themeOfTheMonth) {
        this.themeOfTheMonth = themeOfTheMonth;
    }

    /**
     * 
     * @return
     *     The versesForTheMonth
     */
    public List<String> getVersesForTheMonth() {
        return versesForTheMonth;
    }

    /**
     * 
     * @param versesForTheMonth
     *     The verses_for_the_month
     */
    public void setVersesForTheMonth(List<String> versesForTheMonth) {
        this.versesForTheMonth = versesForTheMonth;
    }

    /**
     * 
     * @return
     *     The events
     */
    public List<Event> getEvents() {
        return events;
    }

    /**
     * 
     * @param events
     *     The events
     */
    public void setEvents(List<Event> events) {
        this.events = events;
    }

}
