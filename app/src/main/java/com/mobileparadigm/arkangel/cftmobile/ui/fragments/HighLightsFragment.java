package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.common.eventbus.Subscribe;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.adapters.HighLightsRowItemAdapter;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.MainActivityMaterial;
import com.mobileparadigm.arkangel.cftmobile.ui.view.AutoScrollViewPager;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by arkangel on 27/04/15.
 */
public class HighLightsFragment extends
        Fragment {
    private static final int HIDE_THRESHOLD = 20;
/*    @InjectView(R.id.slideshow)
    AutoScrollViewPager viewPager;*/

    /*    @InjectView(R.id.header)
        RecyclerViewHeader recyclerViewHeader;*/
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    /*    @InjectView(R.id.container_home)
        LinearLayout mainContainer;
        @InjectView(R.id.tabernacle_news_date)
        IntelligentTextView datePublished;*/
    private int currentPosition;
    private View header;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;
    private HomePageBannerImagePagerAdapter homePageBannerImagePagerAdapter;
    private HighLightsRowItemAdapter movieRowsRecyclerAdapter;
    private int toolbarMarginOffset = 0;
    private Toolbar toolbar;
    public RecyclerView.OnScrollListener onScrollListenerToolbarHide = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            toolbarMarginOffset += dy;
            if (toolbarMarginOffset > Utils.dpToPx(48)) {
                toolbarMarginOffset = Utils.dpToPx(48);
            }
            if (toolbarMarginOffset < 0) {
                toolbarMarginOffset = 0;
            }
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toolbar.getLayoutParams();
            params.topMargin = -1 * toolbarMarginOffset;
            toolbar.setLayoutParams(params);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.highlights_fragment, container, false);
        ButterKnife.bind(this, view);
       // EventBusProvider.getInstance().register(HighLightsFragment.this);
        //  setRetainInstance(true);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        recyclerView.setHasFixedSize(false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        return view;
    }

    @Subscribe
    public void onDrawablesEvent(MainActivityMaterial.MainActivityMaterialEvent mainActivityMaterialEvent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (movieRowsRecyclerAdapter == null) {
            movieRowsRecyclerAdapter = new HighLightsRowItemAdapter( getActivity(), Arrays.asList(CFTMaterialApplication.getInstance().getResources().getStringArray(R.array.panelTitles)));
        }
        recyclerView.setAdapter(movieRowsRecyclerAdapter);
    }




    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
         /*   new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new MaterialShowcaseView.Builder(getActivity())
                            .setTarget(viewPager)
                            .setDismissText("GOT IT")
                            .setContentText(getResources().getString(R.string.view_pager_tutorial))
                            .setDelay(500) // optional but starting animations immediately in onCreate can make them choppy
                            .singleUse("Pager") // provide a unique ID used to ensure it is only shown once
                            .show();
                }
            }, 2000);*/
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //EventBusProvider.getInstance().unregister(HighLightsFragment.this);
       // CFTMaterialApplication.objects.clear();
    }


    private void hideViews() {
        ((MainActivityMaterial) getActivity()).toolbar.animate().translationY(-((MainActivityMaterial) getActivity()).toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        ((MainActivityMaterial) getActivity()).toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }


    public static class SendEvent {

    }

    private class HomePageBannerImagePagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

        private Activity context;
        private ProgressBar progressBar;
        private int myProgress;
        private List<GlideDrawable> listOfObjects;
        private ImageView featuredImage;

        private AutoScrollViewPager pager;

        public HomePageBannerImagePagerAdapter(Context context, AutoScrollViewPager viewPager) {
            this.context = (Activity) context;
            pager = viewPager;
            listOfObjects = new ArrayList<>();
            if (CftDataHolder.getInstance().getListOfHomeViewPagerDrawables().isEmpty()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadDrawables();
                    }
                });
            } else {
                addAllDrawables(CftDataHolder.getInstance().getListOfHomeViewPagerDrawables());
            }
        }


        public void loadDrawables() {
            List<String> listOfImageUrls = Arrays.asList(CFTMaterialApplication.getInstance().getResources().getStringArray(R.array.cft_pictures_landscape));

            for (int urlIndex = 0; urlIndex < listOfImageUrls.size(); urlIndex++) {
                Glide.with(CFTMaterialApplication.getInstance()).load(listOfImageUrls.get(urlIndex))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                CftDataHolder.getInstance().getListOfHomeViewPagerDrawables().add(resource);
                                listOfObjects.add(resource);
                                notifyDataSetChanged();
                            }

                        });


            }
        }

        public void addToDrawable(GlideDrawable drawable) {
            listOfObjects.add(drawable);
            notifyDataSetChanged();
        }


        public void addAllDrawables(ArrayList<GlideDrawable> drawables) {
            listOfObjects.addAll(drawables);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return listOfObjects.size();
        }

        @Override
        public Object instantiateItem(View container, int position) {
            LayoutInflater inflater = LayoutInflater.from(context);
            LinearLayout view = (LinearLayout) inflater.inflate(
                    R.layout.main_header_view, null);
            featuredImage = (ImageView) view.findViewById(R.id.header_image);
            featuredImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //EventBusProvider.getInstance().post(new SendEvent());
                }
            });

            progressBar = (ProgressBar) view.findViewById(R.id.player_exp_bar);
            progressBar.setProgressDrawable(context.getResources().getDrawable(
                    R.drawable.lightblue_progress));

            if (listOfObjects != null && !listOfObjects.isEmpty()) {
                final GlideDrawable drawable = listOfObjects
                        .get(position);
                featuredImage.setImageDrawable(drawable);
            }

            ((ViewPager) container).addView(view, 0);
            new ProgressThread().start();
            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            pager.stopAutoScroll();
        }

        @Override
        public void onPageSelected(int position) {
            currentPosition = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        private class ProgressThread extends Thread {

            private Handler progressThreadHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    myProgress++;
                    progressBar.setProgress(myProgress);
                }
            };

            @Override
            public void run() {
                while (myProgress < 50) {
                    try {
                        myProgress += 20;
                        progressBar.setProgress(myProgress);
                        progressThreadHandler.sendMessage(progressThreadHandler
                                .obtainMessage());
                        Thread.sleep(200);
                    } catch (Throwable t) {
                    }
                }

            }

        }

    }


}