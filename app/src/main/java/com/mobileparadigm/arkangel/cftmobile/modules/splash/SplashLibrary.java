package com.mobileparadigm.arkangel.cftmobile.modules.splash;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.google.gson.Gson;
import com.mobileparadigm.arkangel.cftmobile.BuildConfig;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.Constants;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.background_jobs.BibleVersesManager;
import com.mobileparadigm.arkangel.cftmobile.data_models.CftLibraryModule;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verse;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verses;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Book;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.BookTable;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Root;
import com.mobileparadigm.arkangel.cftmobile.data_models.firebase.Announcement;
import com.mobileparadigm.arkangel.cftmobile.data_models.youtube.YoutubeVideosContent;
import com.mobileparadigm.arkangel.cftmobile.endpoints.BooksApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.VersesApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.YoutubeService;
import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;
import com.mobileparadigm.arkangel.cftmobile.model.calendar.CalendarHolder;
import com.mobileparadigm.arkangel.cftmobile.model.calendar.Event;
import com.mobileparadigm.arkangel.cftmobile.model.calendar.Pages;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;
import com.mobileparadigm.arkangel.cftmobile.utils.DateUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.mobileparadigm.arkangel.cftmobile.utils.VerseProcessor;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import io.socket.client.Socket;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func3;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class SplashLibrary {
    private static final String TAG = SplashLibrary.class.getSimpleName();
    @Inject
    BooksApiService booksApiService;

    @Inject
    Context context;

    @Inject
    VersesApiService verseApiService;

    @Inject
    Socket mSocket;

    @Inject
    YoutubeService youtubeService;
    @Inject
    Firebase firebaseService;


    @Inject
    VerseProcessor verseProcessor;
    @Inject
    Gson gsonDeserializer;
    private String UID = "-KM5ixm5pKDA2mb5IJ0G";

    public SplashLibrary() {
        CFTMaterialApplication.getAppComponent().inject(this);
        intializeRealm();
        loadFirebase();
        try {
            getCalendarEvents();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (BuildConfig.websocketsEnabled) {
            initalizeSocket();
            if (mSocket.connected()) {
                mSocket.on(Constants.COMMAND_GET_BIBLE_BOOKS, args -> {
                    JSONObject data = (JSONObject) args[0];
                    Root biblebooks = gsonDeserializer.fromJson(data.toString(), Root.class);
                    saveBibleBooksData(biblebooks);
                    Timber.i("Sockets");
                });
                mSocket.on(Constants.COMMAND_GET_YOUTUBE_CONTENT, args -> {
                    JSONObject data = (JSONObject) args[0];
                    YoutubeVideosContent youtubeContent = gsonDeserializer.fromJson(data.toString(), YoutubeVideosContent.class);
                    Log.i(TAG, youtubeContent.toString());
                });
                getVerses();
                callObserveableZip();
            }

        } else {
            Timber.i("Not sockets");
            loadData();
            try {
                getCalendarEvents();
                loadFirebase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    public void initalizeSocket() {
        mSocket.on(Socket.EVENT_CONNECT, args3 -> {
            Timber.i("Websocket Connected");
        });
        mSocket.on(Socket.EVENT_DISCONNECT, args2 -> {
            Timber.i("Websocket DisConnected");
        });
        mSocket.on(Socket.EVENT_CONNECT_ERROR, args1 -> {
            Timber.i("Websocket Connected Error");
        });
        mSocket.on(Socket.EVENT_CONNECT_ERROR, args -> {
            Timber.i("Websocket Connected Error");
        });
        mSocket.connect();
    }


    public Boolean loadFirebase() {
        firebaseService.child("announcement").orderByChild("-KM5ixm5pKDA2mb5IJ0G").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.v("DownloadBulletin", snapshot.toString());
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    if (postSnapshot.getKey().equals("-KM5ixm5pKDA2mb5IJ0G")) {
                        CftDataHolder.getInstance().setAnnouncement(postSnapshot.child("Announcement").getValue(Announcement.class));
                        Log.v("DownloadBulletin", CftDataHolder.getInstance().getAnnouncement().toString());
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError error) {
                Log.v("DownloadBulletin", error.toString());
            }

        });
        return true;
    }

    private void intializeRealm() {
        RealmConfiguration libraryConfig = new RealmConfiguration.Builder(context)
                .name("library.realm")
                .setModules(new CftLibraryModule())
                .build();
        Realm.setDefaultConfiguration(libraryConfig);
    }


    public String initializedString() {
        return "Initialized " + getClass().getSimpleName();
    }


    private void loadData() {
        booksApiService.getBooks(PreferencesHelper.getUserBibleVersion())
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .map(root -> {
                    saveBibleBooksData(root);
                    return true;
                }).subscribe(bibleBooksObserver());
    }


    public Observable<Boolean> getAnnoucementObservable() {

        return Observable.just((loadFirebase()));
    }


    private Subscriber<Boolean> bibleBooksObserver() {
        return new Subscriber<Boolean>() {

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    callObserveableZip();
                }

            }

            ;
        };

    }

    public Observable<Boolean> getCalendarObservable() {
        return Observable.defer(() -> {
            try {
                return Observable.just(getCalendarEvents());
            } catch (IOException e) {
                e.printStackTrace();
                return Observable.error(e);
            }
        });
    }


    @Nullable
    public Boolean getCalendarEvents() throws IOException {
        ArrayList<CalendarEvent> allBaseCalendarEvents = new ArrayList<>();
        CftDataHolder.getInstance().getCalendarEventArrayList().clear();
        ArrayList<Event> allEvents = new ArrayList<>();
        CalendarHolder calendar = gsonDeserializer.fromJson(Utils.loadAssetTextAsString("calendar_2016_update.json"), com.mobileparadigm.arkangel.cftmobile.model.calendar.CalendarHolder.class);
        Pages pages = calendar.getPages();
        for (int count = 0; count < pages.getPage().size(); count++) {
            allEvents.addAll(pages.getPage().get(count).getEvents());
        }
        for (Event event : allEvents) {
            Calendar startTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();

            DateTime dateTime = DateUtils.parseDate(event.getDate());
            startTime.set(Calendar.DAY_OF_MONTH, dateTime.getDayOfMonth());
            startTime.set(Calendar.MONTH, dateTime.getMonthOfYear() - 1);
            startTime.set(Calendar.HOUR_OF_DAY, 01);
            startTime.set(Calendar.MINUTE, 0);

            endTime.set(Calendar.DAY_OF_MONTH, dateTime.getDayOfMonth());
            endTime.set(Calendar.MONTH, dateTime.getMonthOfYear() - 1);
            endTime.set(Calendar.HOUR_OF_DAY, 23);
            endTime.set(Calendar.MINUTE, 0);
            BaseCalendarEvent baseCalendarEvent = new BaseCalendarEvent(event.getEventTitle(), event.getEventDescription(), event.getEventLocation(),
                    Color.parseColor(event.getColorCode()), startTime, endTime, false);
            allBaseCalendarEvents.add(baseCalendarEvent);
        }

        CftDataHolder.getInstance().setCalendarEventArrayList(allBaseCalendarEvents);
        return true;

    }

    public void saveBibleBooksData(Root root) {
        List<Book> books = root.getResponse().getBooks();
        Realm jobRealm = null;
        try {
            jobRealm = Realm.getDefaultInstance();
            RealmResults<BookTable> results = jobRealm.where(BookTable.class).findAll();
            jobRealm.beginTransaction();
            results.clear();
            jobRealm.commitTransaction();
            jobRealm.beginTransaction();
            for (Book book : books) {
                BookTable bookTable = new BookTable();
                bookTable.setAbbreviation(book.getAbbr());
                bookTable.setName(book.getName());
                bookTable.setBooktableId(new Random().nextLong());
                jobRealm.copyToRealmOrUpdate(bookTable);
            }
            jobRealm.commitTransaction();
            jobRealm.close();
            PreferencesHelper.setDatabaseCreated(true);
        } catch (RealmException realmException) {
            jobRealm.cancelTransaction();
            jobRealm.close();
            realmException.printStackTrace();
        }
    }

    public Observable<Boolean> getYoutubeContentObservable() {
        return Observable.defer(() -> {
            try {
                return Observable.just(getYoutubeVideosContent());
            } catch (IOException e) {
                e.printStackTrace();
                return Observable.error(e);
            }
        });
    }


    @Nullable
    public Boolean getYoutubeVideosContent() throws IOException {

        youtubeService.getYoutube("mostPopular", "snippet", "christfaithtabernaclelondon", "AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ")
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .subscribe(new Subscriber<YoutubeVideosContent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(YoutubeVideosContent youtubeVideosContent) {
                        Timber.i("Got youtube: " + youtubeVideosContent.toString());
                        CFTMaterialApplication.youtubeVideosContent = youtubeVideosContent;
                    }
                });

        return false;
    }


    public void callObserveableZip() {

        Observable.zip(getYoutubeContentObservable(), getAnnoucementObservable(), getCustomerOne(), (Func3<Boolean, Boolean, Boolean,Boolean>) (aBoolean, aBoolean2, aBoolean3) -> {
            return null;
        })
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Boolean o) {
                        //done
                    }
                });
    }

    public void getVerses(){
        ArrayList<String> verseSublist = BibleVersesManager.getInstance().getListOfStackedVersed().pop();
        for(String string : verseSublist){
            final ProcessedVerse processedVerse = verseProcessor.process(string);
            final StringBuilder builder = new StringBuilder();
            builder.append(":");
            builder.append(processedVerse.getBookName());
            builder.append(".");
            builder.append(processedVerse.getBookChaper());
            String start = processedVerse.getStartVerse();
            String end = processedVerse.getEndVerse();
            if (TextUtils.isEmpty(end)) {
                end = String.valueOf(Integer.parseInt(start));
            }
            VersesParam param = new VersesParam();
            param.setMain(builder.toString());
            param.setEnd(end);
            param.setStart(start);
            String url = "https://bibles.org/v2/chapters/"+PreferencesHelper.getUserBibleVersion()+param.getMain()+param.getStart()+param.getEnd();
            mSocket.on("command_get_verse", args -> {

            Log.i(TAG,args.toString());
            });
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("URL",url);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("command_get_verse", jsonObject);
        }
    }


    @NonNull
    private Observable<Boolean> getCustomerOne() {
        return Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                return Observable.create(new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {
                        if (subscriber != null && !subscriber.isUnsubscribed()) {
                            Timber.i("In getVersesObservable ");
                            ArrayList<String> verseSublist = BibleVersesManager.getInstance().getListOfStackedVersed().pop();
                            Observable.from(verseSublist).reduce(new Func2<String, String, String>() {
                                @Override
                                public String call(String s, String s2) {
                                    Timber.i("String after map 1 : " + s2);
                                    return s2;
                                }
                            }).map(new Func1<String, VersesParam>() {
                                @Override
                                public VersesParam call(String s) {
                                    final ProcessedVerse processedVerse = verseProcessor.process(s);
                                    final StringBuilder builder = new StringBuilder();
                                    builder.append(":");
                                    builder.append(processedVerse.getBookName());
                                    builder.append(".");
                                    builder.append(processedVerse.getBookChaper());
                                    String start = processedVerse.getStartVerse();
                                    String end = processedVerse.getEndVerse();
                                    if (TextUtils.isEmpty(end)) {
                                        end = String.valueOf(Integer.parseInt(start));
                                    }
                                    VersesParam param = new VersesParam();
                                    param.setMain(builder.toString());
                                    param.setEnd(end);
                                    param.setStart(start);

                                    Timber.i("String after map 2 : " + param.getMain() + ": " + param.getStart() + ": " + param.getEnd());
                                    return param;
                                }
                            }).subscribe(new Action1<VersesParam>() {
                                @Override
                                public void call(VersesParam versesParam) {
                                    verseApiService.getVerses(PreferencesHelper.getUserBibleVersion() + versesParam.getMain(), versesParam.getStart(), versesParam.getEnd())
                                            .subscribeOn(Schedulers.io())//do the work on this thread
                                            .map(new Func1<Verses, Verses>() {
                                                @Override
                                                public Verses call(Verses verses) {
                                                    Log.i(TAG, "GETTING SOMTHING");
                                                    CFTMaterialApplication.bibleVerses.add(verses);
                                                    return null;
                                                }
                                            })
                                            .observeOn(AndroidSchedulers.mainThread())//return the result on
                                            .subscribe(new Subscriber<Verses>() {
                                                //oncomplete and onError mutex. only 1 gets called
                                                @Override
                                                public void onCompleted() {

                                                }

                                                @Override
                                                public void onError(Throwable e) {

                                                }

                                                @Override
                                                public void onNext(Verses verses) {
                                                    Log.i(TAG, "defo SOMTHING: " + verses.toString());
                                                }

                                            });
                                }
                            });


                        }
                    }
                });
            }
        });

    }

    private class VersesParam {
        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        private String main;
        private String start;
        private String end;
    }
}