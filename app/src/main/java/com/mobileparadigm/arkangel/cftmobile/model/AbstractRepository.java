package com.mobileparadigm.arkangel.cftmobile.model;

import android.support.annotation.NonNull;


import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;


public abstract class AbstractRepository<T extends RealmObject> {

    public T save(T t) {
        if ( !isValidToSave(t) ) {
            throw new RuntimeException("Tried to save an invalid item.");
        }

        Realm realm = getRealm();
        realm.beginTransaction();
        t = realm.copyToRealm(t);
        realm.commitTransaction();
        return t;
    }

    public List<T> save(List<T> listOfT) {
        Realm realm = getRealm();
        realm.beginTransaction();
        listOfT = realm.copyToRealm(listOfT);
        realm.commitTransaction();
        return listOfT;
    }

    public T saveOrUpdate(T t) {
        if ( !isValidToSave(t) ) {
            throw new RuntimeException("Tried to save or update an invalid item.");
        }

        Realm realm = getRealm();
        realm.beginTransaction();
        t = realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
        return t;
    }

    public List<T> saveOrUpdate(List<T> listOfT) {
        Realm realm = getRealm();
        realm.beginTransaction();
        listOfT = realm.copyToRealmOrUpdate(listOfT);
        realm.commitTransaction();
        return listOfT;
    }

    public void executeTransaction(@NonNull Runnable transaction) {
        Realm realm = getRealm();
        realm.beginTransaction();
        transaction.run();
        realm.commitTransaction();
    }

    protected Realm getRealm(){
        return Realm.getDefaultInstance();
    }

    public boolean isValidToSave(T t) {
        return true;
    }

    // Abstract methods

    protected abstract Class<T> getModelClass();

    public abstract String getPrimaryKeyName();

    // Load with different types of primary key.

    public T loadWithPrimaryKey(String primaryKeyValue) {
        return getRealm()
                .where(getModelClass())
                .equalTo(getPrimaryKeyName(), primaryKeyValue)
                .findFirst();
    }

    public T loadWithPrimaryKey(Long primaryKeyValue) {
        return getRealm()
                .where(getModelClass())
                .equalTo(getPrimaryKeyName(), primaryKeyValue)
                .findFirst();
    }

}
