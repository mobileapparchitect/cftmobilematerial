package com.mobileparadigm.arkangel.cftmobile.utils;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;

import java.io.IOException;
import java.io.InputStream;



/**
 * Created by arkangel on 24/07/15.
 */
public class FileUtils {


    public  static String readFileAsString(String fileName){
        String tContents = "";

        try {
            InputStream stream = CFTMaterialApplication.getInstance().getAssets().open(fileName);

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }

        return tContents;
    }

}
