/*
package com.mobileparadigm.arkangel.cftmobile.rxjava;


import com.mobileparadigm.arkangel.cftmobile.endpoints.BooksApiService;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;

import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import mobileparadigm.arkangelx.com.back.model.booksresponse.Book;
import mobileparadigm.arkangelx.com.back.model.booksresponse.BookTable;
import mobileparadigm.arkangelx.com.back.model.booksresponse.Root;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

*/
/**
 * Created by arkangel on 05/08/16.
 *//*

public class BooksObservable implements Observable.OnSubscribe<Boolean> {
    private BooksApiService booksApiService;

    public BooksObservable(BooksApiService bookService) {
        booksApiService = bookService;
    }

    @Override
    public void call(final Subscriber<? super Boolean> subscriber) {

        booksApiService.getBooks(PreferencesHelper.getUserBibleVersion())
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .subscribe(new Subscriber<Root>() {
                    //oncomplete and onError mutex. only 1 gets called
                    @Override
                    public void onCompleted() {
                        Timber.i("complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        subscriber.onError(e);
                        Timber.e(e, e.getMessage());

                    }

                    @Override
                    public void onNext(Root root) {
                        List<Book> books = root.getResponse().getBooks();
                        Realm jobRealm = null;
                        try {
                            jobRealm = Realm.getDefaultInstance();
                            RealmResults<BookTable> results = jobRealm.where(BookTable.class).findAll();
                            jobRealm.beginTransaction();
                            results.clear();
                            jobRealm.commitTransaction();
                            jobRealm.beginTransaction();
                            for (Book book : books) {
                                BookTable bookTable = new BookTable();
                                bookTable.setAbbreviation(book.getAbbr());
                                bookTable.setName(book.getName());
                                bookTable.setBooktableId(new Random().nextLong());
                                jobRealm.copyToRealmOrUpdate(bookTable);
                            }
                            jobRealm.commitTransaction();
                            jobRealm.close();
                            PreferencesHelper.setDatabaseCreated(true);
                        } catch (RealmException realmException) {
                            jobRealm.cancelTransaction();
                            jobRealm.close();
                            realmException.printStackTrace();
                        }
                    }


                });
    }
}
*/
