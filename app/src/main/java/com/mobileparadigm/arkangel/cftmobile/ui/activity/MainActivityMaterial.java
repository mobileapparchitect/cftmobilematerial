package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.modules.PostFromAnyThreadBus;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.BibleVersesFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.EventsFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.HighLightsFragment;
import com.mobileparadigm.arkangel.cftmobile.utils.ActivityUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;



/**
 * TODO
 */
public class MainActivityMaterial extends BaseDrawerActivity {
    private static final int ANIM_DURATION_TOOLBAR = 300;
    private static final int ANIM_DURATION_FAB = 400;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    private Adapter adapter;
    private boolean pendingIntroAnimation;
    private ActionBarDrawerToggle toggle;
    @Inject
    Bus postFromAnyThreadBus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        postFromAnyThreadBus.register(this);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        }
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(toggle);
        AppBarLayout.LayoutParams toolbarLayoutParams = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        toolbarLayoutParams.setScrollFlags(0);
        toolbar.setLayoutParams(toolbarLayoutParams);

        CoordinatorLayout.LayoutParams appBarLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        appBarLayoutParams.setBehavior(null);
        appBarLayout.setLayoutParams(appBarLayoutParams);
        ActivityUtils.setupToolbar(this,toolbar);

    }



    @Override
    protected void onResume() {
        super.onResume();
        if (viewpager != null) {
            setupViewPager(viewpager);
            tabLayout.setupWithViewPager(viewpager);
        }else{
            viewpager.setCurrentItem(1);
        }
    }

    @OnClick(R.id.fab)
    public void fabClicked() {
        //CommentsActivity.startCommentsActivity(MainActivityMaterial.this);
    }

    @Subscribe
    public void receiveEvent(HighLightsFragment.SendEvent sendEvent) {
        DetailActivity.startDetailsActivity(MainActivityMaterial.this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (pendingIntroAnimation) {
            pendingIntroAnimation = false;
            startIntroAnimation();
        }
        return true;
    }

    private void startIntroAnimation() {

        int actionbarSize = Utils.dpToPx(56);
        getToolbar().setTranslationY(-actionbarSize);
        getIvLogo().setTranslationY(-actionbarSize);
        getInboxMenuItem().getActionView().setTranslationY(-actionbarSize);

        getToolbar().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(300);
        getIvLogo().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(400);
        getInboxMenuItem().getActionView().animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        //    startContentAnimation();
                    }
                })
                .start();
    }


    private void setupViewPager(ViewPager viewPager) {
        if (adapter == null) {
            adapter = new Adapter(getSupportFragmentManager());
            adapter.addFragment(BibleVersesFragment.getInstance(), "Bible Verses");
            adapter.addFragment(new HighLightsFragment(), "HighLights");
            adapter.addFragment(EventsFragment.getInstance(), "Events");
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(1);
            viewPager.setOffscreenPageLimit(3);
        }
        viewPager.setCurrentItem(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        postFromAnyThreadBus.unregister(this);

    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    public static class MainActivityMaterialEvent {

    }
}
