package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.utils.CroutonUtils;
import com.mobileparadigm.arkangel.cftmobile.ui.view.CustomEditTextView;
import com.mobileparadigm.arkangel.cftmobile.utils.DeviceUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by arkangel on 05/07/15.
 */
public class LoginWithEmailFragment extends BaseFragment implements Validator.ValidationListener {

    @BindView(R.id.loginButton)
    Button loginButton;
    @NotEmpty
    @BindView(R.id.username)
    CustomEditTextView emailEditText;
    @NotEmpty
    @BindView(R.id.signupFieldPassword)
    CustomEditTextView passwordEditText;
    private Activity activity;
    private Validator validator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_login_with_email_form, null);
        validator = new Validator(this);
        activity = getActivity();
        validator.setValidationListener(this);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.loginButton)
    public void loginWithEmail() {
        validator.validate();


    }

    @Override
    public void onValidationSucceeded() {
        if (DeviceUtils.isConnected(getActivity())) {
            Toast.makeText(getActivity(), emailEditText.getText().toString(), Toast.LENGTH_SHORT).show();
            Toast.makeText(getActivity(), passwordEditText.getText().toString(), Toast.LENGTH_SHORT).show();
            firebaseService.createUser(emailEditText.getText().toString(), passwordEditText.getText().toString(), new Firebase.ValueResultHandler<Map<String, Object>>() {
                @Override
                public void onSuccess(Map<String, Object> result) {
                    Log.i("LoginWithEmail ", "Successfully created user account with uid: " + result.get("uid"));
                    showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_GO_MAIN);
                   // PreferencesHelper.setLoggedIn(true);
                    PreferencesHelper.setSeenIntro(true);
            //        EventBusProvider.getInstance().post(showNextEvent);
                }

                @Override
                public void onError(FirebaseError firebaseError) {
                    // there was an error
                }
            });

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                CroutonUtils.error(getActivity(), message);
            }
        }
    }
}
