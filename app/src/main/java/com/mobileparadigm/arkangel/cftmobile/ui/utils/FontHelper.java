package com.mobileparadigm.arkangel.cftmobile.ui.utils;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Typeface;

public class FontHelper {

    private static final HashMap<String, Typeface> mBetfuzeFonts = new HashMap<String, Typeface>();

    public static Typeface getFontFromAssets(Context context, String fontName) {
        Typeface font;
        if (mBetfuzeFonts.containsKey(fontName)) {
            font = mBetfuzeFonts.get(fontName);
        } else {
            font = Typeface.createFromAsset(context.getAssets(), "fonts/"
                    + fontName + ".otf");
            mBetfuzeFonts.put(fontName, font);
        }
        return font;
    }

}