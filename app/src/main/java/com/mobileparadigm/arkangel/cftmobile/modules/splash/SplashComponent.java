package com.mobileparadigm.arkangel.cftmobile.modules.splash;

import com.mobileparadigm.arkangel.cftmobile.SplashActivity;
import com.mobileparadigm.arkangel.cftmobile.modules.ApplicationComponent;
import com.mobileparadigm.arkangel.cftmobile.modules.DataModule;

import dagger.Component;

@Component(
    dependencies = {
        ApplicationComponent.class
    },
    modules = {
        SplashModule.class
    }
)
@SplashScope
public interface SplashComponent {
  void inject(SplashActivity splashActivity);
}
