package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.utils.FontHelper;

public class CustomTextView extends TextView {
    private boolean isPressed = false;

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    private void init(AttributeSet attrs) {
        TypedArray a = getContext()
                .obtainStyledAttributes(
                        attrs,
                        R.styleable.com_mobileparadigm_arkangel_cftmobile_ui_view_CustomTextView);
        String customFont = a
                .getString(R.styleable.com_mobileparadigm_arkangel_cftmobile_ui_view_CustomTextView_font);

        if (!TextUtils.isEmpty(customFont)) {
            setTypeface(FontHelper.getFontFromAssets(getContext(), customFont));
        }
        a.recycle();

    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf);
    }

//	@Override
//	public boolean onTouchEvent(MotionEvent event) {
//		// TODO Auto-generated method stub
//
//		switch (event.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			com.yuza.betfuze_android.log.Log.i("Action Down");
//			isPressed = true;
//			this.invalidate();
//			break;
//		case MotionEvent.ACTION_UP: 
////			isPressed = !isPressed;
//			com.yuza.betfuze_android.log.Log.i("Action Up");
//			this.invalidate();
//
//			break;
//		case MotionEvent.ACTION_MOVE:
//
//			break;
//		}
//		return false;
//	}
//	
//	@Override
//	protected void onDraw(Canvas canvas) {
//		// TODO Auto-generated method stub
//		super.onDraw(canvas);
//		if(isPressed){
//		canvas.translate(2,2);
//		}else{
////			canvas.translate(2,2);
//		}
//	}

}