package com.mobileparadigm.arkangel.cftmobile.ridesharing.match;


import com.google.android.gms.maps.model.LatLng;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Constants;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Driver;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Passenger;

import java.util.Random;

public class Decisions {

	public static void main(String[] args) {
		
		// start with 2 passengers, 1 driver
		String passenger1 = "Passenger1", passenger2 = "Passenger2", driver1 = "Transporter";
		
		Passenger a = getPassenger(passenger1);
		Passenger b = getPassenger(passenger2);

		Driver d = getDriver(driver1);
		
//		if(DEBUG) {
//			System.out.println(d.getUID() + " " + d.getCurrentLocation().getX() + "," + d.getCurrentLocation().getY() + "\n\n");
//			
//			System.out.println(a.getUID() + " " + a.getOrigin().getX() + "," + a.getOrigin().getY());
//			System.out.println(a.getUID() + " " + a.getDestination().getX() + "," + a.getDestination().getY());
//			
//			System.out.println(b.getUID() + " " + b.getOrigin().getX() + "," + b.getOrigin().getY());
//			System.out.println(b.getUID() + " " + b.getDestination().getX() + "," + b.getDestination().getY());
//			
//		}
		
		MatchMaker MVP = new MatchMaker();
		MVP.setPassengers(a,b);
		MVP.setDrivers(d);
		MVP.process();
		
	}
	
	private static Driver getDriver(String UID) {
		// get random location
		LatLng location = getRandomPoint();
		
		return new Driver(UID, location);
	}
	
	private static Passenger getPassenger(String UID) {
		// get random points
		LatLng origin = getRandomPoint();
		LatLng destination = getRandomPoint();
		
		return new Passenger(UID, origin, destination);
	}
	
	private static LatLng getRandomPoint() {

		
		// random latitude in range
		double randomLat = getRandomDoubleInRange(Constants.getMinimumLatitude() ,
				Constants.getMaximumLatitude());
		
		// random longitude in range
		double randomLong = getRandomDoubleInRange(Constants.getMinimumLongitude() , 
				Constants.getMaximumLongitude());
		
		// update fields

		LatLng point = new LatLng(randomLat, randomLong);
		return point;
	}
	
	private static double getRandomDoubleInRange(double minimum, double maximum)  {
		Random rand = new Random();
		double randomVal =  minimum + (maximum - minimum) * rand.nextDouble();
		return randomVal;
	}
}
