package com.mobileparadigm.arkangel.cftmobile.modules;

import android.content.Context;
import android.util.Base64;
import android.view.LayoutInflater;

import com.cloudinary.Cloudinary;
import com.firebase.client.Firebase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobileparadigm.arkangel.cftmobile.BuildConfig;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.data_access_layer.DataAccessManager;
import com.mobileparadigm.arkangel.cftmobile.data_models.CftLibraryModule;
import com.mobileparadigm.arkangel.cftmobile.endpoints.BooksApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.ChaptersService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.VersesApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.YoutubeService;
import com.mobileparadigm.arkangel.cftmobile.utils.VerseProcessor;
import com.squareup.okhttp.HttpUrl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.socket.client.IO;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {


    @Provides
    Gson provideGson() {
        /**
         * GSON instance to use for all request  with date format set up for proper parsing.
         * <p/>
         * You can also configure GSON with different naming policies for your API.
         * Maybe your API is Rails API and all json values are lower case with an underscore,
         * like this "first_name" instead of "firstName".
         * You can configure GSON as such below.
         * <p/>
         *
         * public static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd")
         *         .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES).create();
         */
        return new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    }

    @Provides
    @Named("ApiKey")
    public String generateApiKey() {
        String userpass = "jY0RGOZbm8mohPA1hK8MZZLRbjmY5DCOngdcUQDQ:X" + ":" + "X";
        String basicAuth = "Basic " + android.util.Base64.encodeToString(userpass.getBytes(), Base64.NO_WRAP);
        return basicAuth;
    }
    @Provides
    @Named("BibleOkhttp")
    public okhttp3.OkHttpClient provideLoggingCapableHttpClient(@Named("ApiKey") final String basicAuth) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return new okhttp3.OkHttpClient.Builder().addInterceptor(logging).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("AUTHORIZATION", basicAuth).addHeader("Content-Type", "application/json")
                        .cacheControl(new CacheControl.Builder().maxStale(365, TimeUnit.DAYS).build()).build();
                return chain.proceed(request);
            }
        })
                .addInterceptor(logging)
                .build();
    }

    @Provides
    @Named("YoutubeOkhttp")
    public okhttp3.OkHttpClient provideLoggingCapableYoutbuteHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return new okhttp3.OkHttpClient.Builder().addInterceptor(logging).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Content-Type", "application/json")
                        .cacheControl(new CacheControl.Builder().maxStale(365, TimeUnit.DAYS).build()).build();
                return chain.proceed(request);
            }
        })
                .addInterceptor(logging)
                .build();
    }

    @Provides
    @Named("BibleEndPoint")
    public String provideBaseUrl() {
        return "https://bibles.org/v2/";
    }

    @Provides
    @Named("FirebaseEndPoint")
    public String provideFirebaseBaseUrl() {
        return "https://amber-heat-9438.firebaseio.com/";
    }



    @Provides
    public VerseProcessor providesVersprocessor() {
        return VerseProcessor.getInstance();
    }


    @Provides
    @Singleton
    public Cloudinary providesCloudinary(Context context){
            Map config = new HashMap();
            config.put("cloud_name", context.getResources().getString(R.string.cloudinary_name));
            config.put("api_key", context.getResources().getString(R.string.cloudinary_key));
            config.put("api_secret", context.getResources().getString(R.string.cloudinary_secret));
            return new Cloudinary(config);


    }

    @Provides
    public LayoutInflater providesLayoutInflater(Context context){
       return LayoutInflater.from(context);

    }

    @Provides
    @Named("BibleRetrofit")
    public Retrofit provideRetrofit(@Named("BibleOkhttp") OkHttpClient okHttpClient, @Named("BibleEndPoint") String baseUrl, Gson gson) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Named("YoutubeEndpoint")
    public String provideYoutubeBaseUrl() {
        return "https://www.googleapis.com/";
    }




    @Provides
    @Named("YoutubeRetrofit")
    public Retrofit provideYoutbuteRetrofit(@Named("YoutubeOkhttp") OkHttpClient okHttpClient, @Named("YoutubeEndpoint") String baseUrl, Gson gson) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }


    @Provides
    HttpUrl providesHttpUrl() {

        return new HttpUrl.Builder()
                .scheme("http")
                .host("69.164.218.51").port(3000)
                .addQueryParameter("device", "Android")
                .addQueryParameter("transport", "websocket")
                .build();
    }


    @Provides
    @Singleton
    io.socket.client.Socket providesSocket(HttpUrl baseUrl) {
        io.socket.client.Socket mSocket;
        try {
            IO.Options opts = new IO.Options();
            opts.transports = new String[]{"websocket"};
            mSocket = IO.socket(baseUrl.toString(), opts);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        return mSocket;
    }


    @Provides
    public BooksApiService provideBookService(   @Named("BibleRetrofit") Retrofit retrofit) {
        return retrofit.create(BooksApiService.class);
    }

    @Provides
    public VersesApiService provideVersesService(   @Named("BibleRetrofit") Retrofit retrofit) {
        return retrofit.create(VersesApiService.class);
    }

    @Provides
    public ChaptersService provideChapterService(@Named("BibleRetrofit") Retrofit retrofit) {
        return retrofit.create(ChaptersService.class);
    }

    @Provides
    public YoutubeService provideYoutubeService(   @Named("YoutubeRetrofit") Retrofit retrofit) {
        return retrofit.create(YoutubeService.class);
    }



    @Provides
    @Singleton
    public Firebase providesFirebase(Context context,@Named("FirebaseEndPoint") String baseUrl){
        Firebase.setAndroidContext(context);
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
        return new Firebase(baseUrl);
    }

    @Provides
    @Singleton
    public Realm providesReal(Context context) {

        RealmConfiguration libraryConfig = new RealmConfiguration.Builder(context)
                .name("library.realm")
                .setModules(new CftLibraryModule())
                .build();
        Realm.setDefaultConfiguration(libraryConfig);


    return Realm.getDefaultInstance();
    }
    @Provides
    @Singleton
    public DataAccessManager providesDataAccessManager() {
        return new DataAccessManager();
    }
}