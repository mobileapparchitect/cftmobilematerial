package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.ScrollingMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.eventbus.Subscribe;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.CftBulletin;
import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;
import com.mobileparadigm.arkangel.cftmobile.ui.view.GameSpan;
import com.mobileparadigm.arkangel.cftmobile.ui.view.LinkMovementMethodExt;
import com.mobileparadigm.arkangel.cftmobile.ui.view.MessageSpan;
import com.mobileparadigm.arkangel.cftmobile.ui.view.VerseTagHandler;
import com.mobileparadigm.arkangel.cftmobile.utils.FileUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.mobileparadigm.arkangel.cftmobile.utils.VerseProcessor;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import de.greenrobot.event.EventBus;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class DetailActivity extends BaseActivity implements View.OnClickListener {
    public static final String BUNDLE_KEY = "progress";
    public static final String TAG = DetailActivity.class.getSimpleName();
    private static final String EXTRA_IMAGE = "com.antonioleiva.materializeyourapp.extraImage";
    private static final String EXTRA_TITLE = "com.antonioleiva.materializeyourapp.extraTitle";
    private static StringBuilder builder;
    private static SlidingUpPanelLayout slidingUpPanelLayout;
    public static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }
    };
    private static TextView bibleVerseIndex;
    private static Context context;
    private static boolean firstTime = true;
    private static LinearLayout bibleVerseRowholder;
    private ViewGroup layout;
    private RelativeLayout.LayoutParams relativeLayoutParams;
    private RelativeLayout relativeLayout;
    private ProgressBar progressBar;
    private TextView mainContentTextView;
    private TextView titleTextView;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private BackgroundColorSpan color = null;
    private FloatingActionButton floatingActionButton;
    private SwitchCompat switchCompat;

    public static void startDetailsActivity(Activity activity) {
        activity.startActivity(new Intent(activity, DetailActivity.class));

    }

    public static void runQuery(String verse) {
        builder = new StringBuilder();
        Boolean flag = new Boolean(true);
        EventBus.getDefault().post(flag);
        ProcessedVerse processedVerse = VerseProcessor.getInstance().process(verse);
/*        VersesGetRequestBuilder.newBuilder(PreferencesHelper.getUserBibleVersion(), processedVerse.getBookNumber(), processedVerse.getBookName(), processedVerse.getBookChaper(), processedVerse.getStartVerse(), processedVerse.getEndVerse()).setErrorListener(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("DetailActivity: ", volleyError.toString());
                Snackbar.make(((Activity) context).findViewById(android.R.id.content), "No Verse Found", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
                Boolean flag = new Boolean(false);
                EventBus.getDefault().post(flag);
            }
        }).setResponseListener(new Response.Listener<mobileparadigm.arkangelx.com.back.model.Verses>() {

            @Override
            public void onResponse(mobileparadigm.arkangelx.com.back.model.Verses verses) {
                bibleVerseRowholder.removeAllViews();
                Boolean flag = new Boolean(false);
                EventBus.getDefault().post(flag);
                for (Verse verse : verses.getResponse().getVerses()) {
                    LinearLayout verseRow = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.bible_verse_row, null);
                    TextView rowTextNumber = (TextView) verseRow.findViewById(R.id.bible_verse_number);
                    rowTextNumber.setText(String.valueOf(verse.getVerse()));

                    TextView rowTextDetail = (TextView) verseRow.findViewById(R.id.bible_verse_text);
                    rowTextDetail.setText(Html.fromHtml(verse.getText().toString().replace(String.valueOf(verse.getVerse()), "")));
                    rowTextDetail.setMovementMethod(new ScrollingMovementMethod());
                    bibleVerseRowholder.addView(verseRow,
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT, 0.2f)
                    );
                }
    *//*            Spanned spanned = Html.fromHtml(builder.toString());
                bibleTextView.setText(spanned.toString());*//*
                //   bibleTextView.setMovementMethod(new ScrollingMovementMethod());
                slidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
                    @Override
                    public void onPanelSlide(View panel, float slideOffset) {
                        Log.i(TAG, "onPanelSlide, offset " + slideOffset);
                    }

                    @Override
                    public void onPanelExpanded(View panel) {
                        Log.i(TAG, "onPanelExpanded");
                    }

                    @Override
                    public void onPanelCollapsed(View panel) {
                        Log.i(TAG, "onPanelCollapsed");
                        if (!firstTime)
                            handler.sendMessage(new Message());
                        firstTime = false;
                    }

                    @Override
                    public void onPanelAnchored(View panel) {
                        Log.i(TAG, "onPanelAnchored");
                    }

                    @Override
                    public void onPanelHidden(View panel) {
                        Log.i(TAG, "onPanelHidden");

                    }
                });
                firstTime = true;
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);


            }
        }, mobileparadigm.arkangelx.com.back.model.Verses.class).setTag("Verses").build().execute();
        bibleVerseIndex.setText(verse);*/
    }

    @Subscribe
    public void onEventMainThread(Boolean flag) {
        // showProgressBar(flag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.comments, menu);
        MenuItem menuItem = (MenuItem) menu.findItem(R.id.myswitch);
        switchCompat = (SwitchCompat) menuItem.getActionView().findViewById(R.id.actionbar_service_toggle);
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                //    ParsePush.subscribeInBackground(CommentsActivity.COMMENT_TAG_KEY);
                } else {
                //    ParsePush.unsubscribeInBackground(CommentsActivity.COMMENT_TAG_KEY);
                }
            }
        });
        return true;
    }


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        context = this;
        setContentView(R.layout.sliding_panel);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(this);
        layout = (ViewGroup) findViewById(android.R.id.content).getRootView();
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        bibleVerseIndex = (TextView) findViewById(R.id.bible_verse_index);
        bibleVerseRowholder = (LinearLayout) findViewById(R.id.bible_verse_row_holder);
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_IMAGE);
        supportPostponeEnterTransition();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        String itemTitle = getIntent().getStringExtra(EXTRA_TITLE);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        final ImageView image = (ImageView) findViewById(R.id.image);
        titleTextView = (TextView) findViewById(R.id.title);
        mainContentTextView = (TextView) findViewById(R.id.description);
        EventBus.getDefault().register(DetailActivity.this);
        builder = new StringBuilder();
       // toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /*if (BuildConfig.DEBUG) {
           // mainContentTextView.setText(Html.fromHtml(FileUtils.readFileAsString("message.txt"), null, new VerseTagHandler()));

        } else {
            //load from parse or database
        }*/
        loadData();

       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new MaterialShowcaseView.Builder(DetailActivity.this)
                        .setTarget(floatingActionButton)
                        .setDismissText("GOT IT")
                        .setContentText(getResources().getString(R.string.comments_tutorial))
                        .setDelay(500) //
                        .singleUse("Button")
                        .show();
            }
        },2000);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new MaterialShowcaseView.Builder(DetailActivity.this)
                        .setTarget(switchCompat)
                        .setDismissText("GOT IT")
                        .setContentText(getResources().getString(R.string.comments_slide_button))
                        .setDelay(500) //
                        .singleUse("Slide")
                        .show();
            }
        },8000);
*/


    }

    public void putSpan(Message msg) {
        MessageSpan ms = (MessageSpan) msg.obj;
        Object[] spans = (Object[]) ms.getObj();
        TextView view = ms.getView();

        for (Object span : spans) {
            if (span instanceof GameSpan) {
                int start = Selection.getSelectionStart(view.getText());
                int end = Selection.getSelectionEnd(view.getText());
                String verse = view.getText().toString().substring(start, end);
                ((GameSpan) span).setVerse(verse);
                runQuery(verse);
                Spannable _span = (Spannable) view.getText();
                color = new BackgroundColorSpan(view.getLinkTextColors().getDefaultColor());
                _span.setSpan(color, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                view.setText(_span);

                view.setText(_span);
            }
        }
    }


    public void showProgressBar(boolean showOrHide) {
        if (progressBar == null) {
            progressBar = new ProgressBar(DetailActivity.this, null, android.R.attr.progressBarStyleLarge);
            relativeLayoutParams = new
                    RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            relativeLayout = new RelativeLayout(DetailActivity.this);
        }
        if (showOrHide) {
            ((ViewGroup) layout.getParent()).removeView(relativeLayout);
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            relativeLayout.setGravity(Gravity.CENTER);
            relativeLayout.addView(progressBar);
            layout.addView(relativeLayout, relativeLayoutParams);
        } else {
            progressBar.setProgress(ProgressBar.GONE);
            ((ViewGroup) layout.getParent()).removeView(relativeLayout);

        }


    }

    public void unputSpat(Message msg) {
        MessageSpan ms = (MessageSpan) msg.obj;
        TextView view = ms.getView();
        Spannable _span = (Spannable) view.getText();
        _span.removeSpan(color);
        view.setText(_span);
    }

    @Override
    protected void onResume() {
        super.onResume();
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(DetailActivity.this);
    }


    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    private void applyPalette(Palette palette) {
        int primaryDark = getResources().getColor(R.color.primary_dark);
        int primary = getResources().getColor(R.color.primary);
        collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(primary));
        collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkMutedColor(primaryDark));
        updateBackground((FloatingActionButton) findViewById(R.id.fab), palette);
        supportStartPostponedEnterTransition();
    }

    private void updateBackground(FloatingActionButton fab, Palette palette) {
        int lightVibrantColor = palette.getLightVibrantColor(getResources().getColor(android.R.color.white));
        int vibrantColor = palette.getVibrantColor(getResources().getColor(R.color.accent));
        fab.setRippleColor(lightVibrantColor);
        fab.setBackgroundTintList(ColorStateList.valueOf(vibrantColor));
    }

    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout != null &&
                (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
            finish();
        }
    }


    public void loadData() {
        RealmQuery<CftBulletin> query = Realm.getDefaultInstance().where(CftBulletin.class);
        query.equalTo("bulletin_parent_id", PreferencesHelper.getCurrentAnnouncementId());
        RealmResults<CftBulletin> result = query.findAll();
        if (result != null && !result.isEmpty()) {
            mainContentTextView.setText(Html.fromHtml(result.get(0).getBulletin_detail(), null, new VerseTagHandler()));
            titleTextView.setText(result.get(0).getBulletin_title());
            collapsingToolbarLayout.setTitle(result.get(0).getBulletin_title());
        } else {
            mainContentTextView.setText(Html.fromHtml(FileUtils.readFileAsString("message.txt"), null, new VerseTagHandler()));
        }
        mainContentTextView.setMovementMethod(LinkMovementMethodExt.getInstance(this, GameSpan.class));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

    }

    @Override
    public void onClick(View v) {

        //CommentsActivity.startCommentsActivity(DetailActivity.this);
    }
}