package com.mobileparadigm.arkangel.cftmobile.utils;

import android.text.TextUtils;

import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.BookTable;
import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by arkangel on 24/07/15.
 */
public class VerseProcessor implements Processor {
    private static final int MIN_LENGTH_OF_NAME = 4;
    private static VerseProcessor instance;
    private static ProcessedVerse processedVerse;

    private String verseToProcess;


    public static VerseProcessor getInstance() {
        if (instance == null) {
            processedVerse = new ProcessedVerse();
            instance = new VerseProcessor();
        }
        return instance;
    }

    @Override
    public ProcessedVerse process(String verse) {
      //  verse= "Isaiah 52:12";
        processedVerse = new ProcessedVerse();
        String temp = verse;
        processedVerse.setBookName(temp.replaceAll("[\\d.]", "").replace(":", "").replace("-", "").trim());
        processedVerse.setBookName(processedVerse.getBookName().substring(0, 1).toUpperCase() + processedVerse.getBookName().substring(1));
        int indexOfColon= verse.indexOf(":");
        if(verse.substring(indexOfColon,verse.length()).length()>1){
            String[] splittedParts = verse.split(":");
            if (splittedParts[1].contains("-")) {
                String[] splitHyphen = splittedParts[1].split("-");
                processedVerse.setStartVerse(splitHyphen[0]);
                processedVerse.setEndVerse(splitHyphen[1]);
            } else {
                processedVerse.setStartVerse(splittedParts[1]);
            }

        }else {
            processedVerse.setStartVerse("");
            processedVerse.setEndVerse("");
        }


        int indexOfLastCharacterOfBookName;

        if (Character.isDigit(temp.charAt(0))) {
            int firstSpace = temp.indexOf(" ");
            processedVerse.setBookNumber(temp.substring(0, firstSpace));
            String tt = verse.substring(0, verse.indexOf(":"));
            indexOfLastCharacterOfBookName = verse.indexOf(processedVerse.getBookNumber()) + processedVerse.getBookName().length() + (tt.length() - tt.replaceAll(" ", "").length());
        } else {
            indexOfLastCharacterOfBookName = processedVerse.getBookName().length();

        }

        indexOfLastCharacterOfBookName++;//SKip space
        processedVerse.setBookChaper(verse.substring(indexOfLastCharacterOfBookName, verse.indexOf(":")));


        /**
         * finish search for the right bible name using the abbreviated name
         */
      if(!TextUtils.isEmpty(processedVerse.getBookNumber())){
          processedVerse.setBookName(processedVerse.getBookNumber() + " "+processedVerse.getBookName());
      }
        RealmQuery<BookTable> query = Realm.getDefaultInstance().where(BookTable.class);
        query.equalTo("name", processedVerse.getBookName());
        RealmResults<BookTable> result = query.findAll();
        if (result != null && !result.isEmpty()) {
            processedVerse.setBookName(result.first().getAbbreviation());
        }


        return processedVerse;

    }


}
