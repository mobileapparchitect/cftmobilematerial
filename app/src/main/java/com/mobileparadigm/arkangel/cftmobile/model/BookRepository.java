package com.mobileparadigm.arkangel.cftmobile.model;


import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.BookTable;

public class BookRepository extends AbstractRepository<BookTable> {

    @Override
    protected Class<BookTable> getModelClass() {
        return BookTable.class;
    }

    @Override
    public String getPrimaryKeyName() {
        return "itemNumber";
    }
}
