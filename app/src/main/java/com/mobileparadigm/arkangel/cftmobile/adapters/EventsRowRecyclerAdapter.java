package com.mobileparadigm.arkangel.cftmobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.data_models.firebase.EventList;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class EventsRowRecyclerAdapter extends RecyclerView.Adapter<EventsRowRecyclerAdapter.ViewHolder> {


    public static Context mContext;
    private List<EventList> allEvents;

    public EventsRowRecyclerAdapter(Context context) {
        mContext = context;
        allEvents = CftDataHolder.getInstance().getAnnouncement().getEventList();
    }

    @Override
    public int getItemCount() {
        return allEvents != null ? allEvents.size() : 0;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.titleTextView.setText(allEvents.get(position).getTitle());
        holder.dateTextView.setText(allEvents.get(position).getEventStartDate());
        //    holder.locationTextView.setText(allEvents.get(position).ge());

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int arg1) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.home_card_row_item, parent, false);

        if (allEvents.size() == 1) {
            WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            int width = windowManager.getDefaultDisplay().getWidth();
            int height = windowManager.getDefaultDisplay().getHeight();
            convertView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
        }

        return new ViewHolder(convertView);
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_text_view)
        TextView titleTextView;
        @BindView(R.id.date_text_view)
        TextView dateTextView;
        @BindView(R.id.location_textview)
        TextView locationTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
