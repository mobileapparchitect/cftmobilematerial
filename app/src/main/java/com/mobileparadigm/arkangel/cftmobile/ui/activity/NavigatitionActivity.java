package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.AcknowledgementFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.BibleStudyFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.CftWebViewFragment;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by arkangel on 24/12/2015.
 */
public class NavigatitionActivity extends BaseDrawerActivity {
    public static String FRAGMENT_INDEX="fragmentKey";
    public static String URL_KEY="urlKey";
    public static AppCompatActivity activity;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private static String urlToLoad;
    private static Integer index;

    public static void finishNavigation() {
        if (activity != null) {
            activity.finish();
        }
    }

    public static void showNavigation(Activity callingActivity, Intent intent) {

        callingActivity.startActivity(intent);


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_content);
        ButterKnife.bind(this);
        activity = this;
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(toggle);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            index = bundle.getInt(FRAGMENT_INDEX);
            urlToLoad = bundle.getString(URL_KEY);
            loadFragment(index,urlToLoad);
        }


    }

    public static void loadFragment(int fragmentIndex, String url) {
        Fragment fragment = null;
        switch (fragmentIndex) {

            case 0:
                break;
            case 1:
                break;
            case 2:
                fragment = BibleStudyFragment.getInstance();
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            case 8:
                fragment = AcknowledgementFragment.getInstance();
                break;
            case 9:
                fragment = CftWebViewFragment.getInstance(url);
                break;
        }
        if (fragment != null) {
            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_navigation_container, fragment);
            transaction.commit();
        } else {
            activity.finish();
        }

    }


    @Override
    public void onBackPressed() {
        if (BibleStudyFragment.slidingPaneLayout != null &&
                (BibleStudyFragment.slidingPaneLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || BibleStudyFragment.slidingPaneLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            BibleStudyFragment.slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
            finish();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        activity=null;
    }
}
