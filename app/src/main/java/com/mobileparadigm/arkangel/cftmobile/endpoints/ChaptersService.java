package com.mobileparadigm.arkangel.cftmobile.endpoints;


import com.mobileparadigm.arkangel.cftmobile.data_models.ChapterResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by arkangel on 11/07/16.
 */
public interface ChaptersService {

    //   bibles.org/v2/books/eng-GNTD:1Tim/chapters.js
    @GET("/v2/books/{versions}/chapters.js")
    Observable<ChapterResponse> getChapterCount(@Path("versions") String versions);

}
