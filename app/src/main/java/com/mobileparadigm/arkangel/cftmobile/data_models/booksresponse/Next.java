package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Next  implements Parcelable{

    private static final String FIELD_BOOK = "book";

    @SerializedName(FIELD_BOOK)
    private Book mBook;


    public Next(){

    }

    public void setBook(Book book) {
        mBook = book;
    }

    public Book getBook() {
        return mBook;
    }

    public Next(Parcel in) {
        mBook = in.readParcelable(Book.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Next> CREATOR = new Creator<Next>() {
        public Next createFromParcel(Parcel in) {
            return new Next(in);
        }

        public Next[] newArray(int size) {
        return new Next[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mBook, flags);
    }


}