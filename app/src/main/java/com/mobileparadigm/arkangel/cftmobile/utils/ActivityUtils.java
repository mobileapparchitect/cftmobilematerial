package com.mobileparadigm.arkangel.cftmobile.utils;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mobileparadigm.arkangel.cftmobile.R;


/**
 * Created by joseprodriguez on 15/03/16.
 */
public class ActivityUtils {

    public static ActionBar setupToolbar(AppCompatActivity activity, Toolbar toolbar) {
        activity.setSupportActionBar(toolbar);
        return activity.getSupportActionBar();
    }

    public static void setupToolbarBackArrow(AppCompatActivity activity, Toolbar toolbar) {
        setupToolbar(activity,toolbar).setDisplayHomeAsUpEnabled(true);
    }

   /* public static void setupToolbarCloseIcon(AppCompatActivity activity, Toolbar toolbar) {
        ActionBar actionBar = setupToolbar(activity, toolbar);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);
    }*/
}
