package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Parent   implements Parcelable{

    private static final String FIELD_VERSION = "version";

    @SerializedName(FIELD_VERSION)
    private Version mVersion;


    public Parent(){

    }

    public void setVersion(Version version) {
        mVersion = version;
    }

    public Version getVersion() {
        return mVersion;
    }

    public Parent(Parcel in) {
        mVersion = in.readParcelable(Version.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Parent> CREATOR = new Creator<Parent>() {
        public Parent createFromParcel(Parcel in) {
            return new Parent(in);
        }

        public Parent[] newArray(int size) {
        return new Parent[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mVersion, flags);
    }


}