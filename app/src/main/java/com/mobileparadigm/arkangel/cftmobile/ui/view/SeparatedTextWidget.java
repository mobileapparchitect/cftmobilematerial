package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class SeparatedTextWidget extends LinearLayout {

	final static String DEFAULT_SEPARATOR = ".";

	private List<String> mSeparatedItems = null;
	private int mItemLayoutResource;

	public SeparatedTextWidget(Context context) {
		super(context);
	}

	public SeparatedTextWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setSeparatedItems(List<String> items) {
		mSeparatedItems = items;
	}

	public void setItemLayoutResource(int itemLayout) {
		mItemLayoutResource = itemLayout;
	}

	private void addSeparatedTextItem(String item) {
		TextView child = (TextView) View.inflate(getContext(),
				mItemLayoutResource, null);
		child.setText(item);
		addView(child);
	}

	public void viewSeparatedItems() {
		removeAllViews();
		if (mSeparatedItems != null) {
			for (int i = 0; i < mSeparatedItems.size(); i++) {
				if (i != 0)
					addSeparatedTextItem(DEFAULT_SEPARATOR);
				addSeparatedTextItem(mSeparatedItems.get(i));
			}
		}
	}
}
