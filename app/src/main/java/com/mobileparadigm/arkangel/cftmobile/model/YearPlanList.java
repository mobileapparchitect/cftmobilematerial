package com.mobileparadigm.arkangel.cftmobile.model;

import android.os.Parcel;
import android.os.Parcelable;

public class YearPlanList implements Parcelable{

    private static final String FIELD_PAGES = "pages";


    private Pagex mPage;


    public YearPlanList(){

    }

    public void setPage(Pagex page) {
        mPage = page;
    }

    public Pagex getPage() {
        return mPage;
    }

    public YearPlanList(Parcel in) {
        mPage = in.readParcelable(Pagex.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<YearPlanList> CREATOR = new Creator<YearPlanList>() {
        public YearPlanList createFromParcel(Parcel in) {
            return new YearPlanList(in);
        }

        public YearPlanList[] newArray(int size) {
        return new YearPlanList[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mPage, flags);
    }


}