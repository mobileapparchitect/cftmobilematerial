package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.calendar.DrawableEventRenderer;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;
import com.squareup.otto.Bus;

import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventsFragment extends Fragment implements CalendarPickerController {

    private static final String LOG_TAG = EventsFragment.class.getSimpleName();
    @BindView(R.id.agenda_calendar_view)
    AgendaCalendarView mAgendaCalendarView;
    @Inject
    Bus postFromAnyThread;

    public static EventsFragment getInstance() {
        return new EventsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.eventsfragment, null);
        ButterKnife.bind(this, view);
        CFTMaterialApplication.getAppComponent().inject(this);
        postFromAnyThread.register(EventsFragment.this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //    setRetainInstance(true);

        Calendar minDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        minDate.set(Calendar.MONTH, 1);//miniumum date should be the beginning of every year eg Jan 01 2015
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        maxDate.add(Calendar.YEAR, 1);
        mAgendaCalendarView.init(CftDataHolder.getInstance().getCalendarEventArrayList(), minDate, maxDate, Locale.UK, EventsFragment.this);
        mAgendaCalendarView.addEventRenderer(new DrawableEventRenderer());

    }

    @Override
    public void onDaySelected(DayItem dayItem) {
        Log.d(LOG_TAG, String.format("Selected day: %s", dayItem));
    }

    @com.squareup.otto.Subscribe
    public void onHandleDownloadComplete(EventFragmentEvent eventFragmentEvent) {
   //     new RenderCalendarEventsTask().execute();
    }



    @Override
    public void onEventSelected(CalendarEvent event) {
        Log.d(LOG_TAG, String.format("Selected event: %s", event));
    }

    @Override
    public void onScrollToDate(Calendar calendar) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        postFromAnyThread.unregister(EventsFragment.this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    public static class EventFragmentEvent {

    }

    private class RenderCalendarEventsTask extends AsyncTask<Void, Void, CalendarHolder> {

        @Override
        protected CalendarHolder doInBackground(Void... params) {
            Calendar minDate = Calendar.getInstance();
            Calendar maxDate = Calendar.getInstance();

            minDate.set(Calendar.MONTH, 1);//miniumum date should be the beginning of every year eg Jan 01 2015
            minDate.set(Calendar.DAY_OF_MONTH, 1);
            maxDate.add(Calendar.YEAR, 1);//max date should be end of the year Dec 31 2015

            CalendarHolder holder = new CalendarHolder();
            holder.setMin(minDate);
            holder.setMax(maxDate);
            return holder;
        }

        @Override
        protected void onPostExecute(CalendarHolder calendarHolder) {
            super.onPostExecute(calendarHolder);
            mAgendaCalendarView.init(CftDataHolder.getInstance().getCalendarEventArrayList(), calendarHolder.getMin(), calendarHolder.getMax(), Locale.UK, EventsFragment.this);
            mAgendaCalendarView.addEventRenderer(new DrawableEventRenderer());
        }
    }

    private class CalendarHolder {

        private Calendar min;
        private Calendar max;

        public Calendar getMin() {
            return min;
        }

        public void setMin(Calendar min) {
            this.min = min;
        }

        public Calendar getMax() {
            return max;
        }

        public void setMax(Calendar max) {
            this.max = max;
        }
    }

}
