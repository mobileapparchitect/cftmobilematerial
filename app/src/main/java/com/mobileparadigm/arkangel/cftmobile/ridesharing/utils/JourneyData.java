package com.mobileparadigm.arkangel.cftmobile.ridesharing.utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Set;

/**
 * All data for a specific journey
 * Attributes:
 * 	- start location (Point2D.Double)
 *  - end location (Point2D.Double)
 *  - total time (minutes)
 *  - total distance (miles)
 *  
 * @author Debosmit
 *
 */
public class JourneyData implements Comparable<JourneyData> {

	private long time;	
	private long distance;
	
	private LatLng startLocation;
	private LatLng endLocation;
	
	private Driver driver;
	private ArrayList<Passenger> passengerList;
	private Set<LatLng> waypoints;
	private String ordering;
	
	private String URL;
	
	private int orderingCharacterBuffer;
	
	public LatLng getStartLocation() {
		return startLocation;
	}
	
	public void setStartLocation(LatLng startLocation) {
		this.startLocation = startLocation;
	}
	
	public LatLng getEndLocation() {
		return endLocation;
	}
	
	public void setEndLocation(LatLng endLocation) {
		this.endLocation = endLocation;
	}
	
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	
	public long getDistance() {
		return distance;
	}
	
	public void setDistance(long distance) {
		this.distance = distance;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public ArrayList<Passenger> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(ArrayList<Passenger> passengerList) {
		this.passengerList = passengerList;
	}

	public Set<LatLng> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(Set<LatLng> waypoints) {
		this.waypoints = waypoints;
	}

	public String getOrdering() {
		return ordering;
	}

	public void setOrdering(String ordering) {
		this.ordering = ordering;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}

	@Override
	public int compareTo(JourneyData o) {
		if(this.time < o.time)
			return -1;
		else if(this.time > o.time)
			return 1;
		
		// both  times are equal
		if(this.distance < o.distance)
			return -1;
		return 1;
	}
	
	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		
		st.append("Driver: " + driver.getUID() + "\n");
		st.append("Pick-up/Drop-off list: ");
		for(int i = 0 ; i < ordering.length() ; i++) {
			char passengerCh = ordering.charAt(i);
			passengerCh -= orderingCharacterBuffer;
			Passenger passenger = passengerList.get(passengerCh);
			st.append(passenger.getUID() + " ");
		}
		st.append("\n");
		st.append("Total distance: " + distance + " meters\n");
		st.append("Total time: " + time + " seconds\n\n");
//		st.append("Driver        ->          " + getStartLocation() + "\n");
//		st.append("Final dropoff ->   " + getEndLocation() + "\n");
//		st.append("Stopovers     -> \n");
//		for(Point2D.Double waypoint: waypoints) 
//			st.append("\t\t   " + waypoint + "\n");
//		
		
		return st.toString();
	}

	public void setOrderingCharacterBuffer(int buffer) {
		this.orderingCharacterBuffer = buffer;
	}
	
	public int getOrderingCharacterBuffer() {
		return orderingCharacterBuffer;
	}
}
