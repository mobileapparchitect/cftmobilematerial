package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.view.AccountLoginView;

import butterknife.ButterKnife;


/**
 * Created by arkangel on 03/07/15.
 */
public class LoginFragment extends BaseFragment {

    AccountLoginView accountLoginView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.screen_account_login, null);
        accountLoginView = (AccountLoginView) view.findViewById(R.id.accountLoginView);
        accountLoginView.initView(getActivity(), 0);
        ButterKnife.bind(this, view);
        return view;
    }
}
