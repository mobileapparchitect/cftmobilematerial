package com.mobileparadigm.arkangel.cftmobile.ridesharing.utils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Passenger object
 * Attributes:
 * 	String 		     name
 *  Point2D.Double   current location
 * @author Debosmit
 *
 */
public class Driver {
	
	private String UID;
	private LatLng currentLocation;
	
	public Driver(String name) {
		this(name, (LatLng)null);
	}
	
	public Driver(String name, LatLng location) {
		this.setUID(name);
		this.setCurrentLocation(location);
	}

	public String getUID() {
		return UID;
	}

	public void setUID(String uID) {
		UID = uID;
	}

	public LatLng getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(LatLng currentLocation) {
		this.currentLocation = currentLocation;
	}
}