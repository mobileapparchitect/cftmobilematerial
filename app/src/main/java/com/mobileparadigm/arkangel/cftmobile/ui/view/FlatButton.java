package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import com.mobileparadigm.arkangel.cftmobile.R;

public class FlatButton extends Button {

    private boolean iDoubleText;
    private String[] iText = new String[2];
    private Integer[] iTextSize = new Integer[2];
    private Paint iDropShadowPaint = new Paint();

    private Paint[] iTextPaint = new Paint[2];
    private float iLineHeight;
    private boolean[] iBold = {false, false};
    private int[] iGravity = {Gravity.LEFT, Gravity.LEFT};
    private int iIndent;
    private int iLeading;
    private int textColor;
    private int red, green, blue;

    public FlatButton(Context aContext, AttributeSet aAttrs) {
        super(aContext, aAttrs);
        TypedArray a = aContext.obtainStyledAttributes(aAttrs, R.styleable.FlatButton, 0, 0);
        iDoubleText = a.getBoolean(R.styleable.FlatButton_doubleText, false);
        iText[0] = a.getString(R.styleable.FlatButton_textLine1);
        iText[1] = a.getString(R.styleable.FlatButton_textLine2);
        iTextSize[0] = a.getDimensionPixelSize(R.styleable.FlatButton_textSizeLine1, -1);
        iTextSize[1] = a.getDimensionPixelSize(R.styleable.FlatButton_textSizeLine2, -1);
        iBold[0] = a.getBoolean(R.styleable.FlatButton_textLineBold1, false);
        textColor = a.getColor(R.styleable.FlatButton_textColor, 0);
        iBold[1] = a.getBoolean(R.styleable.FlatButton_textLineBold2, false);
        setGravity(0, a.getString(R.styleable.FlatButton_textLineGravity1));
        setGravity(1, a.getString(R.styleable.FlatButton_textLineGravity2));
        iIndent = a.getInteger(R.styleable.FlatButton_textLineIndent, 0);
        iLeading = a.getInteger(R.styleable.FlatButton_textLineLeading, 0);
        iLineHeight = aContext.getResources().getDimension(R.dimen.flat_button_line_height);
        setTextPaint(aContext, 0);
        setTextPaint(aContext, 1);

        //make the line transparent, decision 4th June Liam/Tom
        iDropShadowPaint.setARGB(0, 0, 0, 0);
        //iDropShadowPaint.setStrokeWidth(iLineHeight);
        a.recycle();
    }

    private void setGravity(int aIndex, final String aGravity) {
        if (aGravity == null || aGravity.trim().length() == 0) return;
        if (aGravity.equalsIgnoreCase("right")) {
            iGravity[aIndex] = Gravity.RIGHT;
            return;
        }
        if (aGravity.equalsIgnoreCase("center")) {
            iGravity[aIndex] = Gravity.CENTER;
            return;
        }
        iGravity[0] = Gravity.LEFT;
    }

    public void setTextPaint(Context aContext, int aIndex) {
        iTextPaint[aIndex] = new Paint();
        iTextPaint[aIndex].setColor(textColor);

        iTextPaint[aIndex].setAntiAlias(true);

        //incase the specific size isn't set, default to textSize Attibute
        float textSize = iTextSize[aIndex];
        if (iTextSize[aIndex] == -1) {
            textSize = getTextSize();
        }

        iTextPaint[aIndex].setTextSize(textSize);
        if (iBold[aIndex]) {
            Typeface tf = Typeface.createFromAsset(aContext.getAssets(), "fonts/Bariol_Bold.otf");
            iTextPaint[aIndex].setTypeface(tf);
        } else {
            Typeface tf = Typeface.createFromAsset(aContext.getAssets(), "fonts/Bariol_Regular.otf");
            iTextPaint[aIndex].setTypeface(tf);
        }
    }


    public void onDraw(Canvas aCanvas) {
        int left = 0;
        int right = getWidth();
        int bottom = (int) (getHeight() - iLineHeight + 5);
        int[] xy = getCoordinates(0, aCanvas.getWidth(), aCanvas.getHeight());
        if (iText[0] != null) aCanvas.drawText(iText[0], xy[0], xy[1], iTextPaint[0]);
        if (iDoubleText && iText[1] != null) {
            xy = getCoordinates(1, aCanvas.getWidth(), aCanvas.getHeight());
            aCanvas.drawText(iText[1], xy[0], xy[1], iTextPaint[1]);
        }

        if (!isPressed()) {
            //draw the shadow line
            aCanvas.drawLine(left, bottom, right, bottom, iDropShadowPaint);
        }

    }

    private int[] getCoordinates(int aIndex, int aWidth, int aHeight) {
        int[] xy = new int[2];
        float ascent = iTextPaint[aIndex].ascent();
        float descent = iTextPaint[aIndex].descent();
        if (iDoubleText) {
            if (aIndex == 1) {
                xy[1] = (int) ((aHeight / 2) - (ascent + descent + (iLeading / 2)));
            } else {
                xy[1] = (int) ((aHeight / 2) + (ascent + descent + (iLeading / 2)));
            }
        } else {
            xy[1] = (int) ((aHeight / 2) - (ascent + descent) / 2);
        }

        if (isPressed()) {
            xy[1] += iLineHeight;
        }

        switch (iGravity[aIndex]) {
            case Gravity.RIGHT:
                iTextPaint[aIndex].setTextAlign(Paint.Align.RIGHT);
                xy[0] = aWidth - iIndent;
                return xy;
            case Gravity.CENTER:
                iTextPaint[aIndex].setTextAlign(Paint.Align.CENTER);
                xy[0] = aWidth / 2;
                return xy;
            default:
                xy[0] = iIndent;
                return xy;
        }
    }

    public void setTopText(final String aText) {
        iText[0] = aText;
    }

    public void setBottomText(final String aText) {
        iText[1] = aText;
    }


}
