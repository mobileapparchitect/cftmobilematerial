package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CftPrayerPoint extends RealmObject {


    private String prayer_detail;
    @PrimaryKey
    private long cftPrayerPointId;
    private String prayer_parent_id;

    public long getCftPrayerPointId() {
        return cftPrayerPointId;
    }

    public void setCftPrayerPointId(long cftPrayerPointId) {
        this.cftPrayerPointId = cftPrayerPointId;
    }


    public String getPrayer_detail() {
        return prayer_detail;
    }

    public void setPrayer_detail(String prayer_detail) {
        this.prayer_detail = prayer_detail;
    }

    public String getPrayer_parent_id() {
        return prayer_parent_id;
    }

    public void setPrayer_parent_id(String prayer_parent_id) {
        this.prayer_parent_id = prayer_parent_id;
    }
}