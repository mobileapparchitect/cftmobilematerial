package com.mobileparadigm.arkangel.cftmobile.utils;

import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;

/**
 * Created by arkangel on 24/07/15.
 */
public interface Processor {

    public ProcessedVerse process(String verse);

}
