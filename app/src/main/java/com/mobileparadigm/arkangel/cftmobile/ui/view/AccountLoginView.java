package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;
import com.mobileparadigm.arkangel.cftmobile.utils.DeviceUtils;

import javax.inject.Inject;


public class AccountLoginView extends RelativeLayout {
    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";
    public static final String USER_PASS = "password";
    public int facebookErrorCount;
    public int twitterErrorCount;
    private StartActivity.ShowNextEvent showNextEvent;
    private Activity parent;



    public AccountLoginView(Context context) {
        super(context);
    }

    public AccountLoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AccountLoginView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initView(Activity parent, int bgResourceId) {
        this.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        this.parent = parent;
        int bgResourceId1 = bgResourceId;
        Button emailButton = (Button) findViewById(R.id.loginChoiceEmailButton);
        emailButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoginWithEmailActivity();
            }
        });
        Button facebookButton = (Button) findViewById(R.id.loginChoiceFacebookButton);
        facebookButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoginWithFacebook();
            }
        });
        Button twitterButton = (Button) findViewById(R.id.loginChoiceTwitterButton);
        twitterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoginWithTwitter();
            }
        });
   /*     Button googleButton = (Button) findViewById(R.id.loginChoiceGoogleButton);
        googleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoginWithGoogle();
            }
        });*/

        showNextEvent = new StartActivity.ShowNextEvent();
    }


    private void startLoginWithTwitter() {
        if (DeviceUtils.isConnected(getContext())) {
            showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_LOGIN_WITH_TWITTER);
         //   EventBusProvider.getInstance().post(showNextEvent);

  /*          Intent i = new Intent(getContext(), AccountLoginTwitterActivity.class);
            i.putExtra(BaseAccountLoginActivity.ERROR_COUNT, twitterErrorCount);
            parent.startActivityForResult(i, AccountLoginActivity.TYPE_LOGIN);
            GoogleAnalyticsUtils.loginFlowAction(getContext(), "LoginWithTwitter");*/
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }

    private void startLoginWithFacebook() {
        if (DeviceUtils.isConnected(getContext())) {
            showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_LOGIN_WITH_FACEBOOK);
            //EventBusProvider.getInstance().post(showNextEvent);

            /*Intent i = new Intent(getContext(), AccountLoginFacebookActivity.class);
            i.putExtra(BaseAccountLoginActivity.ERROR_COUNT, facebookErrorCount);
            parent.startActivityForResult(i, AccountLoginActivity.TYPE_LOGIN);
            GoogleAnalyticsUtils.loginFlowAction(getContext(), "LoginWithFacebook");*/
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.no_connectivity), Toast.LENGTH_SHORT).show();
        }
    }

    private void startLoginWithEmailActivity() {

        showNextEvent.setScreenIndex(StartActivity.NAV_CONSTANT_SHOW_EMAIL_LOGIN_FORM);
     //   EventBusProvider.getInstance().post(showNextEvent);


    }

}
