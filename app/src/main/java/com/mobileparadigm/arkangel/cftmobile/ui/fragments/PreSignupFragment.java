package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.view.AccountSignupView;

import butterknife.ButterKnife;

/**
 * Created by arkangel on 03/07/15.
 */
public class PreSignupFragment extends BaseFragment {
    public static final String TAG = PreSignupFragment.class.getSimpleName();
    AccountSignupView accountSignupView;
    private static PreSignupFragment preSignupFragment;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.screen_pre_sign_up, null);
        accountSignupView = (AccountSignupView) view.findViewById(R.id.accountSignupView);
        accountSignupView.initView(getActivity(), 0);
        ButterKnife.bind(this, view);
        return view;
    }

    public static PreSignupFragment getInstance(){
        preSignupFragment = new PreSignupFragment();
        return preSignupFragment;
    }
}
