package com.mobileparadigm.arkangel.cftmobile.utils;

import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.CftAnnouncement;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.CftEvent;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.CftPrayerPoint;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by arkangel on 28/12/2015.
 */
public class DatabaseUtils {

    public static RealmResults<CftEvent> getAllEvents() {
        RealmQuery<CftEvent> query = Realm.getDefaultInstance().where(CftEvent.class);
        query.equalTo("event_detail_parent", PreferencesHelper.getCurrentAnnouncementId());
        RealmResults<CftEvent> realmResults = query.findAll();
        if (realmResults != null && !realmResults.isEmpty()) {
            return realmResults;
        }
        return null;
    }


    public static RealmResults<CftPrayerPoint> getAllPayers() {
        RealmQuery<CftPrayerPoint> query = Realm.getDefaultInstance().where(CftPrayerPoint.class);
        query.equalTo("prayer_parent_id", PreferencesHelper.getCurrentAnnouncementId());
        RealmResults<CftPrayerPoint> realmResults = query.findAll();
        if (realmResults != null && !realmResults.isEmpty()) {
            return realmResults;
        }
        return null;
    }



    public static RealmResults<CftAnnouncement> getCurrentAnnouncement() {
        RealmQuery<CftAnnouncement> query = Realm.getDefaultInstance().where(CftAnnouncement.class);
        query.equalTo("parentId", PreferencesHelper.getCurrentAnnouncementId());
        RealmResults<CftAnnouncement> realmResults = query.findAll();
        if (realmResults != null && !realmResults.isEmpty()) {
            return realmResults;
        }
        return null;
    }
}
