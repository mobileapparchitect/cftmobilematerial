package com.mobileparadigm.arkangel.cftmobile.services;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

public class GcmIntentService extends IntentService implements ServiceConnection {

private Intent mIntent;

public GcmIntentService() {
super("GcmIntentService");
}

@Override
protected void onHandleIntent(Intent intent) {
}

private void connectToService() {
}

@Override
public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
}

@Override
public void onServiceDisconnected(ComponentName componentName) {
}
}