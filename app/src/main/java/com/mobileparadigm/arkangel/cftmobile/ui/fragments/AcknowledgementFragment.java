package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.util.Linkify;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;


import com.mobileparadigm.arkangel.cftmobile.R;
import com.squareup.otto.Subscribe;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class AcknowledgementFragment extends android.support.v4.app.Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        StickyListHeadersListView stickyListHeadersListView = new StickyListHeadersListView(getActivity());
        stickyListHeadersListView.setAdapter(new SimpleHeaderAdapter(getActivity(),
                getResources().getStringArray(R.array.acknowledgementsTitles),
                getResources().getStringArray(R.array.acknowledgementsText)));
        stickyListHeadersListView.setBackgroundColor(Color.WHITE);
        return stickyListHeadersListView;
    }

  public static AcknowledgementFragment getInstance(){return new AcknowledgementFragment();}

    class SimpleHeaderAdapter extends ArrayAdapter<String> implements StickyListHeadersAdapter, SectionIndexer {

        private int padding;
        private String[] headers;

        public SimpleHeaderAdapter(Context context, String[] headers, String[] text) {
            super(context, R.layout.fanzone_layout, text);
            this.headers = headers;
            padding = getContext().getResources().getDimensionPixelSize(R.dimen.notificationLineItemPadding);
        }

        @Override
        public Object[] getSections() {
            return null;
        }

        @Override
        public int getPositionForSection(int sectionIndex) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            TextView textView;
            if (convertView == null) {
                textView = new TextView(getContext());
                textView.setAutoLinkMask(Linkify.EMAIL_ADDRESSES | Linkify.WEB_URLS);
                textView.setPadding(padding, padding, padding, padding);
            } else {
                textView = (TextView) convertView;
            }
            textView.setText(getItem(position));
            return textView;
        }

        @Override
        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            TextView textView;
            if (convertView == null) {
                textView = new TextView(getContext());
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.blue_dark));
                textView.setPadding(padding, padding, padding, padding);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            } else {
                textView = (TextView) convertView;
            }
            textView.setText(headers[position]);
            return textView;
        }

        @Override
        public long getHeaderId(int position) {
            return position;
        }
    }
}
