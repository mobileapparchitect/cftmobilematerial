package com.mobileparadigm.arkangel.cftmobile.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RecordListHolder implements Parcelable{

    private static final String FIELD_RECORDS = "Records";


    private Record mRecord;


    public RecordListHolder(){

    }

    public void setRecord(Record record) {
        mRecord = record;
    }

    public Record getRecord() {
        return mRecord;
    }

    public RecordListHolder(Parcel in) {
        mRecord = in.readParcelable(Record.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RecordListHolder> CREATOR = new Creator<RecordListHolder>() {
        public RecordListHolder createFromParcel(Parcel in) {
            return new RecordListHolder(in);
        }

        public RecordListHolder[] newArray(int size) {
        return new RecordListHolder[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mRecord, flags);
    }


}