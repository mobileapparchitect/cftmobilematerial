/*
package com.mobileparadigm.arkangel.cftmobile.utils;

import com.mobileparadigm.arkangel.cftmobile.Constants;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

import mobileparadigm.arkangelx.com.back.logs.Log;

*/
/**
 * Created by arkangel on 04/05/15.
 *//*

public class CftMobileDispatchManager {

 */
/*   public static List<ParseObject> allEventsList;
    public static List<ParseObject> allPrayerPointList;
    public static List<ParseObject> allBibleStudies;
    public static List<ParseObject> allBulletins;*//*

    private static CftMobileDispatchManager cftMobileDispatchManagerInstance;

    public static CftMobileDispatchManager getInstance(ParseObject parseObject) {
        if (cftMobileDispatchManagerInstance == null) {
            cftMobileDispatchManagerInstance = new CftMobileDispatchManager();
        }
       runAllQueries(parseObject);
        return cftMobileDispatchManagerInstance;
    }




    private static void runAllQueries(ParseObject object) {
        allEventsList=new ArrayList<>();
        allPrayerPointList=new ArrayList<>();
        allBibleStudies=new ArrayList<>();
        allBulletins=new ArrayList<>();
        getAllEvents(object);
        getAllPrayerPoints(object);
        getAllBulletins(object);
        getAllBibleStudies(object);
    }

    public static List<ParseObject> getAllEvents(ParseObject parseObject) {
        allEventsList = parseObject.getList(Constants.KEY_EVENTS);
        if (allEventsList == null)
            return null;
        ParseObject.fetchAllIfNeededInBackground(allEventsList, new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    Log.e("MobileDispatcher"+e.getMessage());
                    return;
                }
            }
        });
        return allEventsList;
    }

    public static List<ParseObject> getAllPrayerPoints(ParseObject parseObject) {
        allPrayerPointList = parseObject.getList(Constants.KEY_PRAYER_POINTS);
        if (allPrayerPointList == null)
            return null;
        ParseObject.fetchAllIfNeededInBackground(allPrayerPointList, new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    Log.e("MobileDispatcher"+e.getMessage());
                    return;
                }
  }
        });
        return allPrayerPointList;
    }

    public static List<ParseObject> getAllBibleStudies(ParseObject object) {
        allBibleStudies = object.getList(Constants.KEY_BIBLE_STUDIES);
        if (allBibleStudies == null)
            return null;

        ParseObject.fetchAllIfNeededInBackground(allBibleStudies, new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    Log.e("MobileDispatcher"+e.getMessage());
                    return;
                }
            }
        });
        return allBibleStudies;
    }

    public static List<ParseObject> getAllBulletins(ParseObject parseObject) {
        allBulletins = parseObject.getList(Constants.KEY_BULLETINS);
        if (allBulletins == null)
            return null;
        ParseObject.fetchAllIfNeededInBackground(allBulletins, new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    Log.e("MobileDispatcher"+e.getMessage());
                    return;
                }
             }
        });
        return allBulletins;
    }


}
*/
