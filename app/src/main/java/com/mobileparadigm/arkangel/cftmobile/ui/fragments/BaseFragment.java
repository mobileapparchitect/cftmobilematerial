package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.firebase.client.Firebase;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;

import javax.inject.Inject;

public class BaseFragment extends Fragment {
    public StartActivity.ShowNextEvent showNextEvent;
    @Inject
    Firebase firebaseService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showNextEvent = new StartActivity.ShowNextEvent();
    }

}