package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Response  implements Parcelable{

    private static final String FIELD_META = "meta";
    private static final String FIELD_BOOKS = "books";


    @SerializedName("meta")
    private Metum mMetum;
    @SerializedName("books")
    private List<Book> mBooks;


    public void setMetum(Metum metum) {
        mMetum = metum;
    }

    public Metum getMetum() {
        return mMetum;
    }

    public void setBooks(List<Book> books) {
        mBooks = books;
    }

    public List<Book> getBooks() {
        return mBooks;
    }

    public Response(Parcel in) {
        mMetum = in.readParcelable(Metum.class.getClassLoader());
        mBooks = new ArrayList<Book>();
        in.readTypedList(mBooks, Book.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Response> CREATOR = new Creator<Response>() {
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
        return new Response[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mMetum, flags);
        dest.writeTypedList(mBooks);
    }


}