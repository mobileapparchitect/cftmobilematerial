package com.mobileparadigm.arkangel.cftmobile.modules;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.SplashActivity;
import com.mobileparadigm.arkangel.cftmobile.data_access_layer.DataAccessManager;
import com.mobileparadigm.arkangel.cftmobile.modules.splash.SplashLibrary;
import com.mobileparadigm.arkangel.cftmobile.modules.splash.SplashModule;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.BaseActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.InitializationActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.MainMenuActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.StartActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.EventsFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.HighLightsFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.LoginWithEmailFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.SignupFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.utils.FacebookHelper;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { ApplicationModule.class ,DataModule.class, SplashModule.class})
public interface ApplicationComponent {
    void inject(CFTMaterialApplication target);
    void inject(MainMenuActivity target);
    void inject(BaseActivity target);
    void inject(InitializationActivity target);
    //void inject(StartActivity target);
    void inject (LoginWithEmailFragment target);
    void inject (SignupFragment target);
    void inject (FacebookHelper target);
    void inject (HighLightsFragment target);
    void inject (DataAccessManager target);
    void inject (EventsFragment target);
    void inject (SplashLibrary target);
}