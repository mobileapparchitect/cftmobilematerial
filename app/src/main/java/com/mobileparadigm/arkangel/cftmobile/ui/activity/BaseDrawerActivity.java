package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;


/**
 * Created by Miroslaw Stanek on 15.07.15.
 */
public class BaseDrawerActivity extends BaseActivity {

    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;
/*    @InjectView(R.id.ivMenuUserProfilePhotoFix)
    ImageView ivMenuUserProfilePhoto;*/


    @BindView(R.id.vNavigation)
    NavigationView navigationView;
    private int avatarSize;
    private String profilePhoto;
    private List<String> listOfUrls;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentViewWithoutInject(R.layout.activity_drawer);
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.flContentRoot);
        LayoutInflater.from(this).inflate(layoutResID, viewGroup, true);
        listOfUrls = Arrays.asList(CFTMaterialApplication.getInstance().getResources().getStringArray(R.array.social_media_urls));
        injectViews();
        //chapters/eng-KJVA:1Cor.2/verses.js
        //setupHeader();
      /*  navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Bundle bundle;
                Intent intent;
                switch (menuItem.getItemId()) {
                    case R.id.menu_home:
                        NavigatitionActivity.finishNavigation();
                        break;

                    case R.id.menu_prayer_requests:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 1);
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        break;


                    case R.id.menu_bible_study:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 2);
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(2, null);
                        }
                        break;


                    case R.id.menu_assistance:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 4);
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        break;

                    case R.id.menu_discipleship:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 5);
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        break;

                    case R.id.menu_credits:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 8);
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(8, null);
                        }
                        break;

                    case R.id.menu_settings:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 7);
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        break;


                    case R.id.menu_facebook:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 9);
                        bundle.putString(NavigatitionActivity.URL_KEY, listOfUrls.get(0));
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(9, listOfUrls.get(0));
                        }
                        break;

                    case R.id.menu_twitter:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 9);
                        bundle.putString(NavigatitionActivity.URL_KEY, listOfUrls.get(3));
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(9, listOfUrls.get(3));
                        }
                        break;


                    case R.id.menu_website:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 9);
                        bundle.putString(NavigatitionActivity.URL_KEY, listOfUrls.get(1));
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(9, listOfUrls.get(1));
                        }
                        break;

                    case R.id.menu_youtube:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 9);
                        bundle.putString(NavigatitionActivity.URL_KEY, listOfUrls.get(4));
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(9, listOfUrls.get(4));
                        }
                        break;
                    case R.id.menu_apostle:
                        bundle = new Bundle();
                        bundle.putInt(NavigatitionActivity.FRAGMENT_INDEX, 9);
                        bundle.putString(NavigatitionActivity.URL_KEY, listOfUrls.get(2));
                        intent = new Intent(BaseDrawerActivity.this, NavigatitionActivity.class);
                        intent.putExtras(bundle);
                        if (NavigatitionActivity.activity == null) {
                            NavigatitionActivity.showNavigation(BaseDrawerActivity.this, intent);
                        } else {
                            NavigatitionActivity.loadFragment(9, listOfUrls.get(2));
                        }
                        break;


                }
                drawerLayout.closeDrawers();
                return true;
            }
        });*/
    }

    @Override
    protected void setupToolbar() {
        super.setupToolbar();
        if (getToolbar() != null) {
            getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            });
        }


    }

    /*@OnClick(R.id.vGlobalMenuHeader)
    public void onGlobalMenuHeaderClick(final View v) {
        drawerLayout.closeDrawer(Gravity.LEFT);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            *//*    int[] startingLocation = new int[2];
                v.getLocationOnScreen(startingLocation);
                startingLocation[0] += v.getWidth() / 2;
                UserProfileActivity.startUserProfileFromLocation(startingLocation, BaseDrawerActivity.this);
                overridePendingTransition(0, 0);*//*
            }
        }, 200);
    }*/

   /* private void setupHeader() {
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.global_menu_avatar_size);
        this.profilePhoto = getResources().getString(R.string.logo_url);
        Glide.with(this).load(profilePhoto).placeholder(R.drawable.img_circle_placeholder).centerCrop().override(avatarSize, avatarSize)
                .bitmapTransform(new CircleTransform(this))
                .into(ivMenuUserProfilePhoto);
    }*/

}
