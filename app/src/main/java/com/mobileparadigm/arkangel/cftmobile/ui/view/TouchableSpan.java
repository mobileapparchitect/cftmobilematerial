package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;

public abstract class TouchableSpan extends ClickableSpan {
    private boolean mIsPressed;
    private int mNormalTextColor;
    private int mPressedTextColor;

    public TouchableSpan() {
        int mColour = CFTMaterialApplication.getInstance().getResources().getColor(R.color.blue_dark);
        mNormalTextColor = mColour;
        mPressedTextColor = mColour;
    }

    public void setPressed(boolean isSelected) {
        mIsPressed = isSelected;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(mIsPressed ? mPressedTextColor : mNormalTextColor);
        ds.setUnderlineText(false);
    }
}