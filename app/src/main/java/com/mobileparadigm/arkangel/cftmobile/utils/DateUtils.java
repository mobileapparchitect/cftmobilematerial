package com.mobileparadigm.arkangel.cftmobile.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.google.common.base.Preconditions.checkArgument;
/**
 * Created by arkangel on 01/12/2015.
 */
public class DateUtils {




    public static  enum DAY_TYPES {MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY};
    public static DAY_TYPES day_types;
    public static  DateTime parseDate(String date){
       return new DateTime(date,  DateTimeZone.UTC);// 2014-09-23T23:03:11.000Z (OK)
    }



    public static String getDateOfWeek(LocalDate newDate) {
        return DateTimeFormat.forPattern("EEEE").print(newDate);
    }
 /*   public static LocalDate getDate(ParseObject parseObject) {
        LocalDate localDate = new LocalDate(parseObject.getDate("date"));
        return localDate;
    }*/

    public static String getFullDateInBannerFormat(LocalDate date){
        String month =  date.monthOfYear().getAsText();
        String dayOfWeek = DateTimeFormat.forPattern("EEEE").print(date);
        String dayOfMonth = date.toString("dd");
        int year = date.getYear();
        StringBuilder builder = new StringBuilder();
        builder.append(dayOfWeek + ", ");
        builder.append(dayOfMonth + getDayOfMonthSuffix(Integer.parseInt(dayOfMonth))+ " ");
        builder.append(month + " ");
        builder.append(year);
        return builder.toString();
    }


    private static String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }
    public static Date UTCDateToLocal(Date date, Locale locale) {
        // TODO: confirm the issue we had before is fixed.
        try {
            SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
            destFormat.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = utcFormat.parse(destFormat.format(date));
        } catch (ParseException e) {
         //   Log.e( "UTCDateToLocalPrettyTime: Cant parse date," + " e: " + e.toString());
        }
        return date;
    }

    public static Date localTimeToUTC(Date date, Locale locale) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
        try {
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            final String utcTime = dateFormat.format(date);
            dateFormat.setTimeZone(TimeZone.getDefault());
            date = dateFormat.parse(utcTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
