package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.WelcomeActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.view.VerseTagHandler;
import com.mobileparadigm.arkangel.cftmobile.utils.FileUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * The Terms of Service fragment in the welcome screen.
 */
public class VisionFragment extends WelcomeFragment implements WelcomeActivity.WelcomeActivityContent {
    //private static final String TAG = makeLogTag(TosFragment.class);

    @BindView(R.id.statement_of_faith)
    TextView statementOfFaithTextView;
    @Override
    public boolean shouldDisplay(Context context) {
        return PreferencesHelper.isConductAccepted();

    }

    @Override
    protected View.OnClickListener getPositiveListener() {
        return new WelcomeFragmentOnClickListener(mActivity) {
            @Override
            public void onClick(View v) {
                PreferencesHelper.markConductAccepted(true);
                doNext();
            }
        };
    }

    @Override
    protected View.OnClickListener getNegativeListener() {
        return new WelcomeFragmentOnClickListener(mActivity) {
            @Override
            public void onClick(View v) {
                doFinish();
            }
        };
    }

    @Override
    protected String getPositiveText() {
        return getResourceString(R.string.accept);
    }

    @Override
    protected String getNegativeText() {
        return getResourceString(R.string.decline);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment.
        View view = inflater.inflate(R.layout.tos_fragment, container, false);
        ButterKnife.bind(this, view);
        statementOfFaithTextView.setText(Html.fromHtml(FileUtils.readFileAsString("vision.txt"), null, new VerseTagHandler()));
        return view;
    }
}
