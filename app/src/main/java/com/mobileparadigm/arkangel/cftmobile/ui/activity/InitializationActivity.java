package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.Manifest;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.google.gson.Gson;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.background_jobs.BibleVersesManager;
import com.mobileparadigm.arkangel.cftmobile.data_access_layer.DataAccessManager;
import com.mobileparadigm.arkangel.cftmobile.data_models.ChapterResponse;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verses;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Book;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.BookTable;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.Root;
import com.mobileparadigm.arkangel.cftmobile.data_models.firebase.Announcement;
import com.mobileparadigm.arkangel.cftmobile.data_models.youtube.YoutubeVideosContent;
import com.mobileparadigm.arkangel.cftmobile.endpoints.BooksApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.ChaptersService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.VersesApiService;
import com.mobileparadigm.arkangel.cftmobile.endpoints.YoutubeService;
import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;
import com.mobileparadigm.arkangel.cftmobile.model.calendar.CalendarHolder;
import com.mobileparadigm.arkangel.cftmobile.model.calendar.Event;
import com.mobileparadigm.arkangel.cftmobile.model.calendar.Pages;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;
import com.mobileparadigm.arkangel.cftmobile.utils.DateUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.mobileparadigm.arkangel.cftmobile.utils.VerseProcessor;
import com.nobrain.android.permissions.AndroidPermissions;
import com.squareup.otto.Bus;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import io.socket.client.Socket;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func5;
import rx.schedulers.Schedulers;
import timber.log.Timber;


/**
 * Created by arkangel on 15/12/2015.
 */
public class InitializationActivity extends AppCompatActivity {
    private static AppCompatActivity mActivity;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parent)
    RelativeLayout parentLayout;
    private String UID = "-KM5ixm5pKDA2mb5IJ0G";
    private Subscription loadBibleBooksSubscription;
    private Subscription loadObservableZipSubscription;

    @Inject
    BooksApiService bookService;

    @Inject
    VerseProcessor verseprocessor;

    @Inject
    YoutubeService youtubeService;
    @Inject
    ChaptersService chaptersService;
    @Inject
    VersesApiService versesApiService;
    @Inject
    Bus postFromAnyThreadBus;

    @Inject
    Gson gsonDeserializer;
    @Inject
    Firebase firebaseService;

    @Inject
    Socket mSocket;




    @Inject
    DataAccessManager dataAccessManager;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initialization_activity_layout);
        ButterKnife.bind(this);
        mActivity = this;
         ((CFTMaterialApplication) getApplication()).getAppComponent().inject(this);
       /* if(!dataAccessManager.getCompositeSubscription().hasSubscriptions()){
            dataAccessManager.getCompositeSubscription().unsubscribe();
            dataAccessManager.loadData();
        }*/
      //     dataAccessManager.getBibleData();
     //     dataAccessManager.getYoutubeContent();
/*        dataAccessManager.getAnnoucementObservable().subscribeOn(Schedulers.io()).map(new Func1<Boolean, Boolean>() {
            @Override
            public Boolean call(Boolean aBoolean) {
                Timber.i("Got Announcement: " + aBoolean);
                progressBar.setVisibility(View.GONE);
                return aBoolean;
            }
        }).observeOn(AndroidSchedulers.mainThread());//return the result on;*/
    }



    private void loadData() {
        loadBibleBooksSubscription = bookService.getBooks(PreferencesHelper.getUserBibleVersion())
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .map(new Func1<Root, Boolean>() {
                    @Override
                    public Boolean call(Root root) {
                        List<Book> books = root.getResponse().getBooks();
                        Realm jobRealm = null;
                        try {
                            jobRealm = Realm.getDefaultInstance();
                            RealmResults<BookTable> results = jobRealm.where(BookTable.class).findAll();
                            jobRealm.beginTransaction();
                            results.clear();
                            jobRealm.commitTransaction();
                            jobRealm.beginTransaction();
                            for (Book book : books) {
                                BookTable bookTable = new BookTable();
                                bookTable.setAbbreviation(book.getAbbr());
                                bookTable.setName(book.getName());
                                bookTable.setBooktableId(new Random().nextLong());
                                jobRealm.copyToRealmOrUpdate(bookTable);
                            }
                            jobRealm.commitTransaction();
                            jobRealm.close();
                            PreferencesHelper.setDatabaseCreated(true);
                        } catch (RealmException realmException) {
                            jobRealm.cancelTransaction();
                            jobRealm.close();
                            realmException.printStackTrace();
                            return false;
                        }
                        return true;
                    }
                })
                .subscribe(new Subscriber<Boolean>() {
                    //oncomplete and onError mutex. only 1 gets called
                    @Override
                    public void onCompleted() {
                        Timber.i("complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, e.getMessage() + "completed with error");
                        handleError();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            callObserveableZip();
                            Timber.i("completed and zip called ");
                        }
                    }


                });
    }


    public void callObserveableZip() {

        loadObservableZipSubscription = Observable.zip(getAnnoucementObservable(), getYoutubeContentObservable(), getVersesObservable(), getCalendarObservable(), getChaptersObservable(), (Func5<Boolean, Boolean, Boolean, Boolean, Boolean, Boolean>) (aBoolean, aBoolean2, aBoolean3, aBoolean4, aBoolean5) -> {
            if (aBoolean && aBoolean2 && aBoolean3 && aBoolean4 && aBoolean5) {
                if (!PreferencesHelper.isTosAccepted()) {
                    WelcomeActivity.startWelcomeActivity(mActivity);
                } else {
                    MainMenuActivity.startMainMenuActivity(mActivity);
                }

            }
            return null;
        })
                .subscribeOn(Schedulers.io())//do the work on this thread
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        finish();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())//return the result on

                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError();
                    }

                    @Override
                    public void onNext(Boolean o) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }


    @NonNull
    private Observable<Boolean> getCustomerOne() {
        return Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                return Observable.create(new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {
                        if (subscriber != null && !subscriber.isUnsubscribed()) {
                            Timber.i("In getVersesObservable ");
                            ArrayList<String> verseSublist = BibleVersesManager.getInstance().getListOfStackedVersed().pop();
                            Observable.from(verseSublist).reduce(new Func2<String, String, String>() {
                                @Override
                                public String call(String s, String s2) {
                                    Timber.i("String after map 1 : " + s2);
                                    return s2;
                                }
                            }).map(new Func1<String, VersesParam>() {
                                @Override
                                public VersesParam call(String s) {
                                    final ProcessedVerse processedVerse = verseprocessor.process(s);
                                    final StringBuilder builder = new StringBuilder();
                                    builder.append(":");
                                    builder.append(processedVerse.getBookName());
                                    builder.append(".");
                                    builder.append(processedVerse.getBookChaper());
                                    String start = processedVerse.getStartVerse();
                                    String end = processedVerse.getEndVerse();
                                    if (TextUtils.isEmpty(end)) {
                                        end = String.valueOf(Integer.parseInt(start));
                                    }
                                    VersesParam param = new VersesParam();
                                    param.setMain(builder.toString());
                                    param.setEnd(end);
                                    param.setStart(start);

                                    Timber.i("String after map 2 : " + param.getMain() + ": " + param.getStart() + ": " + param.getEnd());
                                    return param;
                                }
                            }).subscribe(new Action1<VersesParam>() {
                                @Override
                                public void call(VersesParam versesParam) {
                                    versesApiService.getVerses(PreferencesHelper.getUserBibleVersion() + versesParam.getMain(), versesParam.getStart(), versesParam.getEnd());

                                }
                            });
                            subscriber.onNext(new Boolean(true));
                            subscriber.onCompleted();
                        }
                    }
                });
            }
        });

    }

    public Observable<Boolean> getVersesbyObservable() {
        Timber.i("In getVersesObservable ");
        ArrayList<String> verseSublist = BibleVersesManager.getInstance().getListOfStackedVersed().pop();
        Observable.from(verseSublist).reduce(new Func2<String, String, String>() {
            @Override
            public String call(String s, String s2) {
                Timber.i("String after map 1 : " + s2);
                return s2;
            }
        }).map(new Func1<String, VersesParam>() {
            @Override
            public VersesParam call(String s) {

                final ProcessedVerse processedVerse = verseprocessor.process(s);
                final StringBuilder builder = new StringBuilder();
                builder.append(":");
                builder.append(processedVerse.getBookName());
                builder.append(".");
                builder.append(processedVerse.getBookChaper());
                String start = processedVerse.getStartVerse();
                String end = processedVerse.getEndVerse();
                if (TextUtils.isEmpty(end)) {
                    end = String.valueOf(Integer.parseInt(start));
                }
                VersesParam param = new VersesParam();
                param.setMain(builder.toString());
                param.setEnd(end);
                param.setStart(start);

                Timber.i("String after map 2 : " + param.getMain() + ": " + param.getStart() + ": " + param.getEnd());
                return param;
            }
        }).subscribe(new Action1<VersesParam>() {
            @Override
            public void call(VersesParam versesParam) {
           /*      versesApiService.getVerses(PreferencesHelper.getUserBibleVersion() + versesParam.getMain(), versesParam.getStart(), versesParam.getEnd()).subscribe(new Action1<Verses>() {
                     @Override
                     public void call(Verses verses) {
                         Timber.i("I think i got my result");

                         CFTMaterialApplication.bibleVerses.add(verses);
                     }


                 });*/

                versesApiService.getVerses(PreferencesHelper.getUserBibleVersion() + versesParam.getMain(), versesParam.getStart(), versesParam.getEnd())
                        .subscribeOn(Schedulers.io())//do the work on this thread
                        .map(new Func1<Verses, Verses>() {
                            @Override
                            public Verses call(Verses verses) {
                                CFTMaterialApplication.bibleVerses.add(verses);
                                return null;
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread())//return the result on
                        .subscribe(new Subscriber<Verses>() {
                            //oncomplete and onError mutex. only 1 gets called
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                handleError();
                            }

                            @Override
                            public void onNext(Verses verses) {

                            }

                        });

            }


        });

        return null;
    }


/*    public Boolean loadVerses() {

        ArrayList<String> verseSublist = BibleVersesManager.getInstance().getListOfStackedVersed().pop();
        Observable.create(new Observable.OnSubscribe<ArrayList<String>>() {
            @Override
            public void call(Subscriber<? super ArrayList<String>> subscriber) {

                final ProcessedVerse processedVerse = verseprocessor.process(subscriber.toString());
                final StringBuilder builder = new StringBuilder();
                builder.append(":");
                builder.append(processedVerse.getBookName());
                builder.append(".");
                builder.append(processedVerse.getBookChaper());
                String start = processedVerse.getStartVerse();
                String end = processedVerse.getEndVerse();
                if (TextUtils.isEmpty(end)) {
                    end = String.valueOf(Integer.parseInt(start));
                }
                versesApiService.getVerses(PreferencesHelper.getUserBibleVersion() + builder.toString(), start, end)
                        .subscribeOn(Schedulers.io())//do the work on this thread
                        .map(new Func1<Verses, Verses>() {
                            @Override
                            public Verses call(Verses verses) {
                                CFTMaterialApplication.bibleVerses.add(verses);
                                return null;
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread())//return the result on
                        .subscribe(new Subscriber<Verses>() {
                            //oncomplete and onError mutex. only 1 gets called
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                handleError();
                            }

                            @Override
                            public void onNext(Verses verses) {

                            }

                        });

            }
        }).flatMap(new Func1<List<String>, Observable<String>>() {
            @Override
            public Observable<String> call(List<String> urls) {
                return Observable.from(urls);
            }
        });


        return true;
    }*/



    private  void handleError(){

        Snackbar.make(findViewById(android.R.id.content), "Unknown Error Try later", Snackbar.LENGTH_LONG)
                .setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setActionTextColor(Color.RED).setDuration(Snackbar.LENGTH_INDEFINITE)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        postFromAnyThreadBus.register(this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        //  loadData();
/*        AndroidPermissions.check(this)
                .permissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION)
                .hasPermissions(new Checker.Action0() {
                    @Override
                    public void call(String[] permissions) {
                        // loadData();
                    }
                })
                .noPermissions(new Checker.Action1() {
                    @Override
                    public void call(String[] permissions) {
                        ActivityCompat.requestPermissions(InitializationActivity.this
                                , new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION}
                                , REQUEST_CODE_ASK_PERMISSIONS);
                    }
                })
                .check();*/

    }




    @Nullable
    public Boolean getYoutubeVideosContent() throws IOException {

        youtubeService.getYoutube("mostPopular", "snippet", "christfaithtabernaclelondon", "AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ")
                .subscribeOn(Schedulers.io())//do the work on this thread
                .observeOn(AndroidSchedulers.mainThread())//return the result on
                .subscribe(new Subscriber<YoutubeVideosContent>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
handleError();
                    }

                    @Override
                    public void onNext(YoutubeVideosContent youtubeVideosContent) {
                        Timber.i("Got youtube: " + youtubeVideosContent.toString());
                        CFTMaterialApplication.youtubeVideosContent= youtubeVideosContent;
                    }
                });

        return false;
    }


    @Nullable
    public Observable<Boolean> getChaptersPerBook() {
        Timber.i("In chapters");
        final Realm jobRealm = Realm.getDefaultInstance();
        RealmQuery<BookTable> query = jobRealm.where(BookTable.class);
        RealmResults<BookTable> result = query.findAll();

        Observable.from(result).map(new Func1<BookTable, ChapterResponse>() {
            @Override
            public ChapterResponse call(final BookTable bookTable) {
                Timber.i("In Call");
                String param = PreferencesHelper.getUserBibleVersion() + ":" + bookTable.getAbbreviation();
                Timber.i("Value of chapters search params: " + param);
                chaptersService.getChapterCount(param)
                        .subscribeOn(Schedulers.io())//do the work on this thread
                        .observeOn(AndroidSchedulers.mainThread())//return the result on
                        .map(new Func1<ChapterResponse, Boolean>() {
                            @Override
                            public Boolean call(ChapterResponse chapterResponse) {
                                bookTable.setNumberOfChapters(chapterResponse.getChapters().size());
                                try {
                                    jobRealm.beginTransaction();
                                    jobRealm.copyToRealmOrUpdate(bookTable);
                                    jobRealm.commitTransaction();
                                    jobRealm.close();

                                } catch (RealmException realmException) {
                                    jobRealm.cancelTransaction();
                                    jobRealm.close();
                                    realmException.printStackTrace();
                                }
                                return true;
                            }

                        });

                return null;
            }

        });


      /*  for (final BookTable bookTable : result) {
            String param = PreferencesHelper.getUserBibleVersion() + ":" + bookTable.getAbbreviation();
            chaptersService.getChapterCount(param)
                    .subscribeOn(Schedulers.io())//do the work on this thread
                    .observeOn(AndroidSchedulers.mainThread())//return the result on
                    .map(new Func1<ChapterResponse, ChapterResponse>() {
                        @Override
                        public ChapterResponse call(ChapterResponse chapterResponse) {
                            bookTable.setNumberOfChapters(chapterResponse.getChapters().size());
                            try {
                                jobRealm.beginTransaction();
                                jobRealm.copyToRealmOrUpdate(bookTable);
                                jobRealm.commitTransaction();
                                jobRealm.close();

                            } catch (RealmException realmException) {
                                jobRealm.cancelTransaction();
                                jobRealm.close();
                                realmException.printStackTrace();
                            }
                            return chapterResponse;
                        }
                    })
                    .subscribe(new Subscriber<ChapterResponse>() {
                        //oncomplete and onError mutex. only 1 gets called
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            handleError();
                        }

                        @Override
                        public void onNext(ChapterResponse chapterResponse) {

                        }

                    });
        }*/
        return null;
    }



    @Nullable
    public Boolean getCalendarEvents() throws IOException {
        ArrayList<CalendarEvent> allBaseCalendarEvents = new ArrayList<>();
        if (!CftDataHolder.getInstance().getCalendarEventArrayList().isEmpty()) {
            return true;
        } else {

            ArrayList<Event> allEvents = new ArrayList<>();
          CalendarHolder calendar = gsonDeserializer.fromJson(Utils.loadAssetTextAsString("calendar_2016_update.json"), CalendarHolder.class);
            Pages pages = calendar.getPages();
            for (int count = 0; count < pages.getPage().size(); count++) {
                allEvents.addAll(pages.getPage().get(count).getEvents());
            }
            for (Event event : allEvents) {
                Calendar startTime = Calendar.getInstance();
                Calendar endTime = Calendar.getInstance();

                DateTime dateTime = DateUtils.parseDate(event.getDate());
                startTime.set(Calendar.DAY_OF_MONTH, dateTime.getDayOfMonth());
                startTime.set(Calendar.MONTH, dateTime.getMonthOfYear() - 1);
                startTime.set(Calendar.HOUR_OF_DAY, 01);
                startTime.set(Calendar.MINUTE, 0);

                endTime.set(Calendar.DAY_OF_MONTH, dateTime.getDayOfMonth());
                endTime.set(Calendar.MONTH, dateTime.getMonthOfYear() - 1);
                endTime.set(Calendar.HOUR_OF_DAY, 23);
                endTime.set(Calendar.MINUTE, 0);
                BaseCalendarEvent baseCalendarEvent = new BaseCalendarEvent(event.getEventTitle(), event.getEventDescription(), event.getEventLocation(),
                        Color.parseColor(event.getColorCode()), startTime, endTime, false);
                allBaseCalendarEvents.add(baseCalendarEvent);
            }

            CftDataHolder.getInstance().setCalendarEventArrayList(allBaseCalendarEvents);
            return true;
        }
    }


    public Observable<Boolean> getYoutubeContentObservable() {
        return Observable.defer(() -> {
            try {
                return Observable.just(getYoutubeVideosContent());
            } catch (IOException e) {
                e.printStackTrace();
                return Observable.error(e);
            }
        });
    }

    public Observable<Boolean> getVersesObservable() {
        return Observable.defer(() -> {
            Timber.i("completed and getVersesObservable returned ");
            return getCustomerOne();
        });
    }

 /*   public Observable<Verses> getVersesObservable() {
        return Observable.defer(new Func0<Observable<Observable<Boolean>>>() {
            @Override
            public Observable<Observable<Boolean>> call() {
                return Observable.just(getVersesbyObservable());
            }
        });
    }*/


    public Observable<Boolean> getCalendarObservable() {
        return Observable.defer(() -> {
            try {
                return Observable.just(getCalendarEvents());
            } catch (IOException e) {
                e.printStackTrace();
                return Observable.error(e);
            }
        });
    }

    public Observable<Boolean> getChaptersObservable() {
        return Observable.defer((Func0<Observable<Boolean>>) () -> {
            try {
                // return Observable.just(getChaptersPerBook());
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return Observable.error(e);
            }
        });
    }

    public Observable<Boolean> getAnnoucementObservable() {

        return Observable.defer(() -> Observable.just(getAnnouncement())).map(aBoolean -> {
            Snackbar.make(findViewById(android.R.id.content), "Announcement complete", Snackbar.LENGTH_LONG)
                    .setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    })
                    .setActionTextColor(Color.RED).setDuration(Snackbar.LENGTH_INDEFINITE)
                    .show();
            return true;
        });
    }




    @Nullable
    public Boolean getAnnouncement() {

        if (CftDataHolder.getInstance().getAnnouncement() != null) {
            return true;
        } else {
            firebaseService.child("announcement").orderByChild(UID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    Log.v("DownloadBulletin", snapshot.toString());
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        if (postSnapshot.getKey().equals(UID)) {
                            CftDataHolder.getInstance().setAnnouncement(postSnapshot.child("Announcement").getValue(Announcement.class));

                        }
                    }

                }

                @Override
                public void onCancelled(FirebaseError error) {
                    Log.v("DownloadBulletin", error.toString());
                }

            });

        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadBibleBooksSubscription != null && !loadBibleBooksSubscription.isUnsubscribed()) {
            loadBibleBooksSubscription.unsubscribe();
        }
        if (loadObservableZipSubscription != null && !loadObservableZipSubscription.isUnsubscribed()) {
            loadObservableZipSubscription.unsubscribe();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, final String[] permissions, int[] grantResults) {
        AndroidPermissions.result()
                .addPermissions(REQUEST_CODE_ASK_PERMISSIONS, Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .putActions(REQUEST_CODE_ASK_PERMISSIONS, () -> loadData(), (hasPermissions, noPermissions) -> finish())
                .result(requestCode, permissions, grantResults);
    }


    private class VersesParam {
        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        private String main;
        private String start;
        private String end;
    }

}