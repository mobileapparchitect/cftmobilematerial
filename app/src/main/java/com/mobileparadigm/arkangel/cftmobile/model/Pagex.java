package com.mobileparadigm.arkangel.cftmobile.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Pagex implements Parcelable{

   private static final String FIELD_PAGE = "page";
    private static final String FIELD_PRAYER_POINT_FOOTER = "prayer_point_footer";
    private static final String FIELD_EVENTS = "events";
    private static final String FIELD_PAGE_MONTH = "page_month";
    private static final String FIELD_YEAR_THEME = "year_theme";
    private static final String FIELD_VERSE_OF_THE_MONTH = "verse_of_the_month";
    private static final String FIELD_THEME_OF_THE_MONTH = "theme_of_the_month";
    private static final String FIELD_MONTH_PRAYER_POINT = "month_prayer_point";

    @JsonProperty(FIELD_PAGE)
    private List<Pagex> mPages;

    @JsonProperty(FIELD_PRAYER_POINT_FOOTER)
    private String mPrayerPointFooter;
    @JsonProperty(FIELD_EVENTS)
    private List<Eventx> mEvents;

    @JsonProperty(FIELD_PAGE_MONTH)
    private String mPageMonth;

    @JsonProperty(FIELD_YEAR_THEME)
    private String mYearTheme;


    @JsonProperty(FIELD_VERSE_OF_THE_MONTH)
    private String mVerseOfTheMonth;

    @JsonProperty(FIELD_THEME_OF_THE_MONTH)
    private String mThemeOfTheMonth;


    @JsonProperty(FIELD_MONTH_PRAYER_POINT)
    private String mMonthPrayerPoint;


    public Pagex(){

    }

    public void setPages(List<Pagex> pages) {
        mPages = pages;
    }

    public List<Pagex> getPages() {
        return mPages;
    }

    public void setPrayerPointFooter(String prayerPointFooter) {
        mPrayerPointFooter = prayerPointFooter;
    }

    public String getPrayerPointFooter() {
        return mPrayerPointFooter;
    }

    public void setEvents(List<Eventx> event) {
        mEvents = event;
    }

    public List<Eventx> getEvents() {
        return mEvents;
    }

    public void setPageMonth(String pageMonth) {
        mPageMonth = pageMonth;
    }

    public String getPageMonth() {
        return mPageMonth;
    }

    public void setYearTheme(String yearTheme) {
        mYearTheme = yearTheme;
    }

    public String getYearTheme() {
        return mYearTheme;
    }

    public void setVerseOfTheMonth(String verseOfTheMonth) {
        mVerseOfTheMonth = verseOfTheMonth;
    }

    public String getVerseOfTheMonth() {
        return mVerseOfTheMonth;
    }

    public void setThemeOfTheMonth(String themeOfTheMonth) {
        mThemeOfTheMonth = themeOfTheMonth;
    }

    public String getThemeOfTheMonth() {
        return mThemeOfTheMonth;
    }

    public void setMonthPrayerPoint(String monthPrayerPoint) {
        mMonthPrayerPoint = monthPrayerPoint;
    }

    public String getMonthPrayerPoint() {
        return mMonthPrayerPoint;
    }

    public Pagex(Parcel in) {
        mPages = new ArrayList<Pagex>();
        in.readTypedList(mPages, Pagex.CREATOR);
        mPrayerPointFooter = in.readString();
        mEvents = in.readParcelable(Eventx.class.getClassLoader());
        in.readTypedList(mEvents, Eventx.CREATOR);
        mPageMonth = in.readString();
        mYearTheme = in.readString();
        mVerseOfTheMonth = in.readString();
        mThemeOfTheMonth = in.readString();
        mMonthPrayerPoint = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Pagex> CREATOR = new Creator<Pagex>() {
        public Pagex createFromParcel(Parcel in) {
            return new Pagex(in);
        }

        public Pagex[] newArray(int size) {
        return new Pagex[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mPages);
        dest.writeString(mPrayerPointFooter);
        dest.writeTypedList(mEvents);
        dest.writeString(mPageMonth);
        dest.writeString(mYearTheme);
        dest.writeString(mVerseOfTheMonth);
        dest.writeString(mThemeOfTheMonth);
        dest.writeString(mMonthPrayerPoint);
    }


}