package com.mobileparadigm.arkangel.cftmobile.model;

import android.os.Parcelable;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Eventx implements Parcelable{

    private static final String FIELD_EVENT_LOCATION = "event_location";
    private static final String FIELD_EVENT_TITLE = "event_title";
    private static final String FIELD_EVENT_DESCRIPTION = "event_description";
    private static final String FIELD_COLOR_CODE = "color_code";
    private static final String FIELD_DATE = "date";


    @JsonProperty(FIELD_EVENT_LOCATION)
    private String mEventLocation;


    @JsonProperty(FIELD_EVENT_TITLE)
    private String mEventTitle;


    @JsonProperty(FIELD_EVENT_DESCRIPTION)
    private String mEventDescription;


    @JsonProperty(FIELD_COLOR_CODE)
    private String mColorCode;


    @JsonProperty(FIELD_DATE)
    private String mDate;


    public Eventx(){

    }

    public void setEventLocation(String eventLocation) {
        mEventLocation = eventLocation;
    }

    public String getEventLocation() {
        return mEventLocation;
    }

    public void setEventTitle(String eventTitle) {
        mEventTitle = eventTitle;
    }

    public String getEventTitle() {
        return mEventTitle;
    }

    public void setEventDescription(String eventDescription) {
        mEventDescription = eventDescription;
    }

    public String getEventDescription() {
        return mEventDescription;
    }

    public void setColorCode(String colorCode) {
        mColorCode = colorCode;
    }

    public String getColorCode() {
        return mColorCode;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDate() {
        return mDate;
    }

    public Eventx(Parcel in) {
        mEventLocation = in.readString();
        mEventTitle = in.readString();
        mEventDescription = in.readString();
        mColorCode = in.readString();
        mDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Eventx> CREATOR = new Creator<Eventx>() {
        public Eventx createFromParcel(Parcel in) {
            return new Eventx(in);
        }

        public Eventx[] newArray(int size) {
            return new Eventx[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mEventLocation);
        dest.writeString(mEventTitle);
        dest.writeString(mEventDescription);
        dest.writeString(mColorCode);
        dest.writeString(mDate);
    }


}