package com.mobileparadigm.arkangel.cftmobile.data_models;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Parent extends BaseObservable implements Parcelable{

    private static final String FIELD_CHAPTER = "chapter";

    @SerializedName(FIELD_CHAPTER)
    private Chapter mChapter;


    public Parent(){

    }

    public void setChapter(Chapter chapter) {
        mChapter = chapter;
    }

    public Chapter getChapter() {
        return mChapter;
    }

    public Parent(Parcel in) {
        mChapter = in.readParcelable(Chapter.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Parent> CREATOR = new Creator<Parent>() {
        public Parent createFromParcel(Parcel in) {
            return new Parent(in);
        }

        public Parent[] newArray(int size) {
        return new Parent[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mChapter, flags);
    }


}