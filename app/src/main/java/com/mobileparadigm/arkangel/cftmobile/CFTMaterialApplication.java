package com.mobileparadigm.arkangel.cftmobile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verse;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verses;
import com.mobileparadigm.arkangel.cftmobile.data_models.firebase.Announcement;
import com.mobileparadigm.arkangel.cftmobile.data_models.youtube.YoutubeVideosContent;
import com.mobileparadigm.arkangel.cftmobile.eventbus.BusManager;
import com.mobileparadigm.arkangel.cftmobile.modules.ApplicationComponent;
import com.mobileparadigm.arkangel.cftmobile.modules.ApplicationModule;
import com.mobileparadigm.arkangel.cftmobile.modules.DaggerApplicationComponent;
import com.mobileparadigm.arkangel.cftmobile.modules.splash.DaggerSplashComponent;
import com.mobileparadigm.arkangel.cftmobile.modules.splash.SplashComponent;
import com.mobileparadigm.arkangel.cftmobile.modules.splash.SplashModule;
import com.mobileparadigm.arkangel.cftmobile.receivers.NetworkConnectivityReceiver;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;


public class CFTMaterialApplication extends MultiDexApplication {
    public static ArrayList<Verses> bibleVerses;
    public static YoutubeVideosContent youtubeVideosContent;
    public static ArrayList<Verse> glideDrawableArrayList;


    static Context context;


    private static ApplicationComponent component;
    private SplashComponent splashComponent;


    public static CFTMaterialApplication getInstance() {
        return instance;
    }

    private static CFTMaterialApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        //dependency injection
        this.setAppContext(getApplicationContext());
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        Fabric.with(this, new Crashlytics());
        Timber.plant(new Timber.DebugTree());

        glideDrawableArrayList = new ArrayList<>();
        instance=this;
        bibleVerses = new ArrayList<>();

        PreferencesHelper.setUserBibleVersion(Constants.BIBLE_VERSION_KJVA);
        observeNetworkDisconnectionsToStartBroadcastReceiver();
    }


    private void observeNetworkDisconnectionsToStartBroadcastReceiver() {
        BusManager.getInstance().getNetworkConnectivityChangeEventBus().subscribe(networkStatus -> {
            if (networkStatus == NetworkConnectivityReceiver.NetworkStatus.DISCONNECTED) {
                new NetworkConnectivityReceiver.Helper(getApplicationContext()).startReceiver();
            }
        });
    }




    public void setAppContext(Context mAppContext) {
        CFTMaterialApplication.context = (CFTMaterialApplication) mAppContext;
    }

    @NonNull
    public SplashComponent splashComponent() {
        if (splashComponent == null) {
            splashComponent = DaggerSplashComponent.builder()
                    .applicationComponent(getAppComponent())
                    .splashModule(new SplashModule())
                    .build();
        }
        return splashComponent;
    }


    public static ApplicationComponent getAppComponent(){
        return component;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
