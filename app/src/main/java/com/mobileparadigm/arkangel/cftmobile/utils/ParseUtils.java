/*
package com.mobileparadigm.arkangel.cftmobile.utils;

import android.text.Html;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mobileparadigm.arkangel.cftmobile.Constants;
import com.mobileparadigm.arkangel.cftmobile.model.ProcessedVerse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import mobileparadigm.arkangelx.com.back.builders.VersesGetRequestBuilder;
import mobileparadigm.arkangelx.com.back.model.Verse;
import mobileparadigm.arkangelx.com.back.model.booksresponse.CftAnnouncement;
import mobileparadigm.arkangelx.com.back.model.booksresponse.CftBibleVerse;
import mobileparadigm.arkangelx.com.back.model.booksresponse.CftBulletin;
import mobileparadigm.arkangelx.com.back.model.booksresponse.CftEvent;
import mobileparadigm.arkangelx.com.back.model.booksresponse.CftPrayerPoint;

*/
/**
 * Created by arkangel on 01/05/15.
 *//*

public class ParseUtils {
    private static String uniqueId;
    private static ParseObject parseObjects;
    private static long count = 0;
    private static ParseObject cftAnnouncementParseobject;

    public static void removedFromLocalDataStore(String tag) {
        try {
            ParseObject.unpinAll(tag);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public static void processJsonFromAnnouncementJson(final JSONObject jsonObject, final String tag) {

        try {
            uniqueId = jsonObject.getString("uniqueId");
            PreferencesHelper.setCurrentAnnouncmentId(uniqueId);
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Announcement");
            query.whereEqualTo("objectId", PreferencesHelper.getCurrentAnnouncementId());

            try {
                List<ParseObject> announcementItems = query.find();
                if (announcementItems != null && !announcementItems.isEmpty()) {
                    cftAnnouncementParseobject = announcementItems.get(0);
                    CftMobileDispatchManager.getInstance(cftAnnouncementParseobject);

                    for (final ParseObject verseParseObject : CftMobileDispatchManager.allBibleStudies) {
                        String[] verses = verseParseObject.fetchIfNeeded().getString(Constants.KEY_BIBLE_STUDY_LABEL).split("<br />");
                        for (String verse : verses) {
                            int firstOccurrenceOfOfColon = verse.indexOf(":");
                            final String timeOfDay = verse.substring(0, firstOccurrenceOfOfColon);
                            final String versePart = verse.substring(firstOccurrenceOfOfColon + 1, verse.length());
                            ProcessedVerse processedVerse = VerseProcessor.getInstance().process(versePart.trim());
                            //HERE
                            VersesGetRequestBuilder.newBuilder(PreferencesHelper.getUserBibleVersion(), processedVerse.getBookNumber(), processedVerse.getBookName(), processedVerse.getBookChaper(), processedVerse.getStartVerse(), processedVerse.getEndVerse()).setErrorListener(new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    Log.e(ParseUtils.class.getSimpleName(), volleyError.toString());

                                }
                            }).setResponseListener(new Response.Listener<mobileparadigm.arkangelx.com.back.model.Verses>() {

                                @Override
                                public void onResponse(mobileparadigm.arkangelx.com.back.model.Verses verses) {
                                    StringBuilder builder = new StringBuilder();
                                    for (Verse verse : verses.getResponse().getVerses()) {
                                        builder.append(Html.fromHtml(verse.getText().toString()).toString());
                                        builder.append(" ");
                                    }
                                    Realm.getDefaultInstance().beginTransaction();
                                    CftBibleVerse studyBibleVerse = new CftBibleVerse();
                                    studyBibleVerse.setCftBibleVerseId(new Random().nextLong());
                                    studyBibleVerse.setVerse_title(versePart);
                                    studyBibleVerse.setVerse_detail(builder.toString());
                                    studyBibleVerse.setVerse_detail_parent(PreferencesHelper.getCurrentAnnouncementId());
                                    if (timeOfDay.startsWith("A")) {
                                        studyBibleVerse.setMorningOrEvening(true);
                                    } else {
                                        studyBibleVerse.setMorningOrEvening(false);
                                    }
                                    Realm.getDefaultInstance().copyToRealmOrUpdate(studyBibleVerse);
                                    Realm.getDefaultInstance().commitTransaction();
                                    Realm.getDefaultInstance().close();

                                }
                            }, mobileparadigm.arkangelx.com.back.model.Verses.class).setTag(versePart + "Verses").build().execute();
                        }
                    }

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }





            for (ParseObject parseObject : CftMobileDispatchManager.allEventsList) {
                Realm.getDefaultInstance().beginTransaction();
                CftEvent cftEvent = new CftEvent();
                cftEvent.setEvent_detail(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_DETAIL));
                cftEvent.setEvent_detail_parent(PreferencesHelper.getCurrentAnnouncementId());
                cftEvent.setEvent_time(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_TIME));
                cftEvent.setEvent_title(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_TITLE));
                cftEvent.setCftEventId(new Random().nextLong()+cftEvent.hashCode());
                cftEvent.setEndDate(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_END_DATE));
                cftEvent.setStartDate(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_START_DATE));
                Realm.getDefaultInstance().copyToRealmOrUpdate(cftEvent);
                Realm.getDefaultInstance().commitTransaction();

            }


            for (ParseObject parseObject : CftMobileDispatchManager.allPrayerPointList) {
                Realm.getDefaultInstance().beginTransaction();
                CftPrayerPoint cftPrayerPoint = new CftPrayerPoint();
                cftPrayerPoint.setPrayer_detail(parseObject.fetchIfNeeded().getString(Constants.KEY_PRAYER_POINT));
                cftPrayerPoint.setCftPrayerPointId(new Random().nextLong());
                cftPrayerPoint.setPrayer_parent_id(PreferencesHelper.getCurrentAnnouncementId());
                Realm.getDefaultInstance().copyToRealmOrUpdate(cftPrayerPoint);
                Realm.getDefaultInstance().commitTransaction();
            }


            for (ParseObject parseObject : CftMobileDispatchManager.allBulletins) {
                Realm.getDefaultInstance().beginTransaction();
                CftBulletin bulletin = new CftBulletin();
                bulletin.setBulletin_detail(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_DETAIL));
                bulletin.setBulletin_title(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_TITLE));
                bulletin.setBulletin_theme(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_THEME));
                bulletin.setBulletin_parent_id(PreferencesHelper.getCurrentAnnouncementId());
                Realm.getDefaultInstance().copyToRealmOrUpdate(bulletin);
                Realm.getDefaultInstance().commitTransaction();
            }


            Realm.getDefaultInstance().beginTransaction();
            CftAnnouncement cftAnnouncement = new CftAnnouncement();
            cftAnnouncement.setNewsFlashDetail(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_FLASH_DETAIL));
            cftAnnouncement.setNewsFlashTitle(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_FLASH_TITLE));
            cftAnnouncement.setYearThemeTitle(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_YEAR_THEME_TITLE));
            cftAnnouncement.setDate(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_DATE));
            cftAnnouncement.setAnnouncementId(new Random().nextLong());
            cftAnnouncement.setParentId(PreferencesHelper.getCurrentAnnouncementId());
            Realm.getDefaultInstance().copyToRealmOrUpdate(cftAnnouncement);
            Realm.getDefaultInstance().commitTransaction();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Realm.getDefaultInstance().close();
        Log.i("Time End Log", String.valueOf(System.currentTimeMillis()));
        PreferencesHelper.setBulletinDownloaded(true);
    }



    public static void processJsonFromAnnouncementJson(String uniqueId, final String tag) {

        try {

            PreferencesHelper.setCurrentAnnouncmentId(uniqueId);
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Announcement");
            query.whereEqualTo("objectId", PreferencesHelper.getCurrentAnnouncementId());

            try {
                List<ParseObject> announcementItems = query.find();
                if (announcementItems != null && !announcementItems.isEmpty()) {
                    cftAnnouncementParseobject = announcementItems.get(0);
                    CftMobileDispatchManager.getInstance(cftAnnouncementParseobject);

                    for (final ParseObject verseParseObject : CftMobileDispatchManager.allBibleStudies) {
                        String[] verses = verseParseObject.fetchIfNeeded().getString(Constants.KEY_BIBLE_STUDY_LABEL).split("<br />");
                        for (String verse : verses) {
                            int firstOccurrenceOfOfColon = verse.indexOf(":");
                            final String timeOfDay = verse.substring(0, firstOccurrenceOfOfColon);
                            final String versePart = verse.substring(firstOccurrenceOfOfColon + 1, verse.length());
                            ProcessedVerse processedVerse = VerseProcessor.getInstance().process(versePart.trim());
                            //HERE
                            VersesGetRequestBuilder.newBuilder(PreferencesHelper.getUserBibleVersion(), processedVerse.getBookNumber(), processedVerse.getBookName(), processedVerse.getBookChaper(), processedVerse.getStartVerse(), processedVerse.getEndVerse()).setErrorListener(new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    Log.e(ParseUtils.class.getSimpleName(), volleyError.toString());

                                }
                            }).setResponseListener(new Response.Listener<mobileparadigm.arkangelx.com.back.model.Verses>() {

                                @Override
                                public void onResponse(mobileparadigm.arkangelx.com.back.model.Verses verses) {
                                    StringBuilder builder = new StringBuilder();
                                    for (Verse verse : verses.getResponse().getVerses()) {
                                        builder.append(Html.fromHtml(verse.getText().toString()).toString());
                                        builder.append(" ");
                                    }
                                    Realm.getDefaultInstance().beginTransaction();
                                    CftBibleVerse studyBibleVerse = new CftBibleVerse();
                                    studyBibleVerse.setCftBibleVerseId(new Random().nextLong());
                                    studyBibleVerse.setVerse_title(versePart);
                                    studyBibleVerse.setVerse_detail(builder.toString());
                                    studyBibleVerse.setVerse_detail_parent(PreferencesHelper.getCurrentAnnouncementId());
                                    if (timeOfDay.startsWith("A")) {
                                        studyBibleVerse.setMorningOrEvening(true);
                                    } else {
                                        studyBibleVerse.setMorningOrEvening(false);
                                    }
                                    Realm.getDefaultInstance().copyToRealmOrUpdate(studyBibleVerse);
                                    Realm.getDefaultInstance().commitTransaction();
                                    Realm.getDefaultInstance().close();

                                }
                            }, mobileparadigm.arkangelx.com.back.model.Verses.class).setTag(versePart + "Verses").build().execute();
                        }
                    }

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            for (ParseObject parseObject : CftMobileDispatchManager.allBulletins) {
                Realm.getDefaultInstance().beginTransaction();
                CftBulletin bulletin = new CftBulletin();
                bulletin.setBulletin_detail(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_DETAIL));
                bulletin.setBulletin_title(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_TITLE));
                bulletin.setBulletin_theme(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_THEME));
                bulletin.setBulletin_parent_id(PreferencesHelper.getCurrentAnnouncementId());
                Realm.getDefaultInstance().copyToRealmOrUpdate(bulletin);
                Realm.getDefaultInstance().commitTransaction();
            }


            for (ParseObject parseObject : CftMobileDispatchManager.allEventsList) {
                Realm.getDefaultInstance().beginTransaction();
                CftEvent cftEvent = new CftEvent();
                cftEvent.setEvent_detail(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_DETAIL));
                cftEvent.setEvent_detail_parent(PreferencesHelper.getCurrentAnnouncementId());
                cftEvent.setEvent_time(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_TIME));
                cftEvent.setEvent_title(parseObject.fetchIfNeeded().getString(Constants.KEY_BULLETIN_TITLE));
                cftEvent.setCftEventId(new Random().nextLong());
                cftEvent.setEndDate(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_END_DATE));
                cftEvent.setStartDate(parseObject.fetchIfNeeded().getString(Constants.KEY_EVENT_START_DATE));
                Realm.getDefaultInstance().copyToRealmOrUpdate(cftEvent);
                Realm.getDefaultInstance().commitTransaction();

            }


            for (ParseObject parseObject : CftMobileDispatchManager.allPrayerPointList) {
                Realm.getDefaultInstance().beginTransaction();
                CftPrayerPoint cftPrayerPoint = new CftPrayerPoint();
                cftPrayerPoint.setPrayer_detail(parseObject.fetchIfNeeded().getString(Constants.KEY_PRAYER_POINT));
                cftPrayerPoint.setCftPrayerPointId(new Random().nextLong());
                cftPrayerPoint.setPrayer_parent_id(PreferencesHelper.getCurrentAnnouncementId());
                Realm.getDefaultInstance().copyToRealmOrUpdate(cftPrayerPoint);
                Realm.getDefaultInstance().commitTransaction();
            }

            Realm.getDefaultInstance().beginTransaction();
            CftAnnouncement cftAnnouncement = new CftAnnouncement();
            cftAnnouncement.setNewsFlashDetail(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_FLASH_DETAIL));
            cftAnnouncement.setNewsFlashTitle(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_FLASH_TITLE));
            cftAnnouncement.setYearThemeTitle(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_YEAR_THEME_TITLE));
            cftAnnouncement.setDate(cftAnnouncementParseobject.fetchIfNeeded().getString(Constants.KEY_ANNOUNCEMENT_NEWS_DATE));
            cftAnnouncement.setAnnouncementId(new Random().nextLong());
            cftAnnouncement.setParentId(PreferencesHelper.getCurrentAnnouncementId());
            Realm.getDefaultInstance().copyToRealmOrUpdate(cftAnnouncement);
            Realm.getDefaultInstance().commitTransaction();


        } catch (ParseException e) {
            e.printStackTrace();
        }
        Realm.getDefaultInstance().close();
        Log.i("Time End Log", String.valueOf(System.currentTimeMillis()));
        PreferencesHelper.setBulletinDownloaded(true);
    }


    public static void clearOldRecords() {
        RealmResults<CftPrayerPoint> resultsPrayerpoints = Realm.getDefaultInstance().where(CftPrayerPoint.class).findAll();
        Realm.getDefaultInstance().beginTransaction();
        resultsPrayerpoints.clear();
        Realm.getDefaultInstance().commitTransaction();

        RealmResults<CftEvent> resultsCftEvent = Realm.getDefaultInstance().where(CftEvent.class).findAll();
        Realm.getDefaultInstance().beginTransaction();
        resultsCftEvent.clear();
        Realm.getDefaultInstance().commitTransaction();


        RealmResults<CftBulletin> resultsCftBulletin = Realm.getDefaultInstance().where(CftBulletin.class).findAll();
        Realm.getDefaultInstance().beginTransaction();
        resultsCftBulletin.clear();
        Realm.getDefaultInstance().commitTransaction();


        RealmResults<CftAnnouncement> resultAnnouncement = Realm.getDefaultInstance().where(CftAnnouncement.class).findAll();
        Realm.getDefaultInstance().beginTransaction();
        resultAnnouncement.clear();
        Realm.getDefaultInstance().commitTransaction();

        RealmResults<CftBibleVerse> resultsBibleVerse = Realm.getDefaultInstance().where(CftBibleVerse.class).findAll();
        Realm.getDefaultInstance().beginTransaction();
        resultsBibleVerse.clear();
        Realm.getDefaultInstance().commitTransaction();
        Realm.getDefaultInstance().close();
    }

}
*/
