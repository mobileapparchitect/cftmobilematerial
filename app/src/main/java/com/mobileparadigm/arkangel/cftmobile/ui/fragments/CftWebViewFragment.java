package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mobileparadigm.arkangel.cftmobile.R;

public class CftWebViewFragment extends Fragment {

    private Bitmap iconBitmap;
    private View mRootView;
    private String destinationUrl;
    private static final String WEB_PAGE_REQUESTED = "web_page_requested";
    private static WebView webView;
    private ProgressBar progressBar;
    private static final String TAG = "BetFuzeWebViewFragment";
    private String actionBarColor;
    private String iconUrl;
    private boolean flag_override_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            destinationUrl = bundle.getString(WEB_PAGE_REQUESTED);
             destinationUrl = destinationUrl.replace("&amp;", "&");
        }

        mRootView = inflater.inflate(
                R.layout.screen_webview_layout,
                container, false);
        webView = (WebView) mRootView.findViewById(R.id.webview);
        progressBar = (ProgressBar) mRootView.findViewById(R.id.progress);

        webView.setWebViewClient(new WebClient());
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDatabaseEnabled(true);


        if (!TextUtils.isEmpty(destinationUrl)) {
            webView.loadUrl(destinationUrl);
        }
        return mRootView;
    }

    public static CftWebViewFragment getInstance(String url) {
        Bundle bundle = new Bundle();
        bundle.putString(WEB_PAGE_REQUESTED, url);
        CftWebViewFragment fragment = new CftWebViewFragment();
        fragment.setArguments(bundle);
        return fragment;

    }






    private class WebClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Error")
                    .setMessage(description)
                    .setCancelable(false)
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {


            if (url.startsWith("mailto:")) {
                MailTo mt = MailTo.parse(url);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{mt.getTo()});
                i.putExtra(Intent.EXTRA_SUBJECT, mt.getSubject());
                i.putExtra(Intent.EXTRA_CC, mt.getCc());
                i.putExtra(Intent.EXTRA_TEXT, mt.getBody());
                getActivity().startActivity(i);
                view.reload();
                return true;
            }
            view.loadUrl(url);
            return true;
        }

    }





    @Override
    public void onDestroy() {
        super.onDestroy();


    }



    public static boolean navigateBack() {
        if (webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return false;
    }
}