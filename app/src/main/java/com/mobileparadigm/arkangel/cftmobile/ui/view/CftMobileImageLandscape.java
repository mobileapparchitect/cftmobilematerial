package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mobileparadigm.arkangel.cftmobile.R;

import java.util.Random;

/**
 * Created by arkangel on 16/08/15.
 */
public class CftMobileImageLandscape extends ImageView {


    private String[] urlsOfImagesLandscape = getResources().getStringArray(R.array.cft_pictures_landscape);
    private Handler urlsHandler;
    private  Context mContext;
    private    int index;
    public CftMobileImageLandscape(Context context) {
        super(context);
        urlsHandler = new Handler();
        mContext = context;
        init();
    }

    public CftMobileImageLandscape(Context context, AttributeSet attrs) {
        super(context, attrs);
        urlsHandler = new Handler();
        mContext = context;
        init();
    }

    public CftMobileImageLandscape(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        urlsHandler = new Handler();
        mContext = context;
        init();
    }

 public void init(){
 index  = new Random().nextInt() %urlsOfImagesLandscape.length;
     if(index<0)
         index*=-1;
     Glide.with(mContext).load(urlsOfImagesLandscape[index]).placeholder(R.drawable.header).into(CftMobileImageLandscape.this); ;

 }
}
