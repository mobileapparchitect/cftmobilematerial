package com.mobileparadigm.arkangel.cftmobile.ridesharing.match;


import com.google.android.gms.maps.model.LatLng;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Constants;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Driver;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Journey;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.JourneyData;
import com.mobileparadigm.arkangel.cftmobile.ridesharing.utils.Passenger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Does all the heavy-lifting
 * @author Debosmit
 *
 */
public class MatchMaker {

	ArrayList<Passenger> PassengerList = new ArrayList<Passenger>();
	ArrayList<Driver> DriverList = new ArrayList<Driver>();
	
	// offset for converting passengers to character
	//	  to find best possible combination for journey
	// first 32 chars are for controlling peripherals
	private final int buffer = 65;		
	
	private final boolean DEBUG = Constants.isDebugMode();
	
	public void setPassengers(Passenger... passengers) {
		for(Passenger passenger: passengers)
			PassengerList.add(passenger);
	}
	
	public void setDrivers(Driver... drivers) {
		for(Driver driver: drivers)
			DriverList.add(driver);
	}
	
	public void process() {
		// get all orderings of passengers
		// say list is -> p1, p2, p3, ... pn
		// get permutations of AABBCC... without repeats
		// first instance = pick up
		// second instance = drop off
		// for passenger p, pick up and drop off cannot be back-to-back
		// char in Java is 16 bits
		// so, 2^16 passengers can be supported concurrently in a region
		// not like that is ever going to happen since a car can hold 7
		// people at the most
		
		// assign character for every passenger
		// passenger at index 0 gets (char)0, index 1 gets (char)1, ...
		StringBuilder string = new StringBuilder();
		
		for(int i = buffer ; i < buffer + PassengerList.size() ; i++) {
			string.append((char)i);
			string.append((char)i);
		}
		
		// all possible orderings for passengers
		HashSet<String> orderings = new HashSet<String>();
		
		// get permutations
		permute("", string.toString(), orderings);
		
		if(DEBUG) {
			System.out.print("DEBUG All permutations:               ");
			String[] t = new String[orderings.size()];
			orderings.toArray(t);
			System.out.println(Arrays.toString(t));
		}
		
		// remove cases of no overlap [pick up, drop off, pick up, ...]
		// first [pick, drop] is useless
		removeNoOverlaps(orderings);
		
		if(DEBUG) {
			System.out.print("DEBUG All permutations - no overlaps: ");
			String[] t = new String[orderings.size()];
			orderings.toArray(t);
			System.out.println(Arrays.toString(t));
		}
		
		// As soon as passenger calls for driver, 
		// time starts. We will call this the start of
		// journey. The journey ends when the passenger
		// is dropped off.
		// For each of the orderings, we will start with the
		// driver's current location
		
		// google directions api
		// list of journeys
		ArrayList<Journey> journeys = new ArrayList<Journey>();
		computeJourneyDetailsGoogleDirectionsAPI(journeys, orderings);
		
		// sort Journey to get the best route
		Collections.sort(journeys);
		
		System.out.println("Google Directions API\n" + journeys.get(0));
		
		// haversine
		ArrayList<Journey> journeysHaversine = new ArrayList<Journey>();
		computeJourneyDetailsHaversine(journeysHaversine, orderings);
		
		// sort Journey to get the best route
		Collections.sort(journeysHaversine);
		
		System.out.println("Haversines\n" + journeysHaversine.get(0));
		
	}
	
	private void computeJourneyDetailsHaversine(ArrayList<Journey> journeys, HashSet<String> orderings) {
		// get best path for each of the available drivers
		for(Driver driver: DriverList) {
			
			// corresponding passenger orderings
			for(String ordering: orderings) {
				// start location = driver present location
				
				// end location = last passenger drop off
				char lastPassenger = ordering.charAt(ordering.length() - 1);
				// get passenger drop off from PassengerList
				// lastPassenger - buffer = location of passenger in list
				LatLng last = PassengerList.get(lastPassenger - buffer).getDestination();
				
				// waypoints are all chars from ordering.charAt(0...n-2)
				// first occurrence of character = pick up
				// second occurrence of character = drop off
				// on adding to set, if add returns false, it must mean 
				// first occurrence has been accounted for.
				Set<LatLng> waypoints = new LinkedHashSet<LatLng>();
				
				// using TreeSet since ordering is key
				for(int i = 0;  i < ordering.length() - 1 ; i++) {
					char curPassenger = ordering.charAt(i);
					curPassenger -= buffer;
					boolean addOrigin = waypoints.add(PassengerList.get(curPassenger).getOrigin());
					// if addOrigin is false, it was already added to the set
					if(!addOrigin)
						waypoints.add(PassengerList.get(curPassenger).getDestination());
				}
				
				// get Journey for this ordering
				JourneyData thisJourney = new JourneyData();
				thisJourney.setDriver(driver);
				thisJourney.setPassengerList(PassengerList);
				thisJourney.setOrdering(ordering);
				thisJourney.setWaypoints(waypoints);
				thisJourney.setStartLocation(driver.getCurrentLocation());
				thisJourney.setEndLocation(last);
				thisJourney.setOrderingCharacterBuffer(buffer);
				Journey j = new Journey(thisJourney);
				
				// get haversine distance
				j.computeHaversines();
				
				// add updated journey to the list
				journeys.add(j);
			}
		}
	}
	
	// takes an empty ArrayList and a set of possible ordering
	// populates the array with JourneyData objects that have 
	// total journey time and distances
	private void computeJourneyDetailsGoogleDirectionsAPI(ArrayList<Journey> journeys, HashSet<String> orderings)  {
		// get best path for each of the available drivers
		for(Driver driver: DriverList) {
			
			// corresponding passenger orderings
			for(String ordering: orderings) {
				// start location = driver present location
				
				// end location = last passenger drop off
				char lastPassenger = ordering.charAt(ordering.length() - 1);
				// get passenger drop off from PassengerList
				// lastPassenger - buffer = location of passenger in list
				LatLng last = PassengerList.get(lastPassenger - buffer).getDestination();
				
				// waypoints are all chars from ordering.charAt(0...n-2)
				// first occurrence of character = pick up
				// second occurrence of character = drop off
				// on adding to set, if add returns false, it must mean 
				// first occurrence has been accounted for.
				Set<LatLng> waypoints = new LinkedHashSet<LatLng>();
				// using TreeSet since ordering is key
				for(int i = 0;  i < ordering.length() - 1 ; i++) {
					char curPassenger = ordering.charAt(i);
					curPassenger -= buffer;
					boolean addOrigin = waypoints.add(PassengerList.get(curPassenger).getOrigin());
					// if addOrigin is false, it was already added to the set
					if(!addOrigin)
						waypoints.add(PassengerList.get(curPassenger).getDestination());
				}
				
				// get Journey for this ordering
				JourneyData thisJourney = new JourneyData();
				thisJourney.setDriver(driver);
				thisJourney.setPassengerList(PassengerList);
				thisJourney.setOrdering(ordering);
				thisJourney.setWaypoints(waypoints);
				thisJourney.setStartLocation(driver.getCurrentLocation());
				thisJourney.setEndLocation(last);
				thisJourney.setOrderingCharacterBuffer(buffer);
				Journey j = new Journey(thisJourney);
				
				// talk to the Google Directions API to get trip information
				j.obtainData();
				
				// add updated journey to the list
				journeys.add(j);
			}
		}
	}

	private void removeNoOverlaps(HashSet<String> orderings) {
		for (Iterator<String> i = orderings.iterator(); i.hasNext();) {
		    String ordering = i.next();
		    if (ordering.charAt(0) == ordering.charAt(1)) 
		        i.remove();
		}
	}

	private void permute(String prefix, String string,
			HashSet<String> orderings) {
		int n = string.length();
		if (n == 0) {
            orderings.add(prefix);
            return;
        }
		
		// permute characters after start
		for(int i = 0 ; i < n ; i++) {
			permute(prefix + string.charAt(i), string.substring(0,i) + string.substring(i+1), orderings);
		}
	}
}
