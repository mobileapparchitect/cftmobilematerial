package com.mobileparadigm.arkangel.cftmobile.adapters;

/**
 * Created by arkangel on 07/01/2016.
 */
public class SubMenuModel {
 public static enum MENU_ID {HOME,PRAYER_REQUESTS,BIBLE_STUDY,CREDITS,ASSISTANCE,DISCIPLESHIP,FACEBOOK,TWITTER,CFT_WEBSITE,YOUTUBE,APOSTLE_WEBSITE,SETTINGS}
    private MENU_ID menu_id;
    String title;


    public SubMenuModel(String title,MENU_ID menuId,int drawableResource){
        setMenu_id(menuId);
        setResourceId(drawableResource);
        setTitle(title);
    }
    public MENU_ID getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(MENU_ID menu_id) {
        this.menu_id = menu_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    int resourceId;
}
