
package com.mobileparadigm.arkangel.cftmobile.data_models.firebase;


import com.fasterxml.jackson.annotation.JsonProperty;

public class WeeklyBulletin {

    @JsonProperty("detail")
    private String detail;
    @JsonProperty("email")
    private String email;
    @JsonProperty("theme")
    private String theme;
    @JsonProperty("title")
    private String title;

    /**
     * 
     * @return
     *     The detail
     */
    @JsonProperty("detail")
    public String getDetail() {
        return detail;
    }

    /**
     * 
     * @param detail
     *     The detail
     */
    @JsonProperty("detail")
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The theme
     */
    @JsonProperty("theme")
    public String getTheme() {
        return theme;
    }

    /**
     * 
     * @param theme
     *     The theme
     */
    @JsonProperty("theme")
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

}
