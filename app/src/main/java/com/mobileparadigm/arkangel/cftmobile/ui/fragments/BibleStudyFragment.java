package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.CftAnnouncement;
import com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse.CftBibleVerse;
import com.mobileparadigm.arkangel.cftmobile.ui.adapter.ItemData;
import com.mobileparadigm.arkangel.cftmobile.ui.view.ExpandCollapseAnimation;
import com.mobileparadigm.arkangel.cftmobile.utils.DatabaseUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.DateUtils;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;

import org.joda.time.LocalDate;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by arkangel on 27/12/2015.
 */
public class BibleStudyFragment extends Fragment {


    public static SlidingUpPanelLayout slidingPaneLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.verse_text_detail)
    TextView bibleVerseDetail;
    @BindView(R.id.month_theme)
    TextView monthThemeText;
    @BindView(R.id.year_theme)
    TextView yearThemeText;
    @BindView(R.id.dateTextView)
    TextView dateTextView;

    private View view;
    private CftAnnouncement currentCftAnnouncement;

    public static BibleStudyFragment getInstance() {
        return new BibleStudyFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bible_study_fragment, container, false);
        ButterKnife.bind(this, view);
        slidingPaneLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        currentCftAnnouncement = DatabaseUtils.getCurrentAnnouncement().get(0);

        // this is data fro recycler view
        ItemData itemsData[] = {
                new ItemData("Monday", R.drawable.star_inactive),
                new ItemData("Tuesday", R.drawable.star_inactive),
                new ItemData("Wednesday", R.drawable.star_inactive),
                new ItemData("Thursday", R.drawable.star_inactive),
                new ItemData("Friday", R.drawable.star_inactive),
                new ItemData("Saturday", R.drawable.star_inactive)
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        MyAdapter mAdapter = new MyAdapter(itemsData, getActivity());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        slidingPaneLayout.setPanelSlideListener(new PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelCollapsed(View panel) {

            }

            @Override
            public void onPanelExpanded(View panel) {

            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {

            }

        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
    }


    public void loadData(boolean morningOrEvening, int index) {
        RealmQuery<CftBibleVerse> query = Realm.getDefaultInstance().where(CftBibleVerse.class);
        query.equalTo("verse_detail_parent", PreferencesHelper.getCurrentAnnouncementId());
        query.equalTo("morningOrEvening", morningOrEvening);
        RealmResults<CftBibleVerse> objects = query.findAll();
        if (index < objects.size()) {
            CftBibleVerse result = objects.get(index);
            if (result != null) {
                bibleVerseDetail.setText(Html.fromHtml(result.getVerse_detail()));
                yearThemeText.setText(currentCftAnnouncement.getYearThemeTitle());
                // monthThemeText.setText(result.get.get);
                dateTextView.setText(DateUtils.getFullDateInBannerFormat(new LocalDate()));
                slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
            }

        }


    }


    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ItemData[] itemsData;
        private Context mContext;
        private LinearLayout rightButton;
        private LinearLayout leftButton;
        private int count;

        public MyAdapter(ItemData[] itemsData, Context context) {
            this.itemsData = itemsData;
            mContext = context;
            count = -1;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            // create a new view
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_layout, null);
            count++;
            ViewHolder viewHolder = new ViewHolder(itemLayoutView);

            return viewHolder;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
            //       viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());


        }

        // Return the size of your itemsData (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return itemsData.length;
        }

        // inner class to hold a reference to each item of RecyclerView
        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtViewTitle;
            public LinearLayout dropDown;

            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);

                rightButton = (LinearLayout) itemLayoutView.findViewById(R.id.right_button);
                DataHolder holderRight = new DataHolder();
                holderRight.setFlag(false);
                holderRight.setPosition(count);
                rightButton.setTag(holderRight);

                leftButton = (LinearLayout) itemLayoutView.findViewById(R.id.left_button);
                DataHolder holderLeft = new DataHolder();
                holderLeft.setFlag(true);
                holderLeft.setPosition(count);
                leftButton.setTag(holderLeft);

                dropDown = (LinearLayout) itemLayoutView.findViewById(R.id.match_action);
                dropDown.setVisibility(View.GONE);

                rightButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataHolder tag = (DataHolder) view.getTag();
                        loadData(tag.flag, tag.getPosition());


                    }
                });
                leftButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DataHolder tag = (DataHolder) view.getTag();
                        loadData(tag.flag, tag.getPosition());

                    }
                });
                itemLayoutView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                if (msg.what == 0) {
                                    // first we measure height, we need to do this because we used wrap_content
                                    // if we use a fixed height we could just pass that in px.
                                    ExpandCollapseAnimation.setHeightForWrapContent(getActivity(), dropDown);
                                    ExpandCollapseAnimation expandAni = new ExpandCollapseAnimation(dropDown, 100);
                                    dropDown.startAnimation(expandAni);
                                    Message newMsg = new Message();
                                } else if (msg.what == 1) {
                                    ExpandCollapseAnimation expandAni = new ExpandCollapseAnimation(dropDown, 100);
                                    dropDown.startAnimation(expandAni);
                                }
                            }
                        };
                        handler.sendEmptyMessage(0);
                    }
                });

            }
        }

        private class DataHolder {
            int position;
            boolean flag;

            public boolean isFlag() {
                return flag;
            }

            public void setFlag(boolean flag) {
                this.flag = flag;
            }

            public int getPosition() {
                return position;
            }

            public void setPosition(int position) {
                this.position = position;
            }
        }
    }

}

