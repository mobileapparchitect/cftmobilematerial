package com.mobileparadigm.arkangel.cftmobile.model;

public class ProcessedVerse {
    private String bookChaper;
    private String startVerse;
    private String endVerse;
    private String bookName;
    private String bookNumber;

        public ProcessedVerse(){
            setStartVerse("0");
        }

    private String abbreviatedBibleName;

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public String getStartVerse() {
        return startVerse;
    }

    public void setStartVerse(String startVerse) {
        this.startVerse = startVerse;
    }

    public String getEndVerse() {
        return endVerse;
    }

    public void setEndVerse(String endVerse) {
        this.endVerse = endVerse;
    }


    public String getBookChaper() {
        return bookChaper;
    }

    public void setBookChaper(String bookChaper) {
        this.bookChaper = bookChaper;
    }

    public String getAbbreviatedBibleName() {
        return abbreviatedBibleName;
    }

    public void setAbbreviatedBibleName(String abbreviatedBibleName) {
        this.abbreviatedBibleName = abbreviatedBibleName;
    }


}