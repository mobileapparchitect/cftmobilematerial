
package com.mobileparadigm.arkangel.cftmobile.data_models;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Meta extends BaseObservable implements Parcelable{

    @SerializedName("fums")
    @Expose
    private String fums;
    @SerializedName("fums_tid")
    @Expose
    private String fumsTid;
    @SerializedName("fums_js_include")
    @Expose
    private String fumsJsInclude;
    @SerializedName("fums_js")
    @Expose
    private String fumsJs;
    @SerializedName("fums_noscript")
    @Expose
    private String fumsNoscript;

    protected Meta(Parcel in) {
        fums = in.readString();
        fumsTid = in.readString();
        fumsJsInclude = in.readString();
        fumsJs = in.readString();
        fumsNoscript = in.readString();
    }

    public static final Creator<Meta> CREATOR = new Creator<Meta>() {
        @Override
        public Meta createFromParcel(Parcel in) {
            return new Meta(in);
        }

        @Override
        public Meta[] newArray(int size) {
            return new Meta[size];
        }
    };

    /**
     * 
     * @return
     *     The fums
     */
    public String getFums() {
        return fums;
    }

    /**
     * 
     * @param fums
     *     The fums
     */
    public void setFums(String fums) {
        this.fums = fums;
    }

    /**
     * 
     * @return
     *     The fumsTid
     */
    public String getFumsTid() {
        return fumsTid;
    }

    /**
     * 
     * @param fumsTid
     *     The fums_tid
     */
    public void setFumsTid(String fumsTid) {
        this.fumsTid = fumsTid;
    }

    /**
     * 
     * @return
     *     The fumsJsInclude
     */
    public String getFumsJsInclude() {
        return fumsJsInclude;
    }

    /**
     * 
     * @param fumsJsInclude
     *     The fums_js_include
     */
    public void setFumsJsInclude(String fumsJsInclude) {
        this.fumsJsInclude = fumsJsInclude;
    }

    /**
     * 
     * @return
     *     The fumsJs
     */
    public String getFumsJs() {
        return fumsJs;
    }

    /**
     * 
     * @param fumsJs
     *     The fums_js
     */
    public void setFumsJs(String fumsJs) {
        this.fumsJs = fumsJs;
    }

    /**
     * 
     * @return
     *     The fumsNoscript
     */
    public String getFumsNoscript() {
        return fumsNoscript;
    }

    /**
     * 
     * @param fumsNoscript
     *     The fums_noscript
     */
    public void setFumsNoscript(String fumsNoscript) {
        this.fumsNoscript = fumsNoscript;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fums);
        dest.writeString(fumsTid);
        dest.writeString(fumsJsInclude);
        dest.writeString(fumsJs);
        dest.writeString(fumsNoscript);
    }
}
