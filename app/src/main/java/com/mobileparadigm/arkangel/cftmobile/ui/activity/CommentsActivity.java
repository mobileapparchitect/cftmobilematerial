/*
package com.mobileparadigm.arkangel.cftmobile.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.common.eventbus.Subscribe;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.Utils;
import com.mobileparadigm.arkangel.cftmobile.ui.adapter.CommentsAdapter;
import com.mobileparadigm.arkangel.cftmobile.ui.view.SendCommentButton;
import com.mobileparadigm.arkangel.cftmobile.utils.PreferencesHelper;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import de.greenrobot.event.EventBus;


*/
/**
 * Created by froger_mcs on 11.11.14.
 *//*

public class CommentsActivity extends BaseActivity implements SendCommentButton.OnSendClickListener {
    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";
    public static final String COMMENT_TAG_KEY = "BulletinComment";
    public static final String COMMENT_CREATOR_NAME = "username";
    public static final String COMMENT_ID = "commentId";
    public static final String COMMENT_MESSAGE_DETAIL = "messageDetail";

    @Bind(R.id.contentRoot)
    LinearLayout contentRoot;
    @Bind(R.id.rvComments)
    RecyclerView rvComments;
    @Bind(R.id.llAddComment)
    LinearLayout llAddComment;
    @Bind(R.id.etComment)
    EditText etComment;
    @Bind(R.id.btnSendComment)
    SendCommentButton btnSendComment;
    private ArrayList<ParseObject> parseObjectArrayList;
    private CommentsAdapter commentsAdapter;
    private int drawingStartLocation;

    public static void startCommentsActivity(Context context) {
        Intent intent = new Intent(context, CommentsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        setupComments();
        setupSendCommentButton();

        EventBus.getDefault().register(CommentsActivity.this);

        drawingStartLocation = getIntent().getIntExtra(ARG_DRAWING_START_LOCATION, 0);
        if (savedInstanceState == null) {
            contentRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    contentRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    startIntroAnimation();
                    return true;
                }
            });
        }
    }

    private void setupComments() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setHasFixedSize(true);

        commentsAdapter = new CommentsAdapter(this);
        getLastFifteenComments();

        rvComments.setAdapter(commentsAdapter);
        rvComments.setOverScrollMode(View.OVER_SCROLL_NEVER);
        rvComments.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    commentsAdapter.setAnimationsLocked(true);
                }
            }
        });
    }

    private void setupSendCommentButton() {
        btnSendComment.setOnSendClickListener(this);
    }

    private void startIntroAnimation() {
        ViewCompat.setElevation(getToolbar(), 0);
        contentRoot.setScaleY(0.1f);
        contentRoot.setPivotY(drawingStartLocation);
        llAddComment.setTranslationY(200);

        contentRoot.animate()
                .scaleY(1)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ViewCompat.setElevation(getToolbar(), Utils.dpToPx(8));
                        animateContent();
                    }
                })
                .start();
    }

    private void animateContent() {
        commentsAdapter.updateItems();
        llAddComment.animate().translationY(0)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(200)
                .start();
    }

    @Override
    public void onBackPressed() {
        ViewCompat.setElevation(getToolbar(), 0);
        contentRoot.animate()
                .translationY(Utils.getScreenHeight(this))
                .setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        CommentsActivity.super.onBackPressed();
                        overridePendingTransition(0, 0);
                    }
                })
                .start();
    }

    @Override
    public void onSendClickListener(View v) {
        if (validateComment()) {
            // commentsAdapter.addItem();
            // build parseObject
            saveParseMessageObject();

        }
    }


    @Subscribe
    public void onEventMainThread(CommentsActivityEvent commentsActivityEvent) {
        getLastFifteenComments();
    }

    public void saveParseMessageObject() {
        ParseObject gameScore = new ParseObject(CommentsActivity.COMMENT_TAG_KEY);
        gameScore.put(CommentsActivity.COMMENT_ID, PreferencesHelper.getCurrentAnnouncementId());
        gameScore.put(CommentsActivity.COMMENT_CREATOR_NAME, "Sean Plott");
        gameScore.put(CommentsActivity.COMMENT_MESSAGE_DETAIL, etComment.getText().toString());
        etComment.setText(null);
        btnSendComment.setCurrentState(SendCommentButton.STATE_DONE);
        gameScore.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {



                getLastFifteenComments();
                ParsePush push = new ParsePush();
                push.setChannel(CommentsActivity.COMMENT_TAG_KEY);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("action", "com.mobileparadigm.arkangel.cftmobile.UPDATE_COMMENT");
                    jsonObject.put("alert", "New Comment");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                push.setExpirationTime(1000);
                push.setData(jsonObject);
                push.sendInBackground();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(CommentsActivity.this);
    }

    public void getLastFifteenComments() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("BulletinComment");
        query.orderByDescending("updatedAt");
        query.whereEqualTo(CommentsActivity.COMMENT_ID, PreferencesHelper.getCurrentAnnouncementId());
        query.setLimit(15);
        try {
            if (parseObjectArrayList == null) {
                parseObjectArrayList = new ArrayList<>();
            } else {
                parseObjectArrayList.clear();
            }
            parseObjectArrayList = (ArrayList<ParseObject>) query.find();
            commentsAdapter.addAllItems(parseObjectArrayList);

            if(commentsAdapter.getItemCount()>0){
                rvComments.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rvComments.smoothScrollBy(0, rvComments.getChildAt(0).getHeight() * commentsAdapter.getItemCount());
                    }
                }, 1000);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private boolean validateComment() {
        if (TextUtils.isEmpty(etComment.getText())) {
            btnSendComment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error));
            return false;
        }

        return true;
    }

    public static class CommentsActivityEvent {

    }
}
*/
