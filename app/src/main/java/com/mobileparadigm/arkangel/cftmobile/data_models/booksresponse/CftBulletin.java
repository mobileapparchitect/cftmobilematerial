package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

/**
 * Created by arkangel on 09/08/15.
 */

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;


@RealmClass
public class CftBulletin extends RealmObject {

    private String bulletin_title;

    public String getBulletin_theme() {
        return bulletin_theme;
    }

    public void setBulletin_theme(String bulletin_theme) {
        this.bulletin_theme = bulletin_theme;
    }

    private  String bulletin_theme;

    private String bulletin_detail;

    private String bulletin_parent_id;
    @PrimaryKey
    private long cftBulletinId;

    public long getCftBulletinId() {
        return cftBulletinId;
    }

    public void setCftBulletinId(long cftBulletinId) {
        this.cftBulletinId = cftBulletinId;
    }

    public String getBulletin_title() {
        return bulletin_title;
    }

    public void setBulletin_title(String bulletin_title) {
        this.bulletin_title = bulletin_title;
    }

    public String getBulletin_detail() {
        return bulletin_detail;
    }

    public void setBulletin_detail(String bulletin_detail) {
        this.bulletin_detail = bulletin_detail;
    }

    public String getBulletin_parent_id() {
        return bulletin_parent_id;
    }

    public void setBulletin_parent_id(String bulletin_parent_id) {
        this.bulletin_parent_id = bulletin_parent_id;
    }


}
