package com.mobileparadigm.arkangel.cftmobile.ridesharing.utils;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

/**
 * Journey class
 * 
 * To compute the time and distance to go
 * from (Point2D.Double)A to (Point2D.Double)B.
 * @author Debosmit
 *
 */
public class Journey implements Comparable<Journey> {

	private JourneyData thisJourney;
	
	private static final boolean DEBUG = Constants.isDebugMode();

	public Journey(JourneyData thisJourney) {
		this.setThisJourney(thisJourney);
	}
	
	private void setThisJourney(JourneyData thisJourney) {
		this.thisJourney = thisJourney;
	}

	private String getOriginString() {
		StringBuilder st = new StringBuilder();
		st.append("origin=");
		LatLng start = thisJourney.getStartLocation();
		st.append(String.valueOf(start.longitude + "," + start.latitude));
		return st.toString();
	}
	
	private String getDestinationString() {
		StringBuilder st = new StringBuilder();
		st.append("&destination=");
		LatLng end = thisJourney.getEndLocation();
		st.append(end.longitude + "," + end.latitude);
		return st.toString();
	}
	
	private String getWaypointsString() {
		Set<LatLng> waypoints = thisJourney.getWaypoints();
		if(waypoints == null)
			return "";
		
		StringBuilder st = new StringBuilder();
		st.append("&waypoints=");
		for (Iterator<LatLng> i = waypoints.iterator(); i.hasNext();) {
			LatLng point = i.next();
		    st.append(point.longitude + "," + point.latitude + "|");
		}
		// remove extra | at the end
		// sorry for the bad style
		st.deleteCharAt(st.length() - 1);
		return st.toString();
	}
	
	private void validateAndSetURL() {
		if(thisJourney.getStartLocation() == null || thisJourney.getEndLocation() == null)
			throw new IllegalArgumentException("Coordinates are not set");
		
		// ensure coordinates are within bounds of Seattle
		if(!validBounds()) 
			throw new IllegalStateException("Only trips inside "
					+ "Seattle are supported now.");
		
		// URL for Google Maps
		String url = 
				"https://maps.googleapis.com/maps/api/directions/json?" 
					+ getOriginString()
					+ getDestinationString()
					+ getWaypointsString()
					+ "&mode="
					+ Constants.getMode()
					+ "&language=en-EN&sensor=false"
					+ "&key="
					+ Constants.getKey();
		thisJourney.setURL(url);
		
		if(DEBUG)
			System.out.println("DEBUG URL: " + url);
	}
	
	public void obtainData() {
		validateAndSetURL();
		try {
			URL url = new URL(thisJourney.getURL());
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuilder downloadedData = new StringBuilder();
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	        	downloadedData.append(inputLine);
	        in.close();
	        
	        // parse data
	        parseData(downloadedData.toString());
		} catch (MalformedURLException e) {
			if(DEBUG)
				System.out.println("URL is malformed");
			e.printStackTrace();
		} catch (IOException e) {
			if(DEBUG)
				System.out.println("Something went wrong with IO Streams");
			e.printStackTrace();
		}
	}

	private void parseData(String data) {
		try {
			JSONObject container = new JSONObject(data);
			
			// routes -> legs -> leg0, leg1, leg2, ....
			JSONArray routesObj = container.getJSONArray("routes");
			JSONObject routesBody = routesObj.getJSONObject(0);
			JSONArray legs = routesBody.getJSONArray("legs");
			
			// without waypoints, legs contains 1 object, start -> end
			// with waypoints, legs contains 1 + length(waypoints) objects.
			int length = 1;
			if(thisJourney.getWaypoints() != null)
				length += thisJourney.getWaypoints().size();
			
			assert length == legs.length();
			
			long totalDistance = 0;		// in meters
			long totalTime = 0;			// in seconds
			for(int i = 0 ; i < legs.length() ; i++) {
				JSONObject legContents = legs.getJSONObject(i);
				
				// distance sub-array
				JSONObject distance = legContents.getJSONObject("distance");
				totalDistance += distance.getLong("value");
				
				// duration sub-array
				JSONObject duration = legContents.getJSONObject("duration");
				totalTime += duration.getLong("value");
			}
			
			// debug console
			if(DEBUG) {
				System.out.println("DEBUG Total distance: " + totalDistance);
				System.out.println("DEBUG Total duration: " + totalTime + "\n");
			}
			
			thisJourney.setDistance(totalDistance);
			thisJourney.setTime(totalTime);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private boolean validBounds() {
		// validate start
		if(!validateCoordinate(thisJourney.getStartLocation()))
			return false;
		
		// validate end
		if(!validateCoordinate(thisJourney.getEndLocation()))
			return false;
		
		// validate waypoints
		Set<LatLng> waypoints = thisJourney.getWaypoints();
		if(waypoints == null)
			return true;
		
		for(LatLng waypoint: waypoints) {
			if(!validateCoordinate(waypoint))
				return false;
		}
		return true;
	}
	
	private boolean validateCoordinate(LatLng point) {
//		if(DEBUG) {
//			System.out.println("DEBUG LAT/LONG");
//			System.out.println("(minLong, maxLong), (minLat, maxLat)\n[" + Constants.getMinimumLongitude() + " " + 
//					Constants.getMaximumLongitude() 
//					+"]  [" + Constants.getMinimumLatitude() + 
//					" " + Constants.getMaximumLatitude() + "]") ;
//			
//			System.out.println("(pointLat, pointLong)\n[" + point.getY() + " " + point.getX() + "]\n\n");
//		}
		if(Math.abs(point.latitude) < Math.abs(Constants.getMinimumLongitude())
				|| Math.abs(point.latitude) > Math.abs(Constants.getMaximumLongitude())
				|| Math.abs(point.longitude) < Math.abs(Constants.getMinimumLatitude())
				|| Math.abs(point.longitude) > Math.abs(Constants.getMaximumLatitude()))
			return false;
		
		return true;
	}

	@Override
	public int compareTo(Journey o) {
		return this.thisJourney.compareTo(o.thisJourney);
	}
	
	@Override
	public String toString() {
		return thisJourney.toString();
	}

	public void computeHaversines() {
		if(thisJourney.getStartLocation() == null || thisJourney.getEndLocation() == null)
			throw new IllegalArgumentException("Coordinates are not set");
		
		// ensure coordinates are within bounds of Seattle
		if(!validBounds()) 
			throw new IllegalStateException("Only trips inside "
					+ "Seattle are supported now.");

		LatLng cur = thisJourney.getStartLocation();
		long totalDistance = 0;
		for(LatLng waypoint: thisJourney.getWaypoints()) {
			totalDistance += Haversine.getHaversine(cur.longitude, waypoint.longitude,
													cur.latitude, waypoint.latitude);
			cur = waypoint;
		}

		LatLng end = thisJourney.getEndLocation();
		totalDistance += Haversine.getHaversine(cur.longitude, end.longitude,
				cur.latitude, end.latitude);
		
		thisJourney.setDistance(totalDistance);
		thisJourney.setTime(0);
		
	}
}
