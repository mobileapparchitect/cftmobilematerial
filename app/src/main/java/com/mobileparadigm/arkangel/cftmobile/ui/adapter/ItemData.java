package com.mobileparadigm.arkangel.cftmobile.ui.adapter;

public class ItemData {


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    private int imageUrl;
     
    public ItemData(String title,int imageUrl){
         
        this.title = title;
        this.imageUrl = imageUrl;
    }
    // getters & setters
}
