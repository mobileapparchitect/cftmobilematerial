
package com.mobileparadigm.arkangel.cftmobile.data_models.firebase;


import com.fasterxml.jackson.annotation.JsonProperty;

public class EventList {

    @JsonProperty("detail")
    private String detail;
    @JsonProperty("eventEndDate")
    private String eventEndDate;
    @JsonProperty("eventStartDate")
    private String eventStartDate;
    @JsonProperty("title")
    private String title;

    /**
     * 
     * @return
     *     The detail
     */
    @JsonProperty("detail")
    public String getDetail() {
        return detail;
    }

    /**
     * 
     * @param detail
     *     The detail
     */
    @JsonProperty("detail")
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 
     * @return
     *     The eventEndDate
     */
    @JsonProperty("eventEndDate")
    public String getEventEndDate() {
        return eventEndDate;
    }

    /**
     * 
     * @param eventEndDate
     *     The eventEndDate
     */
    @JsonProperty("eventEndDate")
    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    /**
     * 
     * @return
     *     The eventStartDate
     */
    @JsonProperty("eventStartDate")
    public String getEventStartDate() {
        return eventStartDate;
    }

    /**
     * 
     * @param eventStartDate
     *     The eventStartDate
     */
    @JsonProperty("eventStartDate")
    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

}
