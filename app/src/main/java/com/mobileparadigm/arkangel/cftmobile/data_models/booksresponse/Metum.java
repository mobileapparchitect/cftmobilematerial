package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class Metum   implements Parcelable{

    private static final String FIELD_FUMS = "fums";
    private static final String FIELD_FUMS_JS = "fums_js";
    private static final String FIELD_FUMS_NOSCRIPT = "fums_noscript";
    private static final String FIELD_FUMS_TID = "fums_tid";
    private static final String FIELD_FUMS_JS_INCLUDE = "fums_js_include";


    @SerializedName(FIELD_FUMS)
    private String mFum;
    @SerializedName(FIELD_FUMS_JS)
    private String mFumsJ;
    @SerializedName(FIELD_FUMS_NOSCRIPT)
    private String mFumsNoscript;
    @SerializedName(FIELD_FUMS_TID)
    private String mFumsTid;
    @SerializedName(FIELD_FUMS_JS_INCLUDE)
    private String mFumsJsInclude;


    public Metum(){

    }

    public void setFum(String fum) {
        mFum = fum;
    }

    public String getFum() {
        return mFum;
    }

    public void setFumsJ(String fumsJ) {
        mFumsJ = fumsJ;
    }

    public String getFumsJ() {
        return mFumsJ;
    }

    public void setFumsNoscript(String fumsNoscript) {
        mFumsNoscript = fumsNoscript;
    }

    public String getFumsNoscript() {
        return mFumsNoscript;
    }

    public void setFumsTid(String fumsTid) {
        mFumsTid = fumsTid;
    }

    public String getFumsTid() {
        return mFumsTid;
    }

    public void setFumsJsInclude(String fumsJsInclude) {
        mFumsJsInclude = fumsJsInclude;
    }

    public String getFumsJsInclude() {
        return mFumsJsInclude;
    }

    public Metum(Parcel in) {
        mFum = in.readString();
        mFumsJ = in.readString();
        mFumsNoscript = in.readString();
        mFumsTid = in.readString();
        mFumsJsInclude = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Metum> CREATOR = new Creator<Metum>() {
        public Metum createFromParcel(Parcel in) {
            return new Metum(in);
        }

        public Metum[] newArray(int size) {
        return new Metum[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFum);
        dest.writeString(mFumsJ);
        dest.writeString(mFumsNoscript);
        dest.writeString(mFumsTid);
        dest.writeString(mFumsJsInclude);
    }


}