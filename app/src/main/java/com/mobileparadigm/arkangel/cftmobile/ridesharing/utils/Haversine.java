package com.mobileparadigm.arkangel.cftmobile.ridesharing.utils;

/**
 * Calculate the haversine distance
 * https://en.wikipedia.org/wiki/Haversine_formula
 * 
 * From Wikipedia:
 * 		The haversine formula is an equation important in 
 * 		navigation, giving great-circle distances between 
 * 		two points on a sphere from their longitudes and 
 * 		latitudes. It is a special case of a more general 
 * 		formula in spherical trigonometry, the law of haversines, 
 * 		relating the sides and angles of spherical triangles. 
 * 		The first table of haversines in English was published 
 * 		by James Andrew in 1805.
 * 
 * @author Debosmit
 *
 */
public class Haversine {

	private final static int RADIUS = 6371;	// Earth's radius in kilometers
	
	public static double getHaversine(double lat1, double lat2, 
			double lon1, double lon2) {
		
		// Δlat = lat2− lat1
		double latDistance = toRadian(lat2-lat1);
		
		// Δlong = long2− long1
        double lonDistance = toRadian(lon2-lon1);
        
        // a = sin²(Δlat/2) + cos(lat1).cos(lat2).sin²(Δlong/2)
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRadian(lat1)) * Math.cos(toRadian(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        
        // c = 2.atan2(√a, √(1−a))
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        
        // distance = RADIUS * c
        double distance = RADIUS * c;
        
        return distance;
	}
	
	private static double toRadian(double value) {
        return value * Math.PI / 180;
    }
}
