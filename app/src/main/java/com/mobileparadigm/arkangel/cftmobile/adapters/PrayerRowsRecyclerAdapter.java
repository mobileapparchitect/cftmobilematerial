package com.mobileparadigm.arkangel.cftmobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PrayerRowsRecyclerAdapter extends RecyclerView.Adapter<PrayerRowsRecyclerAdapter.ViewHolder> {


    public static Context mContext;
    private List<String> allPrayers;

    public PrayerRowsRecyclerAdapter(Context context) {
        mContext = context;
        allPrayers = CftDataHolder.getInstance().getAnnouncement().getPrayerPoints();
    //    EventBusProvider.getInstance().register(ViewpagerRowAdapter.this);


    }

    @Override
    public int getItemCount() {
        return allPrayers != null ? allPrayers.size() : 0;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.home_card_row_prayer_item, parent, false);
        if (allPrayers.size() == 1) {
            WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            int width = windowManager.getDefaultDisplay().getWidth();
            int height = windowManager.getDefaultDisplay().getHeight();
            convertView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
        }
        return new ViewHolder(convertView);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.titleTextView.setText(allPrayers.get(position));
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_text_view)
        TextView titleTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
