package com.mobileparadigm.arkangel.cftmobile.ui.fragments;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verse;
import com.mobileparadigm.arkangel.cftmobile.data_models.Verses;
import com.mobileparadigm.arkangel.cftmobile.ui.activity.PickerActivity;
import com.mobileparadigm.arkangel.cftmobile.ui.view.IntelligentTextView;
import com.mobileparadigm.arkangel.cftmobile.ui.view.ShowHideMoreText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


//import com.mobileparadigm.arkangel.cftmobile.background_jobs.VerseShareJob;

/**
 * Created by arkangel on 23/07/15.
 */
public class BibleVersesFragment extends Fragment {
    @BindView(R.id.frame)
    SwipeFlingAdapterView flingContainer;
    @BindView(R.id.bible_verse_progress_bar)
    ProgressBar bibleVerseProgressBar;
    private int i;
    private View rootView;
    private boolean errorFound;
    private CardAdapter cardAdapter;

    public static BibleVersesFragment getInstance() {
        return new BibleVersesFragment();
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        int left = view.getPaddingLeft();
        int top = view.getPaddingTop();
        int right = view.getWidth() - view.getPaddingRight();
        int bottom = view.getHeight() - view.getPaddingBottom();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth()+left, view.getHeight()+right, Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.MAGENTA);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout wrapper = new LinearLayout(getActivity()); // for example
        rootView = inflater.inflate(R.layout.layout_with_swipe_view, wrapper, true);
        ButterKnife.bind(this, rootView);
       // EventBus.getDefault().register(BibleVersesFragment.this);
        init();
     //   setRetainInstance(true);
        return rootView;

    }

    @com.google.common.eventbus.Subscribe
    public void onEventMainThread(Verses verses) {
        cardAdapter.addStringToList(verses);
    }


    @com.google.common.eventbus.Subscribe
    public void onEventMainThread(String verseUrlString) {
        bibleVerseProgressBar.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(verseUrlString)) {
            PickerActivity.startPickerActivity(getContext(),verseUrlString);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        cardAdapter.addAllVerses(CFTMaterialApplication.bibleVerses);
        cardAdapter.notifyDataSetChanged();
    }

    public void init() {
        if (cardAdapter == null) {
            cardAdapter = new CardAdapter((AppCompatActivity) getActivity());
        }
        flingContainer.setAdapter(cardAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                cardAdapter.getListofCardTexts().remove(0);
                cardAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {

            }

            @Override
            public void onRightCardExit(Object dataObject) {

            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // Ask for more data here

               // CFTMaterialApplication.jobManager.addJobInBackground(new ProcessBibleVersesJob(new CountDownLatch(1)));
               // CFTMaterialApplication.jobManager.start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cardAdapter.getListofCardTexts().addAll(CFTMaterialApplication.bibleVerses);
                        cardAdapter.notifyDataSetChanged();
                        Log.d("LIST", "notified");
                        i++;
                    }
                },2000);

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                Snackbar snackbar = Snackbar
                        .make(rootView, "Swipe left or right", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });

    }

    private class  CardAdapter extends BaseAdapter {
        private ArrayList<Verses> listofCardTexts;
        private AppCompatActivity appCompatActivity;

        public CardAdapter(AppCompatActivity context) {
            listofCardTexts = new ArrayList<>();
            appCompatActivity = context;
        }

        public void addStringToList(Verses text) {
            listofCardTexts.add(text);
            notifyDataSetChanged();
        }

        public void addAllVerses(ArrayList<Verses> listOfAllVerses) {
            listofCardTexts.addAll(listOfAllVerses);
            notifyDataSetChanged();
        }

        public ArrayList<Verses> getListofCardTexts() {
            return listofCardTexts;
        }

        public void setListofCardTexts(ArrayList<Verses> listofCardTexts) {
            this.listofCardTexts = listofCardTexts;
        }

        @Override
        public int getCount() {
            return listofCardTexts.size();
        }

        @Override
        public Object getItem(int position) {
            return listofCardTexts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final BibleVerseViewHolder mViewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(appCompatActivity).inflate(R.layout.swipe_card_item, parent, false);
                mViewHolder = new BibleVerseViewHolder(convertView);
                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (BibleVerseViewHolder) convertView.getTag();
            }

            final Verses currentVerse = (Verses) getItem(position);

            StringBuilder builder = new StringBuilder();
            for (final Verse verse : currentVerse.getResponse().getVerses()) {
                builder.append(Html.fromHtml(verse.getText().toString().replace(String.valueOf(verse.getVerse()), "")));
                mViewHolder.verseTitle.setText(verse.getReference());

            }
            mViewHolder.verseDetail.setText(builder.toString());
            mViewHolder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  CFTMaterialApplication.jobManager.addJobInBackground(new VerseShareJob(getBitmapFromView(finalConvertView), currentVerse.getResponse().getVerses().get(0).getReference()));
                 //   CFTMaterialApplication.jobManager.start();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bibleVerseProgressBar.setVisibility(View.VISIBLE);
                        }
                    });
                }
            });

            return convertView;

        }
    }

    private class BibleVerseViewHolder {
        ShowHideMoreText verseDetail;
        TextView verseTitle;
        ImageView shareButton;

        public BibleVerseViewHolder(View item) {
            verseDetail = (ShowHideMoreText) item.findViewById(R.id.verse_detail);
            verseTitle = (IntelligentTextView) item.findViewById(R.id.verse_title);
            shareButton = (ImageView) item.findViewById(R.id.share);
        }
    }
}



