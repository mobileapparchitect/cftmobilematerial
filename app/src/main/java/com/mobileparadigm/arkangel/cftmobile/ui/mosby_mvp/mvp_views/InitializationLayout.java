package com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_views;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.viewstate.ViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.layout.MvpViewStateFrameLayout;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_presenter.InitializationPresenter;
import com.mobileparadigm.arkangel.cftmobile.ui.mosby_mvp.mvp_presenter.InitializationViewPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InitializationLayout extends MvpViewStateFrameLayout<InitializationView, InitializationPresenter>
        implements InitializationView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.loadingView)
    View loadingView;
    @BindView(R.id.errorView)
    TextView errorView;
    @BindView(R.id.contentView)
    SwipeRefreshLayout contentView;


    public InitializationLayout(Context context) {
        super(context);
    }

    public InitializationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InitializationLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public InitializationLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this, this);
        contentView.setOnRefreshListener(this);
    }

    @Override
    public InitializationPresenter createPresenter() {
        return new InitializationViewPresenter();
    }


    @NonNull
    @Override
    public ViewState<InitializationView> createViewState() {
        return null;
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.loadData();
    }


    @OnClick(R.id.errorView)
    public void onErrorViewClicked() {
        presenter.loadData();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

    }

    @Override
    public void onRefresh() {

    }
}