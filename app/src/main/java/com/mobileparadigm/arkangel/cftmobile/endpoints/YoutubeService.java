package com.mobileparadigm.arkangel.cftmobile.endpoints;


import com.mobileparadigm.arkangel.cftmobile.data_models.youtube.YoutubeVideosContent;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by arkangel on 11/07/16.
 */
public interface YoutubeService {

    //"https://www.googleapis.com/youtube/v3/videos?chart=mostPopular&part=snippet&forUsername=christfaithtabernaclelondon&key=AIzaSyANPi9HcCOKb-k9D2Rkb_uijRxZBJT9LNQ")

    @GET("/youtube/v3/videos")
    Observable<YoutubeVideosContent> getYoutube(@Query("chart") String mostPopular, @Query("part") String snippet, @Query("forUsername") String christfaithtabernaclelondon, @Query("key") String key);

}
