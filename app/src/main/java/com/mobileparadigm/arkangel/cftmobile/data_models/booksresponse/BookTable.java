package com.mobileparadigm.arkangel.cftmobile.data_models.booksresponse;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by arkangel on 12/08/15.
 */


public class BookTable extends RealmObject {

    private int numberOfChapters;
    private String abbreviation;
    private String name;

    public int getNumberOfChapters() {
        return numberOfChapters;
    }

    public void setNumberOfChapters(int numberOfChapters) {
        this.numberOfChapters = numberOfChapters;
    }

    public long getBooktableId() {
        return booktableId;
    }

    public void setBooktableId(long booktableId) {
        this.booktableId = booktableId;
    }

    @PrimaryKey
    private long booktableId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
}
