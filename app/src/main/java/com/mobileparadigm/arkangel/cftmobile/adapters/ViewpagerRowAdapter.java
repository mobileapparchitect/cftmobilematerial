package com.mobileparadigm.arkangel.cftmobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.mobileparadigm.arkangel.cftmobile.rxjava.RxUtil;
import com.mobileparadigm.arkangel.cftmobile.ui.fragments.HighLightsFragment;
import com.mobileparadigm.arkangel.cftmobile.ui.view.AutoScrollViewPager;
import com.mobileparadigm.arkangel.cftmobile.utils.CftDataHolder;
import com.mobileparadigm.arkangel.cftmobile.utils.DateUtils;


import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;

import rx.Observable;
import rx.Subscriber;


public class ViewpagerRowAdapter extends RecyclerView.Adapter<ViewpagerRowAdapter.ViewHolder> {

    public static Context mContext;
    private HomePageBannerImagePagerAdapter homePageBannerImagePagerAdapter;

    public ViewpagerRowAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return 1;
    }


    @Override
    public ViewpagerRowAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =
                (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.viewpager_header, parent, false);
        WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        convertView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.dataPublishedTextView.setText(DateUtils.getFullDateInBannerFormat(new LocalDate()));
        homePageBannerImagePagerAdapter = new HomePageBannerImagePagerAdapter(mContext, holder.autoScrollViewPager);
        holder.autoScrollViewPager.setAdapter(homePageBannerImagePagerAdapter);
        holder.autoScrollViewPager.setInterval(2000);
        holder.autoScrollViewPager.setOffscreenPageLimit(0);
        holder.autoScrollViewPager.startAutoScroll();

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.slideshow)
        AutoScrollViewPager autoScrollViewPager;
        @BindView(R.id.tabernacle_news_date)
        TextView dataPublishedTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


    private class HomePageBannerImagePagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

        private Activity context;
        private ProgressBar progressBar;
        private int myProgress;
        private List<GlideDrawable> listOfObjects;
        private ImageView featuredImage;

        private AutoScrollViewPager pager;

        public HomePageBannerImagePagerAdapter(Context context, AutoScrollViewPager viewPager) {
            this.context = (Activity) context;
            pager = viewPager;
            listOfObjects = new ArrayList<>();
            if (CftDataHolder.getInstance().getListOfHomeViewPagerDrawables().isEmpty()) {
                loadGlideDrawable();
            } else {
                addAllDrawables(CftDataHolder.getInstance().getListOfHomeViewPagerDrawables());
            }
        }


        private class LoginSubscriber extends Subscriber<Object> {

            @Override
            public void onCompleted() {

                Log.i("Test", "test");
            }


            @Override
            public void onError(Throwable e) {

                Log.i("Test", "test");
            }

            @Override
            public void onNext(Object userDataResponse) {
                Log.i("Test", userDataResponse.toString());
            }
        }


        /*   public void loadDrawables() {
               List<String> listOfImageUrls = Arrays.asList(CftMobileApplication.getAppContext().getResources().getStringArray(R.array.cft_pictures_landscape));
               for (int urlIndex = 0; urlIndex < listOfImageUrls.size(); urlIndex++) {
                   Glide.with(CFTMaterialApplication.getAppContext()).load(listOfImageUrls.get(urlIndex))
                           .diskCacheStrategy(DiskCacheStrategy.ALL)
                           .into(new SimpleTarget<GlideDrawable>() {
                               @Override
                               public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                   CftDataHolder.getInstance().getListOfHomeViewPagerDrawables().add(resource);
                                   listOfObjects.add(resource);
                                   notifyDataSetChanged();
                               }

                           });


               }
           }
   */
      //  GlideObserable

        public Observable<GlideDrawable> getUrl(String url){
            Glide.with(CFTMaterialApplication.getInstance()).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        Observable.just(resource).subscribe(new Subscriber<GlideDrawable>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(GlideDrawable glideDrawable) {
                                CftDataHolder.getInstance().getListOfHomeViewPagerDrawables().add(glideDrawable);
                                listOfObjects.add(glideDrawable);
                                notifyDataSetChanged();
                            }
                        });
                }
            });
            return null;
        }


        public Observable<Observable<GlideDrawable>> loadGlideDrawable() {
            final List<String> listOfImageUrls = Arrays.asList(CFTMaterialApplication.getInstance().getResources().getStringArray(R.array.cft_pictures_landscape));
            for (final String string : listOfImageUrls) {
                Observable.just(getUrl(string)).compose(RxUtil.<Observable<GlideDrawable>>applyIOToMainThreadSchedulers());
            }
            return null;
        }


        public void addAllDrawables(ArrayList<GlideDrawable> drawables) {
            listOfObjects.addAll(drawables);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return listOfObjects.size();
        }

        @Override
        public Object instantiateItem(View container, int position) {
            LayoutInflater inflater = LayoutInflater.from(context);
            LinearLayout view = (LinearLayout) inflater.inflate(
                    R.layout.main_header_view, null);
            featuredImage = (ImageView) view.findViewById(R.id.header_image);
            featuredImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //EventBusProvider.getInstance().post(new HighLightsFragment.SendEvent());
                }
            });

            progressBar = (ProgressBar) view.findViewById(R.id.player_exp_bar);
            progressBar.setProgressDrawable(context.getResources().getDrawable(
                    R.drawable.lightblue_progress));

            if (listOfObjects != null && !listOfObjects.isEmpty()) {
                final GlideDrawable drawable = listOfObjects
                        .get(position);
                featuredImage.setImageDrawable(drawable);
            }

            ((ViewPager) container).addView(view, 0);
            new ProgressThread().start();
            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            pager.stopAutoScroll();
        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        private class ProgressThread extends Thread {

            private Handler progressThreadHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    myProgress++;
                    progressBar.setProgress(myProgress);
                }
            };

            @Override
            public void run() {
                while (myProgress < 50) {
                    try {
                        myProgress += 20;
                        progressBar.setProgress(myProgress);
                        progressThreadHandler.sendMessage(progressThreadHandler
                                .obtainMessage());
                        Thread.sleep(200);
                    } catch (Throwable t) {
                    }
                }

            }

        }

    }
}
