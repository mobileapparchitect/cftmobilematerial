
package com.mobileparadigm.arkangel.cftmobile.data_models.firebase;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Bulletin {

    @JsonProperty("weeklyBulletin")
    private WeeklyBulletin weeklyBulletin;

    /**
     * 
     * @return
     *     The weeklyBulletin
     */
    @JsonProperty("weeklyBulletin")
    public WeeklyBulletin getWeeklyBulletin() {
        return weeklyBulletin;
    }

    /**
     * 
     * @param weeklyBulletin
     *     The weeklyBulletin
     */
    @JsonProperty("weeklyBulletin")
    public void setWeeklyBulletin(WeeklyBulletin weeklyBulletin) {
        this.weeklyBulletin = weeklyBulletin;
    }

}
