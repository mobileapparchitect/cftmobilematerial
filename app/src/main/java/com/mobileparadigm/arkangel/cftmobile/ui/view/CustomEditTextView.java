package com.mobileparadigm.arkangel.cftmobile.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;

import com.mobileparadigm.arkangel.cftmobile.ui.utils.FontHelper;

import com.mobileparadigm.arkangel.cftmobile.R;
public class CustomEditTextView extends EditText {

    public CustomEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(attrs);
        }
    }

    private void init(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs,
                R.styleable.com_mobileparadigm_arkangel_cftmobile_ui_view_CustomEditTextView);
        String customFont = a
                .getString(R.styleable.com_mobileparadigm_arkangel_cftmobile_ui_view_CustomEditTextView_font);

        if (!TextUtils.isEmpty(customFont)) {
            setTypeface(FontHelper.getFontFromAssets(getContext(), customFont));
        }
        a.recycle();
    }

    @Override
    public void setTypeface(Typeface tf) {
        super.setTypeface(tf);
    }

}