
package com.mobileparadigm.arkangel.cftmobile.data_models.firebase;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Announcement {

    @JsonProperty("announcementDetails")
    private AnnouncementDetails announcementDetails;
    @JsonProperty("bibleStudies")
    private List<BibleStudy> bibleStudies = new ArrayList<BibleStudy>();
    @JsonProperty("bulletin")
    private List<Bulletin> bulletin = new ArrayList<Bulletin>();
    @JsonProperty("eventList")
    private List<EventList> eventList = new ArrayList<EventList>();
    @JsonProperty("prayerPoints")
    private List<String> prayerPoints = new ArrayList<String>();

    /**
     *
     * @return
     *     The announcementDetails
     */
    @JsonProperty("announcementDetails")
    public AnnouncementDetails getAnnouncementDetails() {
        return announcementDetails;
    }

    /**
     *
     * @param announcementDetails
     *     The announcementDetails
     */
    @JsonProperty("announcementDetails")
    public void setAnnouncementDetails(AnnouncementDetails announcementDetails) {
        this.announcementDetails = announcementDetails;
    }

    /**
     *
     * @return
     *     The bibleStudies
     */
    @JsonProperty("bibleStudies")
    public List<BibleStudy> getBibleStudies() {
        return bibleStudies;
    }

    /**
     *
     * @param bibleStudies
     *     The bibleStudies
     */
    @JsonProperty("bibleStudies")
    public void setBibleStudies(List<BibleStudy> bibleStudies) {
        this.bibleStudies = bibleStudies;
    }

    /**
     *
     * @return
     *     The bulletin
     */
    @JsonProperty("bulletin")
    public List<Bulletin> getBulletin() {
        return bulletin;
    }

    /**
     * 
     * @param bulletin
     *     The bulletin
     */
    @JsonProperty("bulletin")
    public void setBulletin(List<Bulletin> bulletin) {
        this.bulletin = bulletin;
    }

    /**
     * 
     * @return
     *     The eventList
     */
    @JsonProperty("eventList")
    public List<EventList> getEventList() {
        return eventList;
    }

    /**
     * 
     * @param eventList
     *     The eventList
     */
    @JsonProperty("eventList")
    public void setEventList(List<EventList> eventList) {
        this.eventList = eventList;
    }

    /**
     * 
     * @return
     *     The prayerPoints
     */
    @JsonProperty("prayerPoints")
    public List<String> getPrayerPoints() {
        return prayerPoints;
    }

    /**
     * 
     * @param prayerPoints
     *     The prayerPoints
     */
    @JsonProperty("prayerPoints")
    public void setPrayerPoints(List<String> prayerPoints) {
        this.prayerPoints = prayerPoints;
    }

}
