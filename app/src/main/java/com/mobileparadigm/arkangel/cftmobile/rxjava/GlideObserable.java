package com.mobileparadigm.arkangel.cftmobile.rxjava;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by arkangel on 06/08/16.
 */
public class GlideObserable implements Observable.OnSubscribe<GlideDrawable> {
    private String url;

    public GlideObserable(String imageUrl) {
        url = imageUrl;
    }

    @Override
    public void call(final Subscriber<? super GlideDrawable> subscriber) {
        Glide.with(CFTMaterialApplication.getInstance()).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        subscriber.onNext(resource);subscriber.onCompleted();
                    }

                });
    }
}
