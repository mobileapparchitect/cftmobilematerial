/*
package com.mobileparadigm.arkangel.cftmobile.background_jobs;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Environment;
import android.text.TextUtils;

import com.cloudinary.utils.ObjectUtils;
import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.R;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.rosaloves.bitlyj.Url;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Map;

import de.greenrobot.event.EventBus;

import static com.rosaloves.bitlyj.Bitly.as;
import static com.rosaloves.bitlyj.Bitly.shorten;


*/
/**
 * Created by arkangel on 20/12/2015.
 *//*

public class VerseShareJob extends Job {
    public static final int PRIORITY = 1;
    public static final String URL_KEY = "url";
    public static final String CFT_VERSE_APP = "cft_verse_app";
    public static final String BIBLE_VERSE_URL = "bible_verse_url";
    public static final String BIBLE_VERSE_NAME = "bible_verse_name";
    public static final String PARSE_TABLE_NAME = "BibleVerseShare";
    private Bitmap bitmapToUpload;
    private File bitmapFile;
    private String nameOfFile;
    private String longUrl;
    private String shortUrl;

    public VerseShareJob(Bitmap bitmap, String fileName) {
        super(new Params(PRIORITY).setRequiresNetwork(true));
        bitmapToUpload = bitmap;
        nameOfFile = fileName;
        nameOfFile = nameOfFile.replace(" ", "_");
    }

    @Override
    public void onAdded() {

    }


    @Override
    public void onRun() throws Throwable {
        //search parse

        ParseQuery query = new ParseQuery(PARSE_TABLE_NAME);
        query.whereEqualTo(BIBLE_VERSE_NAME, nameOfFile);
        ArrayList<ParseObject> arrayList = (ArrayList<ParseObject>) query.find();
        if (arrayList.isEmpty()) {
            File file = saveBitmapToFile(nameOfFile, bitmapToUpload);
            Map cloudinaryMap = CFTMaterialApplication.cloudinaryInstance.uploader().upload(file, ObjectUtils.emptyMap());
            if (cloudinaryMap != null) {
                longUrl = (String) cloudinaryMap.get(URL_KEY);
            }
            Url urlLink = as(CFTMaterialApplication.getAppContext().getResources().getString(R.string.bitly_login), CFTMaterialApplication.getAppContext().getResources().getString(R.string.bitly_api_key)).call(shorten(longUrl));
            if (urlLink != null) {
                shortUrl = urlLink.getShortUrl();
            }
            if (!TextUtils.isEmpty(shortUrl)) {
                //save to parse
                ParseObject parseObject = new ParseObject(PARSE_TABLE_NAME);
                parseObject.put(BIBLE_VERSE_NAME, nameOfFile);
                parseObject.put(BIBLE_VERSE_URL, shortUrl);
                parseObject.save();
                EventBus.getDefault().post(shortUrl);
            }

        } else {
            ParseObject parseObject = arrayList.get(0);
            EventBus.getDefault().post(parseObject.get(BIBLE_VERSE_URL));
            deleteFile(nameOfFile);
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    private File saveBitmapToFile(String filename, Bitmap bitmap) {
        bitmap = getResizedBitmap(bitmap, 500, 500);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;
        File file = new File(extStorageDirectory, filename +".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
        }
        try {
            // make a new bitmap from your file
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;

    }

    private void deleteFile(String nameOfFile){
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File file = new File(extStorageDirectory, nameOfFile+ ".png");
        if (file.exists()) {
            file.delete();
        }
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        resizedBitmap=  addWhiteBorder(resizedBitmap,2);
        bm.recycle();
        return resizedBitmap;
    }
    private Bitmap addWhiteBorder(Bitmap bmp, int borderSize) {
        Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());
        Canvas canvas = new Canvas(bmpWithBorder);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bmp, borderSize, borderSize, null);
        return bmpWithBorder;
    }


}
*/
