package com.mobileparadigm.arkangel.cftmobile.ui.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.mobileparadigm.arkangel.cftmobile.CFTMaterialApplication;
import com.mobileparadigm.arkangel.cftmobile.eventbus.BusManager;
import com.mobileparadigm.arkangel.cftmobile.receivers.NetworkConnectivityReceiver;


public class NetworkHelper {

    private ConnectivityManager mConnectivityManager;

    public NetworkHelper() {

        this((ConnectivityManager) CFTMaterialApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE));
    }

    public NetworkHelper(ConnectivityManager connectivityManager) {
        mConnectivityManager = connectivityManager;
    }

    public boolean hasConnection() {
        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        boolean online = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        BusManager.getInstance().getNetworkConnectivityChangeEventBus().postIfChanged(
                online ? NetworkConnectivityReceiver.NetworkStatus.CONNECTED : NetworkConnectivityReceiver.NetworkStatus.DISCONNECTED
        );

        return online;
    }

}
