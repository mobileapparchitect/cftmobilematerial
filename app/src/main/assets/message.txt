The Church is the assembly of people who are God's worshippers. The word "Church" also refers to you as an individual because Christ lives in you.
Church, therefore, is a relationship or fellowship between God and all those who are saved (<verse>Ezekiel 16:1</verse>), and between individual saints (<verse>1 Corinthians 1:2</verse>).


Baptism is  a key component of salvation. It is an important aspect in the life of someone who has obtained salvation. This is demonstrated by John The Basptist in Jordan. (See <verse> Matt 3: 13-17</verse>)

All churches must baptise new converts and they must take them thru the appropriate road map. The three parts of baptism are as follows:

1st Sacrament - Baptism into salvation. This is the Baptism of the Holy Ghost into Christ.


2nd Sacrament - Water Baptism into Salvation.

3rd Sacrament - Holy Ghost. This is when a person is baptised into the Holy Spirit by Jesus.

<p><b><u>Pillars that build the church.</u></b></p>
The books of <verse>Acts 2:42</verse> says, "They devoted themselves to the apostles teaching and to fellowship to the breaking of bread and to prayer."

The 4 pillars that build the church are devotion, fellowship, breaking of bread and prayer.


<p><b><u>Devotion</u></b></p>

According to the scripture, you must devote yourself to the teaching of the apostles or you will fail. The word of God must be in oyu and you must live the word. This where your life is.

<p><b><u>Fellowship</u></b></p>

You must fellowship with each other (structure - cell group) at least every week. The structure(cell group), as laid down by the apostles must be followed.


<p><b><u>Breaking of bread</u></b></p>

Communion must be administered according to Biblical order and everyone must be reminded of its significance.

<p><b><u>Prayer</u></b></p>

Prayer is of great importance in the growth and success of any Christain. A prayerful Christain is a powerful Christain. It was in the prayer room that the apostles received the baptism of the Holy Ghost.